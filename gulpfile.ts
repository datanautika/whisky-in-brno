/* eslint-disable no-sync */

import * as path from 'path';
import * as gulp from 'gulp';
import * as del from 'del';
import * as babel from 'gulp-babel';
import * as replace from 'gulp-replace';
import * as webpack from 'webpack';
import * as fs from 'fs';
import * as ExtractText from 'extract-text-webpack-plugin';
import * as autoprefixer from 'autoprefixer';
import * as postcssCustomProperties from 'postcss-custom-properties';
import * as postcssVerticalRhythm from 'postcss-vertical-rhythm';
import * as postcssNested from 'postcss-nested';
import * as postcssPxToRem from 'postcss-pxtorem';
import * as postcssCalc from 'postcss-calc';
import * as postcssConditionals from 'postcss-conditionals';
import * as postcssCustomMedia from 'postcss-custom-media';
import * as postcssColorFunction from 'postcss-color-function';
import * as util from 'gulp-util';
import * as uglify from 'gulp-uglify';
import * as cssnano from 'gulp-cssnano';
import * as gulpDebug from 'gulp-debug';
import * as gulpRename from 'gulp-rename';
import * as gulpTypescript from 'gulp-typescript';
import * as gulpLineEndings from 'gulp-line-ending-corrector';
import * as merge from 'merge2';
import * as chalk from 'chalk';

import flattenTree from './src/internals/flattenTree';
import stylesConfig from './src/config/styles';


if (util.env.production) {
	console.log(chalk.white('Production environment!'));

	process.env.NODE_ENV = 'production';
	process.env.APP_ROOT = '';
} else {
	console.log(chalk.white('Development environment!'));

	process.env.NODE_ENV = 'development';
	process.env.APP_ROOT = '';
}

let variables = flattenTree(stylesConfig, {valuesToString: true, separator: '-'});

console.log(chalk.white('Variables:'));
console.log(chalk.grey(JSON.stringify(variables, null, ' ')));

const PATHS = {
	nextBuildDir: './build-next/',
	latestBuildDir: './build-latest/',
	publicAssetsDir: './public/assets',
	publicDistDir: './public/dist',
	publicDir: './public',

	srcFiles: ['./src/**/*.ts', './src/**/*.tsx'],
	typingsFiles: './typings/index.d.ts',
	nextBuildFiles: ['./build-next/**/*.js', './build-next/**/*.jsx'],
	latestBuildFiles: './build-latest/**/*',
	clientBuildFile: './build-next/client.js',
	clientJavaScriptDistFile: './public/dist/client.js',
	clientStylesDistFile: './public/dist/client.css',
	imageFiles: './assets/images/**/*',
	fontFiles: './assets/fonts/**/*.woff*',
	publicFiles: './public/**/*',
	stylesFiles: './src/**/*.css',
	definitionFiles: './build-next/**/*.d.ts',
};

const TASKS = {
	watch: 'watch',
	build: 'build',
	cleanup: 'cleanup',
	nextBuild: 'next-build',
	latestBuild: 'latest-build',
	dist: 'dist',
	distLineEndings: 'dist-line-endings',
	styles: 'styles',
	fonts: 'fonts',
	images: 'images',
	minifyScripts: 'minifiy-scripts',
	minifyStyles: 'minify-styles',
	definitions: 'definitions'
};

let mediaQueries = {
	'--tinyMenu-start-min': `(min-width: ${variables['breakpoints-tinyMenu-start']}px)`,
	'--tinyMenu-start-max': `(max-width: ${variables['breakpoints-tinyMenu-start']}px)`,
	'--tinyMenu-end-min': `(min-width: ${variables['breakpoints-tinyMenu-end']}px)`,
	'--tinyMenu-end-max': `(max-width: ${variables['breakpoints-tinyMenu-end']}px)`,
	'--compactMenu-start-min': `(min-width: ${variables['breakpoints-compactMenu-start']}px)`,
	'--compactMenu-start-max': `(max-width: ${variables['breakpoints-compactMenu-start']}px)`,
	'--compactMenu-end-min': `(min-width: ${variables['breakpoints-compactMenu-end']}px)`,
	'--compactMenu-end-max': `(max-width: ${variables['breakpoints-compactMenu-end']}px)`,
	'--compactPage-start-min': `(min-width: ${variables['breakpoints-compactPage-start']}px)`,
	'--compactPage-start-max': `(max-width: ${variables['breakpoints-compactPage-start']}px)`,
	'--compactPage-end-min': `(min-width: ${variables['breakpoints-compactPage-end']}px)`,
	'--compactPage-end-max': `(max-width: ${variables['breakpoints-compactPage-end']}px)`,
	'--singleColumnPage-start-min': `(min-width: ${variables['breakpoints-singleColumnPage-start']}px)`,
	'--singleColumnPage-start-max': `(max-width: ${variables['breakpoints-singleColumnPage-start']}px)`,
	'--singleColumnPage-middle-min': `(min-width: ${variables['breakpoints-singleColumnPage-middle']}px)`,
	'--singleColumnPage-middle-max': `(max-width: ${variables['breakpoints-singleColumnPage-middle']}px)`,
	'--singleColumnPage-end-min': `(min-width: ${variables['breakpoints-singleColumnPage-end']}px)`,
	'--singleColumnPage-end-max': `(max-width: ${variables['breakpoints-singleColumnPage-end']}px)`,
};


/**
 * Cleanup task
 *
 * Cleans all files created by various build tasks.
 */
gulp.task(TASKS.cleanup, () => del([PATHS.publicFiles, PATHS.nextBuildDir, PATHS.latestBuildDir]));


/**
 * Build scripts, next JS version
 *
 * Build source TypeScript files into JavaScript files.
 */
let typescriptSettings: any = {module: 'es6'};
let typescriptProject = gulpTypescript.createProject('tsconfig.json', typescriptSettings);

gulp.task(TASKS.nextBuild, () => {
	let result = merge([gulp.src(PATHS.typingsFiles), gulp.src(PATHS.srcFiles, {since: gulp.lastRun(TASKS.nextBuild)})])
		.pipe(gulpDebug({title: TASKS.nextBuild}))
		.pipe(typescriptProject());

	return merge([
		result.dts.pipe(gulp.dest(PATHS.nextBuildDir)),
		result.js
			.pipe(gulpRename({
				extname: '.js'
			}))
			.pipe(gulp.dest(PATHS.nextBuildDir))
	]);
});


/**
 * Build scripts, latest JS version
 *
 * Build JavaScript files for use in Node.js.
 */
let babelLatestConfig = JSON.parse(fs.readFileSync('.babelrc', {encoding: 'utf8'}));

gulp.task(TASKS.latestBuild, (done) => gulp.src(PATHS.nextBuildFiles, {since: gulp.lastRun(TASKS.latestBuild)})
	.pipe(gulpDebug({title: TASKS.latestBuild}))
	.pipe(babel(babelLatestConfig).on('error', (error) => {
		console.log(`${chalk.red(`${error.fileName}${(error.loc ?	`(${error.loc.line}, ${error.loc.column}): ` :	': ')}`)}error Babel: ${error.message}\n${error.codeFrame}`);
		
		done();
	}))
	.pipe(gulp.dest(PATHS.latestBuildDir)));


/**
 * Copy definitions
 */
gulp.task(TASKS.definitions, () => gulp.src(PATHS.definitionFiles, {since: gulp.lastRun(TASKS.definitions)})
	.pipe(gulpDebug({title: TASKS.definitions}))
	.pipe(gulp.dest(PATHS.latestBuildDir)));


/**
 * Create dist files
 *
 * Create dist files of scripts and styles.
 */
let babelCompatConfig = JSON.parse(fs.readFileSync(process.env.NODE_ENV === 'production' ? '.babelrc-compat-prod' : '.babelrc-compat-dev', {encoding: 'utf8'}));
let webpackCompiler = webpack({
	target: 'web',
	context: path.resolve(__dirname),
	entry: PATHS.clientBuildFile,
	output: {
		filename: path.basename(PATHS.clientJavaScriptDistFile),
		path: path.resolve(__dirname, PATHS.publicDistDir)
	},
	cache: true,
	module: {
		rules: [{
			test: /\.js$/,
			loader: 'babel-loader',
			options: babelCompatConfig,
			exclude: [/node_modules/],
		}, {
			test: /\.css$/,
			use: ExtractText.extract({
				use: [{
					loader: 'css-loader',
					options: {
						modules: true,
						url: false,
						importLoaders: 1,
						localIdentName: '[name]__[local]__[hash:base64:4]'
					}
				}, {
					loader: 'postcss-loader',
					options: {
						ident: 'postcss',
						plugins: () => [
							postcssNested(),
							postcssCustomProperties({variables}),
							postcssCustomMedia({extensions: mediaQueries}),
							postcssConditionals(),
							postcssVerticalRhythm({
								unit: 'bh',
								baselineHeight: stylesConfig.grid.baselineHeight
							}),
							postcssCalc({precision: 8}),
							postcssPxToRem({
								rootValue: stylesConfig.typographicScale.base,
								unitPrecision: 8,
								propWhiteList: ['font', 'font-size', 'line-height', 'letter-spacing', 'width', 'height', 'left', 'right', 'top', 'bottom', 'margin', 'margin-left', 'margin-right', 'margin-top', 'margin-bottom', 'padding', 'padding-left', 'padding-right', 'padding-top', 'padding-bottom', 'border', 'border-left', 'border-right', 'border-top', 'border-bottom', 'background', 'background-size', 'transform', 'transition', 'box-shadow'],
								replace: true,
								mediaQuery: false,
								selectorBlackList: ['html']
							}),
							postcssColorFunction(),
							autoprefixer({remove: false})
						]
					}
				}]
			})
		}]
	},
	plugins: [
		new ExtractText({
			filename: path.basename(PATHS.clientStylesDistFile),
			disable: false,
			allChunks: true
		}),
		new webpack.DefinePlugin({
			'process.env': {NODE_ENV: JSON.stringify(process.env.NODE_ENV)}
		}),
		new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en|cs)$/)
	],
	watch: false
} as any);

gulp.task(TASKS.dist, (done) => {
	webpackCompiler.run((error, stats) => {
		if (error) {
			util.log(error);

			if (done) {
				done();
			}
		} else {
			(stats as any).compilation.errors.forEach((compilationError) => {
				util.log(compilationError.error);
			});

			Object.keys((stats as any).compilation.assets).forEach((key) => {
				util.log('Webpack: output ', util.colors.green(key));
			});

			if (done) {
				done();
			}
		}
	});
});

gulp.task(TASKS.distLineEndings, () => gulp.src(PATHS.clientJavaScriptDistFile)
	.pipe(gulpLineEndings({verbose: true, eolc: 'LF'}))
	.pipe(gulp.dest(PATHS.publicDistDir)));


/**
 * Copy styles
 *
 * Copy styles to all the scripts build directories.
 */
gulp.task(TASKS.styles, () => gulp.src(PATHS.stylesFiles, {since: gulp.lastRun(TASKS.styles)})
	.pipe(gulpDebug({title: TASKS.styles}))
	.pipe(replace('%{appRoot}', process.env.APP_ROOT))
	.pipe(gulp.dest(PATHS.nextBuildDir))
	.pipe(gulp.dest(PATHS.latestBuildDir)));


/**
 * Copy fonts
 */
gulp.task(TASKS.fonts, () => gulp.src(PATHS.fontFiles)
	.pipe(gulp.dest(PATHS.publicAssetsDir)));


/**
 * Copy images
 */
gulp.task(TASKS.images, () => gulp.src(PATHS.imageFiles)
	.pipe(gulp.dest(PATHS.publicAssetsDir)));


/**
 * Minify dist script files
 */
gulp.task(TASKS.minifyScripts, () => gulp.src(PATHS.clientJavaScriptDistFile)
	.pipe(uglify())
	.pipe(gulp.dest(PATHS.publicDistDir)));


/**
 * Minify dist styles file
 */
gulp.task(TASKS.minifyStyles, () => gulp.src(PATHS.clientStylesDistFile)
	.pipe(cssnano({
		mergeLonghand: false,
		autoprefixer: false,
		safe: true
	}))
	.pipe(gulp.dest(PATHS.publicDistDir)));


/**
 * Watch task
 */
gulp.task(TASKS.watch, (done) => {
	gulp.watch([...PATHS.srcFiles], gulp.series(TASKS.nextBuild, TASKS.latestBuild, TASKS.dist, TASKS.distLineEndings));
	gulp.watch([PATHS.stylesFiles], gulp.series(TASKS.styles, TASKS.dist, TASKS.distLineEndings));
	gulp.watch([PATHS.imageFiles], gulp.series(TASKS.images));

	done();
});


/**
 * Default task
 *
 * Builds development build and starts watching for changes.
 */
gulp.task('default', gulp.series(
	TASKS.cleanup,
	gulp.parallel(
		TASKS.images, TASKS.fonts, gulp.series(
			gulp.parallel(TASKS.styles, TASKS.nextBuild),
			TASKS.latestBuild,
			TASKS.dist,
			TASKS.distLineEndings,
			TASKS.watch
		)
	)
));


/**
 * Build task
 */
gulp.task(TASKS.build, process.env.NODE_ENV === 'production' ? gulp.series(
	TASKS.cleanup,
	gulp.parallel(
		TASKS.images, TASKS.fonts, gulp.series(
			gulp.parallel(TASKS.styles, TASKS.nextBuild),
			TASKS.latestBuild,
			TASKS.dist,
			TASKS.distLineEndings,
			gulp.parallel(TASKS.minifyScripts, TASKS.minifyStyles)
		)
	)
) : gulp.series(
	TASKS.cleanup,
	gulp.parallel(
		TASKS.images, TASKS.fonts, gulp.series(
			gulp.parallel(TASKS.styles, TASKS.nextBuild),
			TASKS.latestBuild,
			TASKS.dist,
			TASKS.distLineEndings
		)
	)
));
