Whisky in Brno changelog
========================


## v0.7.2

- Update dependencies
- Fix bugs with styles


## v0.7.1

- Fix bugs with styles


## v0.7.0

- Fix bugs
- Update data


## v0.6.1

- Fix bugs


## v0.6.0

- Update dependencies
- Fix IIS config bugs


## v0.5.0

- Add menu
- Add about & update pages
- Update data
- Fix bugs & tweak build tasks


## v0.4.0

- Add whisky & place pages
- Update data


## v0.3.0

- Update data
- Replace React with Inferno
- Fix styles


## v0.2.0

- Add filtering & sorting


## v0.1.4

- Update data
- Add cookie law banner


## v0.1.3

- Handle uncaught server errors


## v0.1.2

- Fix HTTPS bug


## v0.1.1

- Add Google Analytics
- Force using HTTPS


## v0.1.0

- Initial version
