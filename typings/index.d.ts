declare module 'css-modules-require-hook';
declare module 'gulp';
declare module 'del';
declare module 'gulp-babel';
declare module 'extract-text-webpack-plugin';
declare module 'autoprefixer';
declare module 'postcss-css-variables';
declare module 'postcss-vertical-rhythm';
declare module 'postcss-nested';
declare module 'postcss-pxtorem';
declare module 'postcss-calc';
declare module 'postcss-conditionals';
declare module 'postcss-custom-media';
declare module 'gulp-util';
declare module 'gulp-uglify';
declare module 'gulp-cssnano';
declare module 'gulp-debug';
declare module 'gulp-replace';
declare module 'merge2';
declare module 'chalk';
declare module 'geolib';

declare module '*.css';
