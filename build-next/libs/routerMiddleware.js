const HTTP_NOT_FOUND = 404;
export default function routerMiddleware(router, fn) {
    return async (context, next) => {
        if (context.method === 'GET' && context.method === 'HEAD') {
            await next();
            return;
        }
        if (context.body && context.body !== null || context.status !== HTTP_NOT_FOUND) {
            await next();
            return;
        }
        let isRouteMatched = router.trigger(context.path, context);
        if (isRouteMatched) {
            await fn(context, next);
        }
    };
}
