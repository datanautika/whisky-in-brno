export default function routerMiddleware(router: any, fn: any): (context: any, next: any) => Promise<void>;
