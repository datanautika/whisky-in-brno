import Router from './Router';
export default class BrowserRouter {
    private location;
    private history;
    private fragment;
    private router;
    private root;
    private isSilent;
    isStarted: boolean;
    constructor(router: Router);
    readonly isAtRoot: boolean;
    readonly search: string;
    readonly path: string;
    start({root, isSilent}?: {
        root?: string;
        isSilent?: boolean;
    }): boolean;
    stop(): boolean;
    navigate(fragment?: string, {trigger, replace, resetScrollPosition}?: {
        trigger?: boolean;
        replace?: boolean;
        resetScrollPosition?: boolean;
    }): boolean;
}
