export declare class LocalStore {
    clear(): void;
    contains(key: any): boolean;
    keys(): string[];
    get(key: any, defaultValue?: any): any;
    getRemainingSpace(): number;
    getSize(): number;
    isEmpty(): boolean;
    remove(key: any): void;
    set(key: string, value: any): void;
}
declare const _default: LocalStore;
export default _default;
