import _ from 'lodash';
import $ from 'jquery';
const ROUTE_STRIPPER = /^[#\/]|\s+$/g;
const ROOT_STRIPPER = /^\/+|\/+$/g;
const PATH_STRIPPER = /#.*$/;
function normalizePathFragment(fragment) {
    return fragment.replace(ROUTE_STRIPPER, '');
}
let browserRouter;
export default class BrowserRouter {
    constructor(router) {
        this.location = global.location;
        this.history = global.history;
        this.fragment = '';
        this.root = '/';
        this.isSilent = false;
        this.isStarted = false;
        browserRouter = browserRouter ? browserRouter : this;
        this.router = router;
        return browserRouter;
    }
    get isAtRoot() {
        return this.location && this.location.pathname.replace(/[^\/]$/, '$&/') === this.root && !this.search;
    }
    get search() {
        let match = this.location.href.replace(/#.*/, '').match(/\?.+/);
        return match ? match[0] : '';
    }
    get path() {
        let path = decodeURI(this.location.pathname + this.search);
        let root = this.root.slice(0, -1);
        if (!path.indexOf(root)) {
            path = path.slice(root.length);
        }
        return path.slice(1).replace(ROUTE_STRIPPER, '');
    }
    start({ root = '/', isSilent = false } = {}) {
        this.root = root;
        this.isSilent = isSilent;
        this.fragment = this.path;
        this.root = `/${this.root}/`.replace(ROOT_STRIPPER, '/');
        $(document).on('scroll', _.debounce(() => {
            this.history.replaceState({
                scroll: $(document).scrollTop()
            }, document.title);
        }, 40));
        window.addEventListener('popstate', () => {
            let current = this.path;
            if (current === this.fragment) {
                return false;
            }
            this.router.trigger(current);
            this.fragment = current;
            return true;
        });
        this.history.scrollRestoration = 'manual';
        this.isStarted = true;
        if (!this.isSilent) {
            return this.router.trigger(this.fragment);
        }
        return false;
    }
    stop() {
        window.removeEventListener('popstate');
        this.history.scrollRestoration = 'auto';
        this.isStarted = false;
        return false;
    }
    navigate(fragment = '', { trigger = true, replace = false, resetScrollPosition = true } = {}) {
        if (!this.isStarted) {
            return false;
        }
        let newFragment = normalizePathFragment(fragment);
        let url = this.root + newFragment;
        newFragment = decodeURI(newFragment.replace(PATH_STRIPPER, ''));
        if (this.fragment === newFragment) {
            return false;
        }
        this.fragment = newFragment;
        if (this.fragment === '' && url !== '/') {
            url = url.slice(0, -1);
        }
        if (resetScrollPosition) {
            this.history.replaceState({ scroll: $(document).scrollTop() }, window.document.title);
            this.history[replace ? 'replaceState' : 'pushState']({ scroll: 0 }, window.document.title, url);
        }
        else {
            this.history.replaceState({ scroll: $(document).scrollTop() }, window.document.title);
            this.history[replace ? 'replaceState' : 'pushState']({}, window.document.title, url);
        }
        if (trigger) {
            return this.router.trigger(this.fragment);
        }
        return false;
    }
}
