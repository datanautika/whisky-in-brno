import Stream from '../libs/Stream';
export interface RouteRegExp extends RegExp {
    parameterNames: Array<string>;
}
export interface RouteParametersResult {
    context?: any;
    [parameter: string]: string | null;
}
export declare type RouteStream = Stream<RouteParametersResult>;
export interface Route {
    route: RouteRegExp;
    stream: RouteStream;
}
export default class Router {
    routes: Array<Route>;
    trigger(fragment?: string, context?: any): boolean;
    add(route: string, stream?: RouteStream): RouteStream;
}
