let defaultSize = 5242880;
export class LocalStore {
    clear() {
        window.localStorage.clear();
    }
    contains(key) {
        if (typeof key !== 'string') {
            throw new Error('Key must be a string for function contains(key)');
        }
        return this.keys().includes(key);
    }
    keys() {
        let keys = [];
        for (let i = 0; i < window.localStorage.length; i++) {
            keys.push(window.localStorage[i]);
        }
        return keys;
    }
    get(key, defaultValue) {
        if (typeof key === 'undefined') {
            let value = {};
            let keys = this.keys();
            for (let i = 0; i < keys.length; i++) {
                value[keys[i]] = this.get(keys[i]);
            }
            return value;
        }
        if (typeof key !== 'string') {
            throw new Error('Key must be a string for function get(key)');
        }
        let value = window.localStorage.getItem(key);
        let number = value ? parseFloat(value) : NaN;
        if (value === null) {
            return arguments.length === 2 ? defaultValue : null;
        }
        else if (!isNaN(number)) {
            return number;
        }
        else if (value.toLowerCase() === 'true' || value.toLowerCase() === 'false') {
            return value === 'true';
        }
        try {
            value = JSON.parse(value);
            return value;
        }
        catch (error) {
            return value;
        }
    }
    getRemainingSpace() {
        return defaultSize - this.getSize();
    }
    getSize() {
        return JSON.stringify(window.localStorage).length;
    }
    isEmpty() {
        return this.keys().length === 0;
    }
    remove(key) {
        if (typeof key === 'string') {
            window.localStorage.removeItem(key);
        }
        else if (key instanceof Array) {
            for (let i = 0; i < key.length; i++) {
                if (typeof key[i] === 'string') {
                    window.localStorage.removeItem(key[i]);
                }
                else {
                    throw new Error('Key in index ' + i + ' is not a string');
                }
            }
        }
        else {
            throw new Error('Key must be a string or array for function remove(key || array)');
        }
    }
    set(key, value) {
        let newValue = value;
        if (typeof key === 'string') {
            if (typeof newValue === 'object') {
                newValue = JSON.stringify(newValue);
            }
            window.localStorage.setItem(key, newValue);
        }
    }
}
export default new LocalStore();
