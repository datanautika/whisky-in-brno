export default class I18n {
    private strings;
    private currency;
    locale: string;
    constructor();
    use({strings, currency, locale}?: {
        strings?: {};
        currency?: string;
        locale?: string;
    }): this;
    translate(literals: any, ...values: any[]): any;
}
