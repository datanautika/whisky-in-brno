import Stream from '../libs/Stream';
const ROUTE_STRIPPER = /^[#\/]|\s+$/g;
const OPTIONAL_PARAM = /\((.*?)\)/g;
const NAMED_PARAM = /(\(\?)?:\w+/g;
const SPLAT_PARAM = /\*\w+/g;
const ESCAPE_REGEX = /[\-{}\[\]+?.,\\\^$|#\s]/g;
function routeToRegExp(route) {
    let parameterNames = [];
    let routeRegExp = new RegExp(`^${route.replace(ESCAPE_REGEX, '\\$&')
        .replace(OPTIONAL_PARAM, '(?:$1)?')
        .replace(NAMED_PARAM, (match, optional) => {
        parameterNames.push(match.slice(1));
        return optional ? match : '([^/?]+)';
    })
        .replace(SPLAT_PARAM, '([^?]*?)')}(?:\\?([\\s\\S]*))?$`);
    routeRegExp.parameterNames = parameterNames;
    return routeRegExp;
}
function extractParameters(routeRegExp, fragment) {
    let allParameters = routeRegExp.exec(fragment);
    if (allParameters) {
        let parameters = allParameters.slice(1);
        return parameters.map((parameter, index) => {
            if (parameters && index === parameters.length - 1) {
                return parameter || null;
            }
            return parameter ? decodeURIComponent(parameter) : null;
        });
    }
    return null;
}
function normalizePathFragment(fragment) {
    return fragment.replace(ROUTE_STRIPPER, '');
}
export default class Router {
    constructor() {
        this.routes = [];
    }
    trigger(fragment = '', context) {
        let normalizedFragment = normalizePathFragment(fragment);
        for (let i = 0; i < this.routes.length; i++) {
            if (this.routes[i].route.test(normalizedFragment)) {
                let parameterNames = this.routes[i].route.parameterNames.concat('search');
                let parameters = extractParameters(this.routes[i].route, normalizedFragment);
                let result = {};
                if (parameters) {
                    for (let j = 0; j < parameterNames.length; j++) {
                        result[parameterNames[j]] = parameters[j];
                    }
                    if (context) {
                        result.context = context;
                    }
                    this.routes[i].stream.push(result);
                    return true;
                }
            }
        }
        return false;
    }
    add(route, stream) {
        this.routes.unshift({
            route: routeToRegExp(route),
            stream: stream instanceof Stream ? stream : new Stream()
        });
        return this.routes[0].stream;
    }
}
