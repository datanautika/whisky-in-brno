import './internals/polyfills';
import $ from 'jquery';
import Inferno from 'inferno';
import Immutable from 'immutable';
import moment from 'moment';
import './components/Page.css';
import App from './components/App';
import Stream from './libs/Stream';
import browserRouter from './streams/browserRouter';
if (process.env.NODE_ENV === 'development') {
    global.Inferno = Inferno;
    global.$ = $;
    global.Immutable = Immutable;
    global.Stream = Stream;
    global.moment = moment;
}
if (process.env.NODE_ENV === 'development') {
    const G_KEY_CODE = 71;
    $(global.document).on('keydown', (event) => {
        let tagName = event.target.tagName.toLowerCase();
        if (event.keyCode === G_KEY_CODE && event.target && tagName !== 'textarea' && tagName !== 'input') {
            $('body').toggleClass('hasGrid');
        }
    });
}
browserRouter.start();
let rootNode = document.getElementById('app');
if (rootNode) {
    Inferno.render(<App />, rootNode);
}
$('#app').addClass('isLoaded');
const GA_ID = 'UA-90001694-1';
if (process.env.NODE_ENV === 'development') {
    global.ga('create', GA_ID, {
        cookieDomain: 'none'
    });
}
else {
    global.ga('create', GA_ID, 'auto');
}
