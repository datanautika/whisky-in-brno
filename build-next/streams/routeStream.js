import router from './router';
import constants from '../internals/constants';
import uriAppRoot from '../internals/uriAppRoot';
import support from '../internals/support';
import isProbablyJson from '../internals/isProbablyJson';
const EN_US = constants.EN_US;
const CS_CZ = constants.CS_CZ;
const CS = constants.CS;
const LANGUAGE_REGEX = new RegExp(CS);
const CLIENT_PLATFORM = constants.CLIENT_PLATFORM;
let routeStream = router.add(`${uriAppRoot.length ? `${uriAppRoot}(/)` : ''}(:language)(/:page)(/:subpage)(/:filter)(/)`).map((value) => {
    let { language, page, subpage, filter, context } = value;
    if (process.env.NODE_ENV === 'development' && support.platform === CLIENT_PLATFORM) {
        let routeString = '';
        if (language) {
            routeString += `/${language}`;
        }
        if (page) {
            routeString += `/${page}`;
        }
        if (subpage) {
            routeString += `/${subpage}`;
        }
        if (filter) {
            routeString += `/${filter}`;
        }
        if (!routeString) {
            routeString = '/';
        }
        global.ga('set', 'page', routeString);
        global.ga('send', 'pageview');
    }
    if (typeof window !== 'undefined' && window.history && window.history.state && typeof window.history.state.scroll !== 'undefined') {
        if (window.history.state.scroll <= 10) {
            requestAnimationFrame(() => {
                window.scrollTo(0, window.history.state.scroll);
            });
        }
        else {
            setTimeout(() => {
                window.scrollTo(0, window.history.state.scroll);
            }, 40);
        }
    }
    if (language !== CS_CZ && language !== EN_US) {
        filter = subpage;
        subpage = page;
        page = language;
        language = EN_US;
        if (context) {
            if (LANGUAGE_REGEX.test(context.headers['accept-language'])) {
                language = CS_CZ;
            }
            else {
                language = EN_US;
            }
        }
    }
    if (subpage && isProbablyJson(subpage)) {
        filter = subpage;
        subpage = null;
    }
    if (filter && !isProbablyJson(filter)) {
        filter = null;
    }
    return { language, page, subpage, filter };
});
global.routeStream = routeStream;
export default routeStream;
