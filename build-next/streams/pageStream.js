import routeStream from './routeStream';
import Stream from '../libs/Stream';
let pageStream = new Stream({
    previous: null,
    current: null
});
pageStream.combine((self, changed, dependency) => {
    let value = self.value;
    let { page } = dependency.value;
    if (page !== value.current) {
        let newValue = {
            current: page,
            previous: value.current
        };
        self.push(newValue);
    }
}, routeStream);
export default pageStream;
