import Stream from '../libs/Stream';
export interface SubpageStreamValue {
    previous: string | null;
    current: string | null;
}
declare let subpageStream: Stream<SubpageStreamValue>;
export default subpageStream;
