import Stream from '../libs/Stream';
export interface LocationStreamValue {
    latitude: number;
    longitude: number;
}
declare let locationStream: Stream<LocationStreamValue>;
export default locationStream;
