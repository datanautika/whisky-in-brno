import BrowserRouter from '../libs/BrowserRouter';
import router from './router';
export default new BrowserRouter(router);
