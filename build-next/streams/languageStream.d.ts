import Stream from '../libs/Stream';
export interface LanguageStreamValue {
    previous: string | null;
    current: string | null;
}
declare let languageStram: Stream<LanguageStreamValue>;
export default languageStram;
