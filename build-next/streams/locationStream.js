import Stream from '../libs/Stream';
let locationStream = new Stream({
    latitude: 49.194924,
    longitude: 16.608363
});
let navigator = global.navigator;
if (navigator && navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
        locationStream.push({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        });
    });
    let watchId = navigator.geolocation.watchPosition((position) => {
        locationStream.push({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        });
    });
}
global.locationStream = locationStream;
export default locationStream;
