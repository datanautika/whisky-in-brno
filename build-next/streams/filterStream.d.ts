import Stream from '../libs/Stream';
declare let filterStream: Stream<{
    previous: null;
    current: null;
    filter: {
        whiskyRegion: null;
        whiskyBrand: null;
        placeType: null;
        sortPlacesBy: string;
        sortWhiskiesBy: string;
    };
}>;
export default filterStream;
