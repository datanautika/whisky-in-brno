import Stream from '../libs/Stream';
import { Places, Whiskies, WhiskyRegions, PlaceTypes, WhiskyBrands } from '../types/data';
export interface DataStreamValue {
    places: Places;
    placeTypes: PlaceTypes;
    whiskies: Whiskies;
    whiskyRegions: WhiskyRegions;
    whiskyBrands: WhiskyBrands;
}
declare let dataStream: Stream<DataStreamValue>;
export default dataStream;
