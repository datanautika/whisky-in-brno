import nconf from 'nconf';
import secrets from './secrets';
nconf.argv();
nconf.env();
nconf.add('secrets', { type: 'literal', store: { secrets } });
export default nconf;
