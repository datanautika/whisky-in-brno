import _ from 'lodash';
export default function findWhisky(whiskies, whiskyId) {
    return _.cloneDeep(_.find(whiskies, { id: whiskyId }));
}
