import covariance from './covariance';
export default function sd(array) {
    return Math.sqrt(covariance(array, array));
}
