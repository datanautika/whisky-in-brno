import moment from 'moment';
import stringLocaleDayOfWeekToNumber from './stringLocaleDayOfWeekToNumber';
export default function parsePlaceOpeningHours(place) {
    for (let i = 0; i < place.openingHours.length; i++) {
        let [startDay, startTime] = place.openingHours[i].start.split(' ');
        let [endDay, endTime] = place.openingHours[i].end.split(' ');
        place.openingHours[i].startTime = moment(`${stringLocaleDayOfWeekToNumber(startDay)} ${startTime}`, 'e HH:mm');
        place.openingHours[i].endTime = moment(`${stringLocaleDayOfWeekToNumber(endDay)} ${endTime}`, 'e HH:mm');
        if (place.openingHours[i].endTime.isBefore(place.openingHours[i].startTime)) {
            place.openingHours[i].endTime.add(1, 'week');
        }
    }
}
