import { Whisky, Whiskies } from '../types/data';
export default function findWhisky(whiskies: Whiskies, whiskyId: number): Whisky | undefined;
