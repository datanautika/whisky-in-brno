export default function getWhiskyName(whisky) {
    let subtitle = '';
    let titlePart1 = '';
    let titlePart2 = '';
    let distilleries = whisky.distillery ? whisky.distillery.split(/(\s*,\s*)|(\s&\s)/) : [];
    if (whisky.age) {
        titlePart2 += `${whisky.age}\xa0yo`;
    }
    if (whisky.name) {
        titlePart2 += ` ${whisky.name}`;
    }
    if (whisky.brand) {
        titlePart1 = whisky.brand;
    }
    else if (distilleries.length === 1) {
        titlePart1 = distilleries[0];
    }
    else if (whisky.bottler && (!whisky.name || whisky.name && whisky.age)) {
        titlePart1 = whisky.bottler;
    }
    if (whisky.edition) {
        subtitle += whisky.edition;
    }
    if (whisky.batch) {
        subtitle += ` ${whisky.batch}`;
    }
    if (!titlePart2) {
        titlePart2 = subtitle;
        subtitle = '';
        if (!titlePart2 && whisky.vintage) {
            let date = whisky.vintage.match(/^\d{4}/);
            titlePart2 = date[0];
        }
    }
    if (titlePart1 && titlePart2) {
        titlePart1 = `${titlePart1.trim()} `;
    }
    return [titlePart1, titlePart2, subtitle];
}
