import _ from 'lodash';
const WHISKY_RATING_MEAN = 100;
const WHISKY_RATING_SD = 50;
export default function getPlaceWhiskyRating(place, mean, sd) {
    let whiskyRatingsSum = 0;
    let whiskyRatingsCount = 0;
    place.review.bottles.forEach((bottle) => {
        bottle.offers.forEach((offer) => {
            let rating = 1 / offer.score;
            if (rating && _.isFinite(rating)) {
                whiskyRatingsSum += rating;
                whiskyRatingsCount++;
            }
        });
    });
    if (whiskyRatingsCount && _.isFinite(whiskyRatingsSum)) {
        return ((whiskyRatingsSum / whiskyRatingsCount - mean) / sd) * WHISKY_RATING_SD + WHISKY_RATING_MEAN;
    }
    return NaN;
}
