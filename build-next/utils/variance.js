import covariance from './covariance';
export default function variance(array) {
    return covariance(array, array);
}
