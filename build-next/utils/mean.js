import product from './product';
import sum from './sum';
export default function mean(array) {
    return product([sum(array), 1 / array.length]);
}
