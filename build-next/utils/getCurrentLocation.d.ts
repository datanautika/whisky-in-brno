/// <reference types="bluebird" />
import Promise from 'bluebird';
export default function getCurrentLocation(): Promise<{}>;
