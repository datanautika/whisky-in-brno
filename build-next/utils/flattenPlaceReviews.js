import _ from 'lodash';
export default function flattenPlaceReviews(place) {
    place.review = _.assign({}, ...place.reviews);
    return place;
}
