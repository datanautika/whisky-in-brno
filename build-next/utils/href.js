import uriAppRoot from '../internals/uriAppRoot';
export default function href(...levels) {
    return `${uriAppRoot ? `${uriAppRoot}` : ''}/${levels.join('/')}`;
}
