function walkFlattenTree(tree, list, parent, convertToString, separator) {
    for (let property in tree) {
        if (Object.prototype.hasOwnProperty.call(tree, property)) {
            if (typeof tree[property] === 'object') {
                walkFlattenTree(tree[property], list, `${parent}${property}${separator}`, convertToString, separator);
            }
            else {
                list[parent + property] = convertToString ? `${tree[property]}` : tree[property];
            }
        }
    }
}
export default function flattenTree(tree, { valuesToString, separator } = {}) {
    let list = {};
    walkFlattenTree(tree, list, '', !!valuesToString, separator);
    return list;
}
