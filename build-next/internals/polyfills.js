import bluebird from 'bluebird';
import support from './support';
import constants from './constants';
const CLIENT_PLATFORM = constants.CLIENT_PLATFORM;
global.Promise = bluebird;
if (process.env.NODE_ENV === 'development' && support.platform === CLIENT_PLATFORM) {
}
function includes(searchElement, fromIndex = 0) {
    if (this === null) {
        throw new TypeError('Array.prototype.includes called on null or undefined');
    }
    let O = Object(this);
    let len = parseInt(O.length, 10) || 0;
    if (len === 0) {
        return false;
    }
    let n = parseInt(fromIndex, 10);
    let k;
    if (n >= 0) {
        k = n;
    }
    else {
        k = len + n;
        if (k < 0) {
            k = 0;
        }
    }
    let currentElement;
    while (k < len) {
        currentElement = O[k];
        if (searchElement === currentElement || (searchElement !== searchElement && currentElement !== currentElement)) {
            return true;
        }
        k++;
    }
    return false;
}
if (!Array.prototype.includes) {
    Array.prototype.includes = includes;
}
function areIntlLocalesSupported(...locales) {
    if (typeof Intl === 'undefined') {
        return false;
    }
    let intlConstructors = [
        Intl.Collator,
        Intl.DateTimeFormat,
        Intl.NumberFormat
    ].filter((intlConstructor) => intlConstructor);
    if (intlConstructors.length === 0) {
        return false;
    }
    return intlConstructors.every((intlConstructor) => {
        let supportedLocales = intlConstructor.supportedLocalesOf(locales);
        return supportedLocales.length === locales.length;
    });
}
if (global.Intl) {
    if (!areIntlLocalesSupported('cs-CZ')) {
        let IntlPolyfill = require('intl');
        Intl.NumberFormat = IntlPolyfill.NumberFormat;
        Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat;
    }
}
else {
    global.Intl = require('intl');
}
