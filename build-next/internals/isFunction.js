let isFunction = (value) => typeof value === 'function' || false;
if (isFunction(/x/) || global.Uint8Array && !isFunction(global.Uint8Array)) {
    isFunction = (value) => Object.prototype.toString.call(value) === '[object Function]';
}
export default isFunction;
