const ROUTE_STRIPPER = /^[#\/]|\s+$/g;
let uriAppRoot;
if (process.env.NODE_ENV === 'production') {
    uriAppRoot = ''.replace(ROUTE_STRIPPER, '');
}
else {
    uriAppRoot = ''.replace(ROUTE_STRIPPER, '');
}
export default uriAppRoot;
