declare const _default: {
    CLIENT_PLATFORM: string;
    SERVER_PLATFORM: string;
    EN_US: string;
    CS_CZ: string;
    EN: string;
    CS: string;
};
export default _default;
