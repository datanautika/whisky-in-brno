import * as url from 'url';
const HTTPS_PORT = 443;
const HTTP_OK = 200;
const HTTP_MOVED_PERMANENTLY = 301;
const HTTP_FOUND = 302;
const HTTP_FORBIDDEN = 403;
const HTTP_METHOD_NOT_ALLOWED = 405;
function portToUrlString({ skipDefaultPort, port }) {
    return (skipDefaultPort && port === HTTPS_PORT) ? '' : `:${port}`;
}
export default function forceHttpsMiddleware({ trustProtoHeader = false, trustAzureHeader = false, port = HTTPS_PORT, hostname = null, skipDefaultPort = true, ignoreUrl = false, isTemporary = false, redirectMethods = ['GET', 'HEAD'], internalRedirectMethods = [], useSpecCompliantDisallow = false } = {}) {
    let redirectStatus = {};
    redirectMethods.forEach((x) => {
        redirectStatus[x] = isTemporary ? HTTP_FOUND : HTTP_MOVED_PERMANENTLY;
    });
    internalRedirectMethods.forEach((x) => {
        redirectStatus[x] = 307;
    });
    redirectStatus.OPTIONS = 0;
    return async (context, next) => {
        let secure = context.secure;
        if (!secure && trustProtoHeader) {
            secure = context.request.header['x-forwarded-proto'] === 'https';
        }
        if (!secure && trustAzureHeader && context.request.header['x-arr-ssl']) {
            secure = true;
        }
        if (secure) {
            return await next();
        }
        if (!redirectStatus[context.method]) {
            if (context.method === 'OPTIONS') {
                context.response.status = HTTP_OK;
            }
            else {
                context.response.status = useSpecCompliantDisallow ? HTTP_METHOD_NOT_ALLOWED : HTTP_FORBIDDEN;
            }
            context.response.set('Allow', Object.keys(redirectStatus).join());
            context.response.body = '';
            return null;
        }
        let httpsHost = hostname || url.parse(`http://${context.request.header.host}`).hostname;
        let redirectTo = `https://${httpsHost}${portToUrlString({ skipDefaultPort, port })}`;
        if (!ignoreUrl) {
            redirectTo += context.request.url;
        }
        context.response.status = redirectStatus[context.method];
        context.response.redirect(redirectTo);
        return null;
    };
}
