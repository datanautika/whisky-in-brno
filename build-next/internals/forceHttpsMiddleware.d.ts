export interface ForceHttpsMiddlewareOptions {
    trustProtoHeader?: boolean;
    trustAzureHeader?: boolean;
    port?: number;
    hostname?: string | null;
    skipDefaultPort?: boolean;
    ignoreUrl?: boolean;
    isTemporary?: boolean;
    redirectMethods?: Array<string>;
    internalRedirectMethods?: Array<string>;
    useSpecCompliantDisallow?: boolean;
}
export interface RedirectStatus {
    GET?: number;
    HEAD?: number;
    OPTIONS?: number;
}
export default function forceHttpsMiddleware({trustProtoHeader, trustAzureHeader, port, hostname, skipDefaultPort, ignoreUrl, isTemporary, redirectMethods, internalRedirectMethods, useSpecCompliantDisallow}?: ForceHttpsMiddlewareOptions): (context: any, next: any) => Promise<any>;
