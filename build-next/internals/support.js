import constants from './constants';
const CLIENT_PLATFORM = constants.CLIENT_PLATFORM;
const SERVER_PLATFORM = constants.SERVER_PLATFORM;
let support = {};
support.platform = typeof exports !== 'undefined' && typeof global.process === 'object' ? SERVER_PLATFORM : CLIENT_PLATFORM;
export default support;
