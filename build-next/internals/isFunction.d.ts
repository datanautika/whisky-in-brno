declare let isFunction: (value: any) => boolean;
export default isFunction;
