import moment from 'moment';
import _ from 'lodash';
import originalWhiskies from './rawWhiskies';
import originalPlaces from './rawPlaces';
import flattenPlaceReviews from '../utils/flattenPlaceReviews';
import getWhiskyPriceScore from '../utils/getWhiskyPriceScore';
import findWhisky from '../utils/findWhisky';
import mean from '../utils/mean';
import sd from '../utils/sd';
import parsePlaceOpeningHours from '../utils/parsePlaceOpeningHours';
import isPlaceOpen from '../utils/isPlaceOpen';
import getWhiskyName from '../utils/getWhiskyName';
const WHISKY_RATING_MEAN = 100;
const WHISKY_RATING_SD = 50;
const HOURS_IN_DAY = 24;
const DAYS_IN_WEEK = 7;
let rawPlaces = originalPlaces;
let rawWhiskies = originalWhiskies;
rawWhiskies.forEach((whisky) => {
    whisky.fullName = getWhiskyName(whisky);
    whisky.sortName = whisky.fullName.map((name) => name.trim()).join(' ');
    whisky.readableId = `${whisky.id}-${_.kebabCase(`${whisky.fullName[0].trim()} ${whisky.fullName[1].trim()}`)}`;
    if (whisky.links) {
        whisky.links = whisky.links.filter((link) => !!link);
    }
});
rawPlaces.forEach((place) => {
    place.reviews.forEach((review) => {
        review.date = moment(review.date);
    });
    place.reviews.sort((ratingA, ratingB) => ratingA.date.valueOf() - ratingB.date.valueOf());
    place.isOpen = isPlaceOpen(place);
    place.readableId = `${place.id}-${_.kebabCase(place.name.trim())}`;
});
rawPlaces = rawPlaces.map(flattenPlaceReviews);
let whiskyRatings = [];
let placeRatings = [];
rawPlaces.forEach((place) => {
    let whiskyRatingsSum = 0;
    let whiskyRatingsCount = 0;
    if (place.review && place.review.bottles) {
        place.review.bottles.map((bottle) => {
            bottle.whisky = findWhisky(rawWhiskies, bottle.whiskyId);
            if (bottle.whisky) {
                bottle.offers.forEach((offer) => {
                    let score = bottle.whisky ? getWhiskyPriceScore(bottle.whisky, offer) : NaN;
                    if (_.isFinite(1 / score)) {
                        whiskyRatingsSum += 1 / score;
                        whiskyRatingsCount++;
                        offer.score = score;
                        whiskyRatings.push(1 / score);
                    }
                    else {
                        offer.score = NaN;
                    }
                });
            }
        });
        place.review.allBottles = place.review.bottles;
    }
    if (_.isFinite(whiskyRatingsSum)) {
        place.whiskyRating = whiskyRatingsSum / whiskyRatingsCount;
    }
    else {
        place.whiskyRating = NaN;
    }
    let placeScore = 0;
    let openingHoursSum = 0;
    parsePlaceOpeningHours(place);
    for (let i = 0; i < place.openingHours.length; i++) {
        if (moment.isMoment(place.openingHours[i].endTime)) {
            openingHoursSum += place.openingHours[i].endTime.diff(place.openingHours[i].startTime, 'hours');
        }
    }
    place.placeOpeningHoursScore = openingHoursSum / (HOURS_IN_DAY * DAYS_IN_WEEK / 10);
    placeScore += place.placeOpeningHoursScore;
    if (place.review) {
        if (place.review.bottles && place.review.bottles.length) {
            place.placeWhiskiesCountScore = place.review.bottles.length * 0.25;
            placeScore += place.placeWhiskiesCountScore;
        }
        if (place.review.glass === 'glencairn') {
            place.placeGlassScore = 2;
            placeScore += place.placeGlassScore;
        }
        else if (place.review.glass === 'tulip tasting') {
            place.placeGlassScore = 1;
            placeScore += place.placeGlassScore;
        }
        else {
            place.placeGlassScore = 0;
        }
        if (_.isNumber(place.review.ambient) && _.isFinite(place.review.ambient)) {
            place.placeAmbientScore = place.review.ambient;
            placeScore += place.placeAmbientScore;
        }
        if (_.isNumber(place.review.serviceInformedness) && _.isFinite(place.review.serviceInformedness)) {
            place.placeServiceInformednessScore = place.review.serviceInformedness;
            placeScore += place.placeServiceInformednessScore;
        }
        if (_.isNumber(place.review.serviceAmiability) && _.isFinite(place.review.serviceAmiability)) {
            place.placeServiceAmiabilityScore = place.review.serviceAmiability;
            placeScore += place.placeServiceAmiabilityScore;
        }
        if (_.isNumber(place.review.restroomCleanness) && _.isFinite(place.review.restroomCleanness)) {
            place.placeRestroomCleannessScore = place.review.restroomCleanness;
            placeScore += place.placeRestroomCleannessScore;
        }
        if (place.review.towels === 'paper') {
            place.placeTowelsScore = 1;
            placeScore += place.placeTowelsScore;
        }
        else if (place.review.towels === 'cloth') {
            place.placeTowelsScore = 2;
            placeScore += place.placeTowelsScore;
        }
        else {
            place.placeTowelsScore = 0;
        }
        if (place.review.isCashOnly) {
            place.placeIsCashOnlyScore = 0;
        }
        else {
            place.placeIsCashOnlyScore = 1;
            placeScore += place.placeIsCashOnlyScore;
        }
        if (place.review.isSmokingAllowed) {
            place.placeIsSmokingAllowedScore = 0;
        }
        else {
            place.placeIsSmokingAllowedScore = 1;
            placeScore += place.placeIsSmokingAllowedScore;
        }
    }
    if (_.isFinite(placeScore)) {
        place.placeScore = placeScore;
        place.placeRating = placeScore;
        placeRatings.push(place.placeRating);
    }
    else {
        place.placeScore = NaN;
    }
});
export let whiskyRatingsMean = mean(whiskyRatings);
export let whiskyRatingsSd = sd(whiskyRatings);
export let placeRatingsMean = mean(placeRatings);
export let placeRatingsSd = sd(placeRatings);
rawPlaces.forEach((place) => {
    if (place.review && place.review.bottles) {
        place.review.bottles.map((bottle) => {
            bottle.offers.forEach((offer) => {
                if (_.isNumber(offer.rating) && _.isFinite(offer.rating)) {
                    offer.rating = ((1 / offer.score - whiskyRatingsMean) / whiskyRatingsSd) * WHISKY_RATING_SD + WHISKY_RATING_MEAN;
                }
            });
        });
    }
    if (_.isNumber(place.whiskyRating) && _.isFinite(place.whiskyRating)) {
        place.whiskyRating = ((place.whiskyRating - whiskyRatingsMean) / whiskyRatingsSd) * WHISKY_RATING_SD + WHISKY_RATING_MEAN;
    }
    if (_.isNumber(place.placeRating) && _.isFinite(place.placeRating)) {
        place.placeRating = ((place.placeRating - placeRatingsMean) / placeRatingsSd) * WHISKY_RATING_SD + WHISKY_RATING_MEAN;
    }
    if (place.review && place.review.bottles) {
        place.review.bottles.sort((a, b) => a.offers[0].score - b.offers[0].score);
    }
});
rawPlaces.forEach((place) => {
    if (place.review && place.review.bottles) {
        place.review.bottles.map((bottle) => {
            if (bottle.whisky) {
                let whisky = _.find(rawWhiskies, { id: bottle.whiskyId });
                if (whisky && whisky.places) {
                    whisky.places.push(place);
                }
                else if (whisky) {
                    whisky.places = [place];
                }
            }
        });
    }
});
export let places = rawPlaces;
export let whiskies = rawWhiskies;
let whiskyBrands = rawWhiskies.map((whisky) => {
    if (whisky.brand) {
        return whisky.brand;
    }
    if (whisky.bottler) {
        return whisky.bottler;
    }
    if (whisky.distillery) {
        return whisky.distillery;
    }
    return '';
});
whiskyBrands = whiskyBrands.filter((whiskyBrand) => !!whiskyBrand);
whiskyBrands = _.uniq(whiskyBrands);
whiskyBrands = _.sortBy(whiskyBrands);
let placeTypes = rawPlaces.map((place) => place.type.toLowerCase());
placeTypes = _.uniq(placeTypes);
placeTypes = _.sortBy(placeTypes);
let whiskyRegions = rawWhiskies.map((whisky) => whisky.region);
whiskyRegions = _.uniq(whiskyRegions);
whiskyRegions = whiskyRegions.filter((whiskyRegion) => !!whiskyRegion);
whiskyRegions = _.sortBy(whiskyRegions);
export { whiskyBrands, placeTypes, whiskyRegions };
