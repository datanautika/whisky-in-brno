import Component from 'inferno-component';
import styles from './Footer.css';
import href from '../utils/href';
import isUrlInternal from '../internals/isUrlInternal';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';
let browserRouter = new BrowserRouter(router);
export default class Footer extends Component {
    constructor() {
        super(...arguments);
        this.handleClick = (event) => {
            if (event.button !== 1) {
                let url = event.target.getAttribute('href');
                if (isUrlInternal(url)) {
                    event.preventDefault();
                    browserRouter.navigate(url);
                }
            }
        };
    }
    render() {
        return <div className={styles.root} onClick={this.handleClick}>
			<ol>
				<li><a className={styles.hasNoUnderline} href={href('updates')}>v0.7.2</a></li>
				<li>Do you have any advice? <a href="mailto:whiskyinbrno@datanautika.com">Let us know!</a></li>
			</ol>
			<p className={styles.logo}>
				<a href="http://www.datanautika.com"><img src={href('assets', 'datanautika.svg')} alt="Made by Datanautika"/></a>
			</p>
		</div>;
    }
}
