import Component from 'inferno-component';
export interface InputProps {
    type?: 'text' | 'email';
    id?: string;
    name?: string;
    autocomplete?: boolean;
    value?: string;
    isValid?: boolean;
    isInvalid?: boolean;
    isDisabled?: boolean;
    handleChange?: (value: any) => void;
    handleSave?: (value: any) => void;
    validator?: (value: any) => void;
}
export default class Input extends Component<InputProps, {}> {
    render(): any;
    handleInput: (event: any) => void;
    handleFocusOut: (event: any) => void;
    handleKeyDown: (event: any) => void;
    validate(value: any): any;
}
