import Component from 'inferno-component';
export interface ButtonProps {
    id?: string;
    name?: string;
    type?: 'flat' | 'invisible';
    size?: string;
    link?: string;
    label?: string;
    badge?: string;
    icon?: string;
    selectedIcon?: string;
    iconAfter?: string;
    selectedIconAfter?: string;
    isSelected?: boolean;
    isDisabled?: boolean;
    isSubmit?: boolean;
    useRouter?: boolean;
    handleClick: () => any;
}
export default class Button extends Component<ButtonProps, {}> {
    render(): any;
    handleClick: (event: any) => void;
}
