import Component from 'inferno-component';
export interface TextareaProps {
    id?: string;
    name?: string;
    rows?: number;
    value?: string;
    isValid?: boolean;
    isInvalid?: boolean;
    isDisabled?: boolean;
    handleChange?: (value: string) => void;
    handleSave?: (value: string) => void;
    validator?: (value: string) => string;
}
export default class Textarea extends Component<TextareaProps, {}> {
    shouldComponentUpdate(newProps: any): boolean;
    validate(value: any): any;
    render(): any;
    handleInput: (event: any) => void;
    handleFocusOut: (event: any) => void;
}
