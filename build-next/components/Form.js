export default function Form(props) {
    let formProps = {};
    if (props.className) {
        formProps.className = props.className;
    }
    return <form {...formProps}>{props.children}</form>;
}
