import Component from 'inferno-component';
import { Whisky } from '../types/data';
export interface WhiskyNameProps {
    whisky: Whisky;
}
export default class WhiskyName extends Component<WhiskyNameProps, {}> {
    shouldComponentUpdate(nextProps: any): boolean;
    render(): any;
    handleClick: (event: any) => void;
}
