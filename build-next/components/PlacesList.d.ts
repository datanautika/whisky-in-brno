import Component from 'inferno-component';
import Immutable from 'immutable';
import Stream from '../libs/Stream';
export default class PlacesList extends Component<{}, {}> {
    state: {
        openPlaces: Immutable.Set<{}>;
    };
    refs: {
        map: null;
    };
    onDataStream: Stream<{}>;
    onLocationStream: Stream<{}>;
    componentDidMount(): void;
    componentDidUnmount(): void;
    render(): any;
    handleClick: (event: any) => void;
    handleFilterClick: (event: any) => void;
    handleOpenPlaceClick(placeId: any, event: any): void;
}
