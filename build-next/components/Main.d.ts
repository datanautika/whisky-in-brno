import Component from 'inferno-component';
import Stream from '../libs/Stream';
export default class Main extends Component<{}, {}> {
    onPageStream: Stream<{}>;
    onSubpageStream: Stream<{}>;
    componentDidMount(): void;
    componentDidUnmount(): void;
    render(): null;
}
