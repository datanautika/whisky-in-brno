import Component from 'inferno-component';
import _ from 'lodash';
import styles from './Breadcrumbs.css';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';
import routeStream from '../streams/routeStream';
import href from '../utils/href';
import isUrlInternal from '../internals/isUrlInternal';
import dataStream from '../streams/dataStream';
let browserRouter = new BrowserRouter(router);
export default class Breadcrumbs extends Component {
    constructor() {
        super(...arguments);
        this.handleClick = (event) => {
            if (event.button !== 1) {
                let url = event.target.getAttribute('href');
                if (isUrlInternal(url)) {
                    event.preventDefault();
                    browserRouter.navigate(url);
                }
            }
        };
    }
    render() {
        let { page, subpage } = routeStream.value;
        let { whiskies, places } = dataStream.value;
        let level1;
        let level2;
        if (page === 'whiskies') {
            level1 = 'Whiskies';
            if (subpage) {
                let whisky = _.find(whiskies, { readableId: subpage });
                if (whisky) {
                    level2 = whisky.sortName;
                }
            }
        }
        else if (page === 'places') {
            level1 = 'Places';
            if (subpage) {
                let place = _.find(places, { readableId: subpage });
                if (place) {
                    level2 = place.name;
                }
            }
        }
        return <nav className={styles.root} onClick={this.handleClick}>
			<ol className={styles.menu}>
				<li><a href={href('about')}>About</a></li>
				<li><a href={href('places')}>Places</a></li>
				<li><a href={href('whiskies')}>Whiskies</a></li>
			</ol>

			<ol className={styles.breadcrumbs}>
				<li><a href={href('')}>◊</a></li>
				{level1 ? <li><a href={href(page)}>{level1}</a></li> : null}
				{level2 ? <li>{level2}</li> : null}
			</ol>
		</nav>;
    }
    componentDidMount() {
        routeStream.subscribe(() => {
            this.forceUpdate();
        });
    }
}
