import Component from 'inferno-component';
import styles from './Input.css';
const ENTER_KEY_CODE = 13;
export default class Input extends Component {
    constructor() {
        super(...arguments);
        this.handleInput = (event) => {
            if (this.props && this.props.handleChange) {
                this.props.handleChange(this.validate(event.target.value));
            }
        };
        this.handleFocusOut = (event) => {
            if (this.props && this.props.handleSave) {
                this.props.handleSave(this.validate(event.target.value));
            }
        };
        this.handleKeyDown = (event) => {
            if (event.keyCode === ENTER_KEY_CODE) {
                if (this.props && this.props.handleSave) {
                    this.props.handleSave(this.validate(event.target.value));
                }
            }
        };
    }
    render() {
        let inputProps = {
            key: this.props ? this.props.id || this.props.name : '',
            className: styles.default + (this.props && this.props.isValid ? ' isValid' : '') + (this.props && this.props.isInvalid ? ' isInvalid' : '') + (this.props && this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
            type: 'text',
            name: this.props ? this.props.name || this.props.id : '',
            id: this.props ? this.props.id || this.props.name : '',
            value: this.props ? this.props.value : '',
            onBlur: this.handleFocusOut,
            onChange: this.handleInput,
            onKeyDown: this.handleKeyDown
        };
        if (this.props && this.props.type === 'email') {
            inputProps.type = this.props.type;
        }
        if (this.props && this.props.isDisabled) {
            inputProps.disabled = 'disabled';
        }
        if (this.props && this.props.autocomplete === false) {
            inputProps.autocomplete = 'off';
        }
        return <input {...inputProps}/>;
    }
    validate(value) {
        return this.props && this.props.validator ? this.props.validator(value) : value;
    }
}
