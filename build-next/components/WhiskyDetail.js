import Component from 'inferno-component';
import moment from 'moment';
import styles from './WhiskyDetail.css';
import href from '../utils/href';
const NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 1, maximumFractionDigits: 1 });
const ML_IN_L = 1000;
let name = (whisky) => {
    let [titlePart1, titlePart2, subtitle] = whisky.fullName;
    return <h2 className={styles.heading}>
		<span className={styles.title}>{titlePart1 ? titlePart1 : null}{titlePart2 ? <b>{titlePart2}</b> : null}</span>
		<span className={styles.subtitle}>{subtitle ? subtitle : null}</span>
	</h2>;
};
let parseDate = (dateString) => {
    let partsCount = dateString.match(/-/g);
    if (partsCount <= 1) {
        return dateString;
    }
    let date = moment(dateString);
    if (partsCount > 1) {
        return date.format('Do MMMM YYYY');
    }
    return date.format('MMMM YYYY');
};
export default class WhiskyDetail extends Component {
    render() {
        if (!this.props) {
            return null;
        }
        let { whisky } = this.props;
        let regionClass;
        let regionName;
        let districtName;
        let countryName = whisky.country;
        let isSingleMalt = whisky.type === 'Single Malt Whisky';
        if (whisky.region) {
            regionClass = styles[whisky.region.toLowerCase()];
            if (whisky.region === 'Islay') {
                regionName = 'Islay';
            }
            else if (whisky.region === 'Islands') {
                regionName = 'Islands';
            }
            else if (whisky.region === 'Highland') {
                regionName = 'Highland';
            }
            else if (whisky.region === 'Lowland') {
                regionName = 'Lowland';
            }
            else if (whisky.region === 'Speyside') {
                regionName = 'Speyside';
            }
            else if (whisky.region === 'Campbeltown') {
                regionName = 'Campbeltown';
            }
        }
        if (whisky.district) {
            districtName = whisky.district;
        }
        if (whisky.country === 'Scotland') {
            countryName = 'Skotsko';
        }
        return <main className={styles.root + (regionClass ? ` ${regionClass}` : '') + (isSingleMalt ? ` ${styles.isSingleMalt}` : '')}>
			{name(whisky)}

			<div className={styles.leftColumn}>
				<div className={styles.row}>
					{countryName ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Country</h4><p>{countryName}</p></div> : null}
					{regionName ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Region</h4><p>{regionName}</p></div> : null}
					{districtName ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>District</h4><p>{districtName}</p></div> : null}
				</div>

				{whisky.distillery || whisky.bottler ? <div className={styles.row}>
					{whisky.distillery ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Distillery</h4><p>{whisky.distillery}</p></div> : null}
					{whisky.bottler ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Bottler</h4><p>{whisky.bottler}</p></div> : null}
				</div> : null}

				{whisky.age || whisky.vintage || whisky.bottled ? <div className={styles.row}>
					{whisky.age ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Age</h4><p>{`${whisky.age} let`}</p></div> : null}
					{whisky.vintage ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Vintage</h4><p>{parseDate(whisky.vintage)}</p></div> : null}
					{whisky.bottled ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Bottled</h4><p>{parseDate(whisky.bottled)}</p></div> : null}
				</div> : null}

				<div className={styles.row}>
					{whisky.strength ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Strength</h4><p>{`${NUMBER_FORMAT.format(whisky.strength)} %`}</p></div> : null}
					{whisky.size ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Size</h4><p>{`${whisky.size * ML_IN_L} ml`}</p></div> : null}
					{whisky.caskNumber && whisky.caskType ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Cask</h4><p>{`${whisky.caskNumber} (${whisky.caskType})`}</p></div> : null}
					{!whisky.caskNumber && whisky.caskType ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Cask</h4><p>{`${whisky.caskType}`}</p></div> : null}
				</div>

				{whisky.links && whisky.links.length ? <div className={styles.row}>
					<div className={styles.wideRowItem}><h4 className={styles.attributeHeading}>Links</h4><p>{whisky.links.map((link, index) => <a key={index} href={link}>{link}</a>)}</p></div>
				</div> : null}
			</div>

			{whisky.images && whisky.images[0] ? <figure className={styles.rightColumn} style={{
            backgroundImage: `url("${href('assets', whisky.images[0])}")`
        }}><img src={href('assets', whisky.images[0])} alt=""/></figure> : null}
		</main>;
    }
}
