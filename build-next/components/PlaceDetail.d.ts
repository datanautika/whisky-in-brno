import Component from 'inferno-component';
import { Place } from '../types/data';
import Stream from '../libs/Stream';
export interface PlaceDetailProps {
    place: Place;
}
export default class PlaceDetail extends Component<PlaceDetailProps, {}> {
    onLanguageStream: Stream<{}>;
    onDataStream: Stream<{}>;
    componentDidMount(): void;
    componentDidUnmount(): void;
    render(): any;
}
