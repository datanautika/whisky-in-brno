export interface FormRowProps {
    id?: string;
    label?: string;
    hint?: string;
    showError?: string;
    errorMessage?: string;
    className?: string;
}
export default function FormRow(props: any): any;
