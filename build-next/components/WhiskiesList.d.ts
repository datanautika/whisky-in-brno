import Component from 'inferno-component';
import Stream from '../libs/Stream';
export default class WhiskiesList extends Component<{}, {}> {
    onLanguageStream: Stream<{}>;
    onDataStream: Stream<{}>;
    componentDidMount(): void;
    componentDidUnmount(): void;
    render(): any;
}
