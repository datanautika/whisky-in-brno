import Component from 'inferno-component';
import { Whisky } from '../types/data';
export interface WhiskyDetailProps {
    whisky: Whisky;
}
export default class WhiskyDetail extends Component<WhiskyDetailProps, {}> {
    render(): any;
}
