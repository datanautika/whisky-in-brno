import Component from 'inferno-component';
import styles from './Icon.css';
import href from '../utils/href';
const OLD_EDGE_ID = 10547;
const OLD_WEBKIT_ID = 537;
function embed(svg, target) {
    if (target) {
        let fragment = document.createDocumentFragment();
        let viewBox = !svg.getAttribute('viewBox') && target.getAttribute('viewBox');
        if (viewBox) {
            svg.setAttribute('viewBox', viewBox);
        }
        let clone = target.cloneNode(true);
        while (clone.childNodes.length) {
            fragment.appendChild(clone.firstChild);
        }
        svg.appendChild(fragment);
    }
}
function loadReadyStateChange(xhr) {
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            let cachedDocument = xhr._cachedDocument;
            if (!cachedDocument) {
                cachedDocument = xhr._cachedDocument = document.implementation.createHTMLDocument('');
                cachedDocument.body.innerHTML = xhr.responseText;
                xhr._cachedTarget = {};
            }
            xhr._embeds.splice(0).map((item) => {
                let target = xhr._cachedTarget[item.id];
                if (!target) {
                    target = xhr._cachedTarget[item.id] = cachedDocument.getElementById(item.id);
                }
                embed(item.svg, target);
            });
        }
    };
    xhr.onreadystatechange();
}
export default class Icon extends Component {
    constructor() {
        super(...arguments);
        this.refs = {};
    }
    render() {
        let styleSuffix = '';
        if (this.props && this.props.size === 'medium') {
            styleSuffix = ` ${styles.medium}`;
        }
        else if (this.props && this.props.size === 'large') {
            styleSuffix = ` ${styles.large}`;
        }
        return <span ref={(node) => { this.refs.root = node; }} className={styles.root + styleSuffix}>
			<svg xmlns="http://www.w3.org/2000/svg">
				{this.props && this.props.id ? <use xlinkHref={href('assets', `icons.svg#${this.props.id}`)} ref={(node) => { this.refs.use = node; }}/> : null}
			</svg>
		</span>;
    }
    componentDidMount() {
        let domNode = this.refs.root;
        let useNode = this.refs.use;
        if (!domNode || !useNode) {
            return;
        }
        let polyfill;
        let newerIEUA = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/;
        let webkitUA = /\bAppleWebKit\/(\d+)\b/;
        let olderEdgeUA = /\bEdge\/12\.(\d+)\b/;
        polyfill = newerIEUA.test(navigator.userAgent) || parseInt((navigator.userAgent.match(olderEdgeUA) || [])[1], 10) < OLD_EDGE_ID || parseInt((navigator.userAgent.match(webkitUA) || [])[1], 10) < OLD_WEBKIT_ID;
        let requests = {};
        if (polyfill) {
            requestAnimationFrame(() => {
                if (useNode) {
                    let svg = useNode.parentNode;
                    if (svg && /svg/i.test(svg.nodeName)) {
                        let src = useNode.getAttribute('xlink:href') || useNode.getAttribute('href');
                        svg.removeChild(useNode);
                        let srcSplit = src ? src.split('#') : null;
                        let url = srcSplit ? srcSplit.shift() : null;
                        let id = srcSplit ? srcSplit.join('#') : null;
                        if (url && url.length) {
                            let xhr = requests[url];
                            if (!xhr) {
                                xhr = requests[url] = new XMLHttpRequest();
                                xhr.open('GET', url);
                                xhr.send();
                                xhr._embeds = [];
                            }
                            xhr._embeds.push({
                                svg,
                                id
                            });
                            loadReadyStateChange(xhr);
                        }
                        else if (id) {
                            embed(svg, document.getElementById(id));
                        }
                    }
                }
            });
        }
    }
}
