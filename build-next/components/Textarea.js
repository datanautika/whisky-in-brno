import Component from 'inferno-component';
import styles from './Textarea.css';
export default class Textarea extends Component {
    constructor() {
        super(...arguments);
        this.handleInput = (event) => {
            if (this.props && this.props.handleChange) {
                this.props.handleChange(this.validate(event.target.value));
            }
        };
        this.handleFocusOut = (event) => {
            if (this.props && this.props.handleSave) {
                this.props.handleSave(this.validate(event.target.value));
            }
        };
    }
    shouldComponentUpdate(newProps) {
        if (this.props !== newProps) {
            return true;
        }
        return this.props && newProps &&
            (newProps.id !== this.props.id ||
                newProps.name !== this.props.name ||
                newProps.rows !== this.props.rows ||
                newProps.isValid !== this.props.isValid ||
                newProps.isInvalid !== this.props.isInvalid ||
                newProps.isDisabled !== this.props.isDisabled ||
                newProps.handleChange !== this.props.handleChange ||
                newProps.handleSave !== this.props.handleSave ||
                newProps.validator !== this.props.validator ||
                newProps.value !== this.props.value);
    }
    validate(value) {
        return this.props && this.props.validator ? this.props.validator(value) : value;
    }
    render() {
        let textareaProps = {
            key: this.props ? this.props.id || this.props.name : '',
            className: styles.default + (this.props && this.props.isInvalid ? ' isInvalid' : '') + (this.props && this.props.isValid ? ' isValid' : '') + (this.props && this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
            name: this.props ? this.props.name || this.props.id : '',
            id: this.props ? this.props.id || this.props.name : '',
            rows: this.props ? this.props.rows : 5,
            onBlur: this.handleFocusOut,
            onChange: this.handleInput,
            value: this.props ? this.props.value : ''
        };
        if (this.props && this.props.isDisabled) {
            textareaProps.disabled = 'disabled';
        }
        return <textarea {...textareaProps}/>;
    }
}
