import Component from 'inferno-component';
export default class App extends Component<{}, {}> {
    state: {
        isCookieLawBannerHidden: boolean;
    };
    render(): any;
    componentDidMount(): void;
    hideCookieLawBanner: () => void;
}
