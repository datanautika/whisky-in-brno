import './internals/polyfills';
import './internals/cssModules';
import * as fs from 'fs';
import * as path from 'path';
import * as http from 'http';
import * as https from 'https';
import Koa from 'koa';
import compress from 'koa-compress';
import logger from 'koa-logger';
import bodyParser from 'koa-bodyparser';
import serve from 'koa-static';
import InfernoServer from 'inferno-server';
import Bluebird from 'bluebird';
import appRoot from './internals/appRoot';
import uriAppRoot from './internals/uriAppRoot';
import App from './components/App';
import routerMiddleware from './libs/routerMiddleware';
import router from './streams/router';
import forceHttpsMiddleware from './internals/forceHttpsMiddleware';
const HTTP_NOT_FOUND = 404;
const HTTP_INTERNAL_ERROR = 500;
const HTTPS_PORT = 443;
let readFile = Bluebird.promisify(fs.readFile);
let app = new Koa();
console.log('App root dir:', `"${appRoot}"`);
console.log('App root URL:', `"${uriAppRoot}"`);
app.use(forceHttpsMiddleware({
    trustAzureHeader: true
}));
app.use(bodyParser());
app.use(logger());
app.use(async (context, next) => {
    try {
        await next();
    }
    catch (error) {
        console.warn(error);
        context.status = error.status || HTTP_INTERNAL_ERROR;
        context.body = {
            error: {
                message: error.message
            }
        };
        if (context.app) {
            context.app.emit('error', error, context);
        }
    }
});
app.use(serve(path.resolve(path.join(appRoot, 'public'))));
app.use(routerMiddleware(router, async (context, next) => {
    if (context.method !== 'HEAD' && context.method !== 'GET') {
        await next();
        return;
    }
    if (context.body && context.body !== null || context.status !== HTTP_NOT_FOUND) {
        await next();
        return;
    }
    let componentHTML = InfernoServer.renderToString(<App />);
    context.body = await readFile(path.join(appRoot, 'assets/templates/index.html'), 'utf8');
    context.body = context.body.replace('%{content}', componentHTML).replace(new RegExp('%{appRoot}', 'g'), uriAppRoot);
    await next();
}));
app.use(compress());
const PORT = 8080;
let httpsOptions = {};
if (process.env.NODE_ENV === 'development') {
    httpsOptions.key = fs.readFileSync('./localhost-key.pem');
    httpsOptions.cert = fs.readFileSync('./localhost-cert.pem');
}
let serverErrorListener = (error) => {
    console.log(error);
};
http.createServer(app.callback()).listen(process.env.PORT || PORT).on('error', serverErrorListener);
https.createServer(httpsOptions, app.callback()).listen(HTTPS_PORT).on('error', serverErrorListener);
console.log(`Listening on port ${process.env.PORT || PORT}...`);
