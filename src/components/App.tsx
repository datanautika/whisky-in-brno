import Inferno from 'inferno';
import Component from 'inferno-component';

import Header from './Header';
import Breadcrumbs from './Breadcrumbs';
import Main from './Main';
import Footer from './Footer';
import CookieLawBanner from './CookieLawBanner';
import styles from './App.css';
import localStore from '../libs/localStore';


export default class App extends Component<{}, {}> {
	state = {
		isCookieLawBannerHidden: true
	};

	render() {
		return <div className={styles.root}>
			<Header />
			<Breadcrumbs />
			<Main />
			{this.state.isCookieLawBannerHidden === true ? null : <CookieLawBanner handleHideBanner={this.hideCookieLawBanner} />}
			<Footer />
		</div>;
	}

	componentDidMount() {
		if (!localStore.get('isCookieLawBannerHidden')) {
			this.setState({isCookieLawBannerHidden: false});
		}
	}

	hideCookieLawBanner = () => {
		localStore.set('isCookieLawBannerHidden', true);
		this.setState({isCookieLawBannerHidden: true});

		this.forceUpdate();
	};
}
