import $ from 'jquery';
import _ from 'lodash';
import Inferno from 'inferno';
import Component from 'inferno-component';
import Immutable from 'immutable';

import styles from './PlacesList.css';
import dataStream from '../streams/dataStream';
import locationStream from '../streams/locationStream';
import WhiskyName from './WhiskyName';
import Icon from './Icon';
import tableStyles from './Table.css';
import href from '../utils/href';
import filterToUri from '../utils/filterToUri';
import filterStream from '../streams/filterStream';
import browserRouter from '../streams/browserRouter';
import isUrlInternal from '../internals/isUrlInternal';
import support from '../internals/support';
import constants from '../internals/constants';
import Stream from '../libs/Stream';


const CLIENT_PLATFORM = constants.CLIENT_PLATFORM;
const INTEGER_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 0, maximumFractionDigits: 0});
const FLOAT_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 2, maximumFractionDigits: 2});

let $map;
let map;
let youMarker;
let google = (global as any).google;

if (support.platform === CLIENT_PLATFORM) {
	$map = $('<div></div>');
	map = new google.maps.Map($map[0], {
		zoom: 17,
		center: new google.maps.LatLng(
			locationStream.value.latitude,
			locationStream.value.longitude
		)
	});
	youMarker = new google.maps.Marker({
		position: new google.maps.LatLng(locationStream.value.latitude, locationStream.value.longitude),
		label: 'Y',
		map
	});

	(window as any).$map = $map;

	dataStream.value.places.map((place) => new google.maps.Marker({
		position: new google.maps.LatLng(place.latitude, place.longitude),
		map
	}));
}

export default class PlacesList extends Component<{}, {}> {
	state = {
		openPlaces: Immutable.Set()
	};

	refs = {
		map: null
	};

	onDataStream: Stream<{}>;
	onLocationStream: Stream<{}>;

	componentDidMount() {
		this.onDataStream = dataStream.on(() => {
			this.forceUpdate();
		});
		this.onLocationStream = locationStream.on((value) => {
			map.setCenter(new google.maps.LatLng(value.latitude, value.longitude));
			youMarker.setPosition(new google.maps.LatLng(value.latitude, value.longitude));

			this.forceUpdate();
		});

		$map.appendTo(this.refs.map);
	}

	componentDidUnmount() {
		this.onDataStream.end();
		this.onLocationStream.end();
		$map.detach();
	}

	render() {
		let {places, placeTypes, whiskyRegions, whiskyBrands} = dataStream.value;
		let {filter} = filterStream.value;

		let placesElements = places.map((place) => {
			let whiskyElements = this.state.openPlaces.has(place.id) && place.review && place.review.bottles ? place.review.bottles.map((bottle, bottleIndex) => {
				let whiskyScoreClassName = '';
				let score = (bottle.offers[0].score as number);

				if (score < 2) {
					whiskyScoreClassName = styles.rating11;
				}

				if (score < 1.5) {
					whiskyScoreClassName = styles.rating11;
				} else if (score >= 1.5 && score < 1.7) {
					whiskyScoreClassName = styles.rating10;
				} else if (score >= 1.7 && score < 1.9) {
					whiskyScoreClassName = styles.rating9;
				} else if (score >= 1.9 && score < 2.1) {
					whiskyScoreClassName = styles.rating8;
				} else if (score >= 2.1 && score < 2.3) {
					whiskyScoreClassName = styles.rating7;
				} else if (score >= 2.3 && score < 2.5) {
					whiskyScoreClassName = styles.rating6;
				} else if (score >= 2.5 && score < 2.7) {
					whiskyScoreClassName = styles.rating5;
				} else if (score >= 2.7 && score < 2.9) {
					whiskyScoreClassName = styles.rating4;
				} else if (score >= 2.9 && score < 3.1) {
					whiskyScoreClassName = styles.rating3;
				} else if (score >= 3.1 && score < 3.3) {
					whiskyScoreClassName = styles.rating2;
				} else if (score >= 3.3) {
					whiskyScoreClassName = styles.rating1;
				}

				return bottle.whisky ? <li key={bottle.whisky.id} className={`${tableStyles.subitem} ${styles.tableSubitem}`}>
					<WhiskyName whisky={bottle.whisky} />
					<ul className={styles.whiskyInfo}>
						<li>{bottle.offers[0].price.value} {bottle.offers[0].price.currency === 'CZK' ? 'Kč' : ''}{/*({bottle.offers[0].size * 1000} ml)*/}</li>
						{_.isFinite(bottle.offers[0].score) ? <li><b className={`${styles.ratingBadge} ${whiskyScoreClassName}`}>{FLOAT_NUMBER_FORMAT.format(score)}</b></li> : null}
					</ul>
				</li> : null;
			}) : null;

			let whiskyRatingClassName = '';
			let placeRatingClassName = '';
			let whiskyRating = place.whiskyRating as number;
			let placeRating = place.placeRating as number;

			if (whiskyRating < 50) {
				whiskyRatingClassName = styles.rating1;
			} else if (whiskyRating >= 50 && whiskyRating < 65) {
				whiskyRatingClassName = styles.rating2;
			} else if (whiskyRating >= 65 && whiskyRating < 75) {
				whiskyRatingClassName = styles.rating3;
			} else if (whiskyRating >= 75 && whiskyRating < 85) {
				whiskyRatingClassName = styles.rating4;
			} else if (whiskyRating >= 85 && whiskyRating < 95) {
				whiskyRatingClassName = styles.rating5;
			} else if (whiskyRating >= 95 && whiskyRating < 105) {
				whiskyRatingClassName = styles.rating6;
			} else if (whiskyRating >= 105 && whiskyRating < 115) {
				whiskyRatingClassName = styles.rating7;
			} else if (whiskyRating >= 115 && whiskyRating < 125) {
				whiskyRatingClassName = styles.rating8;
			} else if (whiskyRating >= 125 && whiskyRating < 135) {
				whiskyRatingClassName = styles.rating9;
			} else if (whiskyRating >= 135 && whiskyRating < 150) {
				whiskyRatingClassName = styles.rating10;
			} else if (whiskyRating >= 150) {
				whiskyRatingClassName = styles.rating11;
			}

			if (placeRating < 50) {
				placeRatingClassName = styles.rating1;
			} else if (placeRating >= 50 && placeRating < 65) {
				placeRatingClassName = styles.rating2;
			} else if (placeRating >= 65 && placeRating < 75) {
				placeRatingClassName = styles.rating3;
			} else if (placeRating >= 75 && placeRating < 85) {
				placeRatingClassName = styles.rating4;
			} else if (placeRating >= 85 && placeRating < 95) {
				placeRatingClassName = styles.rating5;
			} else if (placeRating >= 95 && placeRating < 105) {
				placeRatingClassName = styles.rating6;
			} else if (placeRating >= 105 && placeRating < 115) {
				placeRatingClassName = styles.rating7;
			} else if (placeRating >= 115 && placeRating < 125) {
				placeRatingClassName = styles.rating8;
			} else if (placeRating >= 125 && placeRating < 135) {
				placeRatingClassName = styles.rating9;
			} else if (placeRating >= 135 && placeRating < 150) {
				placeRatingClassName = styles.rating10;
			} else if (placeRating >= 150) {
				placeRatingClassName = styles.rating11;
			}
			
			let icon = '';

			if (place.type === 'bar') {
				icon = 'cocktail';
			} else if (place.type === 'pub') {
				icon = 'beer';
			} else if (place.type === 'coffeehouse') {
				icon = 'coffee';
			} else if (place.type === 'winehouse') {
				icon = 'wine';
			} else if (place.type === 'bistro') {
				icon = 'noodles';
			} else if (place.type === 'restaurant') {
				icon = 'dinner';
			}

			return <section key={place.id} className={tableStyles.root} onClick={this.handleClick}>
				<header className={tableStyles.itemHeader}>
					<h3 className={`${tableStyles.itemHeading} ${styles.tableItemHeading}`}>
						<span className={tableStyles.label}>Name</span>
						<a href={href('places', place.readableId)}>{place.name}</a> {place.review && place.review.bottles && place.review.bottles.length ? <a href="#" onClick={this.handleOpenPlaceClick.bind(this, place.id)}>{this.state.openPlaces.has(place.id) ? <Icon id="subtract" /> : <Icon id="add" />}</a> : null}
					</h3>
					{icon ? <p className={`${tableStyles.itemInfo} ${tableStyles.isCenterAligned}`}>
						<span className={tableStyles.label}>Type</span>
						<span className={tableStyles.value}><Icon id={icon} size="medium" /></span>
					</p> : null}
					{place.openingHours && place.openingHours.length ? <p className={`${tableStyles.itemInfo} ${tableStyles.isCenterAligned}`}>
						<span className={tableStyles.label}>Is open?</span>
						<span className={tableStyles.value + (place.isOpen ? ` ${styles.isPlaceOpen}` : ` ${styles.isPlaceClosed}`)}>{place.isOpen ? <Icon id="check-medium" size="medium" /> : <Icon id="cancel-medium" size="medium" />}</span>
					</p> : null}
					{typeof place.distance !== 'undefined' ? <p className={`${tableStyles.itemInfo} ${tableStyles.isRightAligned}`}>
						<span className={tableStyles.label}>Distance</span>
						<span className={tableStyles.value}>{`${INTEGER_NUMBER_FORMAT.format(Math.round(place.distance / 10) * 10)} m`}</span>
					</p> : null}
					<p className={tableStyles.itemInfo + (_.isFinite(whiskyRating) ? '' : ` ${styles.isHidden}`)}>
						<span className={tableStyles.label}>Whisky rating</span>
						<span className={`${tableStyles.value} ${styles.ratingBadge} ${whiskyRatingClassName}`}>{_.isFinite(whiskyRating) ? INTEGER_NUMBER_FORMAT.format(whiskyRating) : '–'}</span>
					</p>
				</header>
				{whiskyElements && whiskyElements.length ? <ol className={`${tableStyles.subitems} ${styles.tableSubitems}`}>
					{whiskyElements}
				</ol> : null}
			</section>;
		});

		return <main className={styles.root}>
			<div ref={(node) => { this.refs.map = node; }} className={styles.map}></div>

			<nav className={tableStyles.filters} onClick={this.handleFilterClick}>
				<div className={tableStyles.filter}>
					<h4 className={tableStyles.filterHeading}>Filter places by whisky region</h4>
					<ol className={tableStyles.filterOptions}>
						{whiskyRegions.map((whiskyRegion) => <li key={whiskyRegion}><a className={whiskyRegion === filter.whiskyRegion ? tableStyles.isSelected : ''} href={href('places', filterToUri({whiskyRegion}))}>{whiskyRegion}</a></li>)}
						{filter.whiskyRegion ? <li><a href={href('places', filterToUri({whiskyRegion: null}))}>(cancel)</a></li> : null}
					</ol>
				</div>

				<div className={tableStyles.filter}>
					<h4 className={tableStyles.filterHeading}>Filter places by whisky brand</h4>
					<ol className={tableStyles.filterOptions}>
						{whiskyBrands.map((whiskyBrand) => <li key={whiskyBrand}><a className={whiskyBrand === filter.whiskyBrand ? tableStyles.isSelected : ''} href={href('places', filterToUri({whiskyBrand}))}>{whiskyBrand}</a></li>)}
						{filter.whiskyBrand ? <li><a href={href('places', filterToUri({whiskyBrand: null}))}>(cancel)</a></li> : null}
					</ol>
				</div>

				<div className={tableStyles.filter}>
					<h4 className={tableStyles.filterHeading}>Filter places by type</h4>
					<ol className={tableStyles.filterOptions}>
						{placeTypes.map((placeType) => <li key={placeType}><a className={placeType === filter.placeType ? tableStyles.isSelected : ''} href={href('places', filterToUri({placeType}))}>{_.startCase(placeType)}</a></li>)}
						{filter.placeType ? <li><a href={href('places', filterToUri({placeType: null}))}>(cancel)</a></li> : null}
					</ol>
				</div>

				<div className={tableStyles.filter}>
					<h4 className={tableStyles.filterHeading}>Sort places by…</h4>
					<ol className={tableStyles.filterOptions}>
						<li><a className={filter.sortPlacesBy === 'rating' ? tableStyles.isSelected : ''} href={href('places', filterToUri({sortPlacesBy: 'rating'}))}>Rating</a></li>
						<li><a className={filter.sortPlacesBy === 'distance' ? tableStyles.isSelected : ''} href={href('places', filterToUri({sortPlacesBy: 'distance'}))}>Distance</a></li>
						<li><a className={filter.sortPlacesBy === 'name' ? tableStyles.isSelected : ''} href={href('places', filterToUri({sortPlacesBy: 'name'}))}>Name</a></li>
					</ol>
				</div>

				<div className={tableStyles.filter}>
					<h4 className={tableStyles.filterHeading}>Sort whiskies by…</h4>
					<ol className={tableStyles.filterOptions}>
						<li><a className={filter.sortWhiskiesBy === 'score' ? tableStyles.isSelected : ''} href={href('places', filterToUri({sortWhiskiesBy: 'score'}))}>Score</a></li>
						<li><a className={filter.sortWhiskiesBy === 'price' ? tableStyles.isSelected : ''} href={href('places', filterToUri({sortWhiskiesBy: 'price'}))}>Price</a></li>
						<li><a className={filter.sortWhiskiesBy === 'name' ? tableStyles.isSelected : ''} href={href('places', filterToUri({sortWhiskiesBy: 'name'}))}>Name</a></li>
					</ol>
				</div>
			</nav>
			
			{placesElements}
		</main>;
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (isUrlInternal(url)) {
				event.preventDefault();

				if (/filter\/.+\//.test(url)) {
					browserRouter.navigate(url);
				} else {
					browserRouter.navigate(url);
				}
			}
		}
	};

	handleFilterClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (isUrlInternal(url)) {
				event.preventDefault();

				if (/places\/%7B/.test(url)) {
					browserRouter.navigate(url, {resetScrollPosition: false});
				} else {
					browserRouter.navigate(url);
				}
			}
		}
	};

	handleOpenPlaceClick(placeId, event) {
		let {places} = dataStream.value;

		event.preventDefault();

		if (this.state.openPlaces.has(placeId)) {
			this.state.openPlaces = this.state.openPlaces.delete(placeId);

			let place = _.find(places, {id: placeId});

			if (place) {
				(global as any).ga('send', 'event', 'Places list', 'Close place', place.name, placeId);
			}
		} else {
			this.state.openPlaces = this.state.openPlaces.add(placeId);

			let place = _.find(places, {id: placeId});

			if (place) {
				(global as any).ga('send', 'event', 'Places list', 'Open place', place.name, placeId);
			}
		}

		this.forceUpdate();
	}
}
