import Inferno from 'inferno';
import Component from 'inferno-component';
import _ from 'lodash';

import styles from './PlaceDetail.css';
import Icon from './Icon';
import WhiskyName from './WhiskyName';
import tableStyles from './Table.css';
import I18n from '../libs/I18n';
import languageStream from '../streams/languageStream';
import dataStream from '../streams/dataStream';
import {Place} from '../types/data';
import Stream from '../libs/Stream';


const INTEGER_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 0, maximumFractionDigits: 0});
const FLOAT_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 2, maximumFractionDigits: 2});
const SHORT_FLOAT_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 1, maximumFractionDigits: 1});

let i18n = new I18n();

let renderDate = (dateString) => {
	let result: Array<Element> = [];
	let thResult = /(\d)(th)/.exec(dateString);
	let stResult = /(\d)(st)/.exec(dateString);
	let ndResult = /(\d)(nd)/.exec(dateString);
	let rdResult = /(\d)(rd)/.exec(dateString);

	if (thResult && thResult.length) {
		dateString.split(/(\d+)(?:th)/).filter((value) => !!value).forEach((value, index, array) => {
			result.push(value);

			if (index < array.length - 1) {
				result.push(<sup>th</sup>);
			}
		});
	} else if (stResult && stResult.length) {
		dateString.split(/(\d+)(?:st)/).filter((value) => !!value).forEach((value, index, array) => {
			result.push(value);

			if (index < array.length - 1) {
				result.push(<sup>st</sup>);
			}
		});
	} else if (ndResult && ndResult.length) {
		dateString.split(/(\d+)(?:nd)/).filter((value) => !!value).forEach((value, index, array) => {
			result.push(value);

			if (index < array.length - 1) {
				result.push(<sup>nd</sup>);
			}
		});
	} else if (rdResult && rdResult.length) {
		dateString.split(/(\d+)(?:rd)/).filter((value) => !!value).forEach((value, index, array) => {
			result.push(value);

			if (index < array.length - 1) {
				result.push(<sup>rd</sup>);
			}
		});
	}

	return result;
};

export interface PlaceDetailProps {
	place: Place;
}

export default class PlaceDetail extends Component<PlaceDetailProps, {}> {
	onLanguageStream: Stream<{}>;
	onDataStream: Stream<{}>;

	componentDidMount() {
		this.onLanguageStream = languageStream.on(() => {
			this.forceUpdate();
		});
		this.onDataStream = dataStream.on(() => {
			this.forceUpdate();
		});
	}

	componentDidUnmount() {
		this.onLanguageStream.end();
		this.onDataStream.end();
	}

	render() {
		if (!this.props) {
			return null;
		}

		let {place} = this.props;

		let whiskyElements = place.review && place.review.bottles ? place.review.bottles.map((bottle, bottleIndex) => {
			let whiskyScoreClassName = '';
			let score = (bottle.offers[0].score as number);

			if (score < 2) {
				whiskyScoreClassName = styles.rating11;
			}

			if (score < 1.5) {
				whiskyScoreClassName = styles.rating11;
			} else if (score >= 1.5 && score < 1.7) {
				whiskyScoreClassName = styles.rating10;
			} else if (score >= 1.7 && score < 1.9) {
				whiskyScoreClassName = styles.rating9;
			} else if (score >= 1.9 && score < 2.1) {
				whiskyScoreClassName = styles.rating8;
			} else if (score >= 2.1 && score < 2.3) {
				whiskyScoreClassName = styles.rating7;
			} else if (score >= 2.3 && score < 2.5) {
				whiskyScoreClassName = styles.rating6;
			} else if (score >= 2.5 && score < 2.7) {
				whiskyScoreClassName = styles.rating5;
			} else if (score >= 2.7 && score < 2.9) {
				whiskyScoreClassName = styles.rating4;
			} else if (score >= 2.9 && score < 3.1) {
				whiskyScoreClassName = styles.rating3;
			} else if (score >= 3.1 && score < 3.3) {
				whiskyScoreClassName = styles.rating2;
			} else if (score >= 3.3) {
				whiskyScoreClassName = styles.rating1;
			}

			return bottle.whisky ? <li key={bottle.whisky.id} className={`${tableStyles.subitem} ${styles.tableSubitem}`}>
				<WhiskyName whisky={bottle.whisky} />
				<ul className={styles.whiskyInfo}>
					<li>{bottle.offers[0].price.value} {bottle.offers[0].price.currency === 'CZK' ? 'Kč' : ''}{/*({bottle.offers[0].size * 1000} ml)*/}</li>
					{_.isFinite(score) ? <li><b className={`${styles.ratingBadge} ${whiskyScoreClassName}`}>{FLOAT_NUMBER_FORMAT.format(score)}</b></li> : null}
				</ul>
			</li> : null;
		}) : null;

		let whiskyRatingClassName = '';
		let placeRatingClassName = '';
		let whiskyRating = place.whiskyRating as number;
		let placeRating = place.placeRating as number;

		if (whiskyRating < 50) {
			whiskyRatingClassName = styles.rating1;
		} else if (whiskyRating >= 50 && whiskyRating < 65) {
			whiskyRatingClassName = styles.rating2;
		} else if (whiskyRating >= 65 && whiskyRating < 75) {
			whiskyRatingClassName = styles.rating3;
		} else if (whiskyRating >= 75 && whiskyRating < 85) {
			whiskyRatingClassName = styles.rating4;
		} else if (whiskyRating >= 85 && whiskyRating < 95) {
			whiskyRatingClassName = styles.rating5;
		} else if (whiskyRating >= 95 && whiskyRating < 105) {
			whiskyRatingClassName = styles.rating6;
		} else if (whiskyRating >= 105 && whiskyRating < 115) {
			whiskyRatingClassName = styles.rating7;
		} else if (whiskyRating >= 115 && whiskyRating < 125) {
			whiskyRatingClassName = styles.rating8;
		} else if (whiskyRating >= 125 && whiskyRating < 135) {
			whiskyRatingClassName = styles.rating9;
		} else if (whiskyRating >= 135 && whiskyRating < 150) {
			whiskyRatingClassName = styles.rating10;
		} else if (whiskyRating >= 150) {
			whiskyRatingClassName = styles.rating11;
		}

		if (placeRating < 50) {
			placeRatingClassName = styles.rating1;
		} else if (placeRating >= 50 && placeRating < 65) {
			placeRatingClassName = styles.rating2;
		} else if (placeRating >= 65 && placeRating < 75) {
			placeRatingClassName = styles.rating3;
		} else if (placeRating >= 75 && placeRating < 85) {
			placeRatingClassName = styles.rating4;
		} else if (placeRating >= 85 && placeRating < 95) {
			placeRatingClassName = styles.rating5;
		} else if (placeRating >= 95 && placeRating < 105) {
			placeRatingClassName = styles.rating6;
		} else if (placeRating >= 105 && placeRating < 115) {
			placeRatingClassName = styles.rating7;
		} else if (placeRating >= 115 && placeRating < 125) {
			placeRatingClassName = styles.rating8;
		} else if (placeRating >= 125 && placeRating < 135) {
			placeRatingClassName = styles.rating9;
		} else if (placeRating >= 135 && placeRating < 150) {
			placeRatingClassName = styles.rating10;
		} else if (placeRating >= 150) {
			placeRatingClassName = styles.rating11;
		}

		let icon = '';

		if (place.type === 'bar') {
			icon = 'cocktail';
		} else if (place.type === 'pub') {
			icon = 'beer';
		} else if (place.type === 'coffeehouse') {
			icon = 'coffee';
		} else if (place.type === 'winehouse') {
			icon = 'wine';
		} else if (place.type === 'bistro') {
			icon = 'noodles';
		} else if (place.type === 'restaurant') {
			icon = 'dinner';
		}

		return <main className={styles.root}>
			<h2 className={styles.heading}>
				<span className={styles.title}>{place.name}</span>
			</h2>

			{place.description && place.description[i18n.locale] ? <div className={styles.row}>
				<div className={styles.tripleRowItem}>
					<p>{place.description[i18n.locale]}</p>
				</div>
			</div> : null}

			<div className={styles.row}>
				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Whisky rating</h4>
					<p>
						<span className={`${styles.ratingBadge} ${whiskyRatingClassName}`}>{_.isFinite(whiskyRating) ? INTEGER_NUMBER_FORMAT.format(whiskyRating) : '–'}</span>
					</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Place rating</h4>
					<p>
						<span className={`${styles.ratingBadge} ${placeRatingClassName}`}>{_.isFinite(placeRating) ? INTEGER_NUMBER_FORMAT.format(placeRating) : '–'}</span>
					</p>
				</div>

				<div className={styles.doubleRowItem}>
					<h4 className={styles.attributeHeading}>Latest rated on</h4>
					<p className={place.review && place.review.date ? '' : styles.isUnknown}>{place.review && typeof place.review.date !== 'string' ? renderDate(place.review.date.format('Do MMMM YYYY')) : '?'}</p>
				</div>
			</div>

			<h3 className={styles.whiskiesHeading}>Information</h3>

			<div className={styles.row}>
				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Type</h4>
					<p className={icon ? '' : `${styles.isUnknown} ${styles.hasOnlyText}`}>{icon ? <Icon id={icon} size="medium" /> : '?'}</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Is open?</h4>
					<p className={(typeof place.isOpen === 'boolean' && place.openingHours && place.openingHours.length ? '' : `${styles.isUnknown} ${styles.hasOnlyText}`) + (place.isOpen ? ` ${styles.isTrue}` : ` ${styles.isFalse}`)}>{typeof place.isOpen === 'boolean' && place.openingHours && place.openingHours.length ? (place.isOpen ? <Icon id="check-medium" size="medium" /> : <Icon id="cancel-medium" size="medium" />) : '?'}</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Card accepted?</h4>
					<p className={(place.review && typeof place.review.isCashOnly === 'boolean' ? '' : `${styles.isUnknown} ${styles.hasOnlyText}`) + (place.review && place.review.isCashOnly ? ` ${styles.isFalse}` : ` ${styles.isTrue}`)}>{place.review && typeof place.review.isCashOnly === 'boolean' ? (place.review && place.review.isCashOnly ? <Icon id="cancel-medium" size="medium" /> : <Icon id="check-medium" size="medium" />) : '?'}</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Non-smoking?</h4>
					<p className={(place.review && typeof place.review.isSmokingAllowed === 'boolean' ? '' : `${styles.isUnknown} ${styles.hasOnlyText}`) + (place.review && place.review.isSmokingAllowed ? ` ${styles.isFalse}` : ` ${styles.isTrue}`)}>{place.review && typeof place.review.isSmokingAllowed === 'boolean' ? (place.review && place.review.isSmokingAllowed ? <Icon id="cancel-medium" size="medium" /> : <Icon id="check-medium" size="medium" />) : '?'}</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Glass</h4>
					<p className={styles.hasOnlyText + (place.review && place.review.glass ? '' : ` ${styles.isUnknown}`)}>{place.review && place.review.glass ? place.review.glass : '?'}</p>
				</div>
			</div>

			<div className={styles.row}>
				<div className={`${styles.rowItem} ${styles.isTopAligned}`}>
					<h4 className={styles.attributeHeading}>Distance</h4>
					<p className={typeof place.distance !== 'undefined' ? '' : styles.isUnknown}>{typeof place.distance !== 'undefined' ? `${INTEGER_NUMBER_FORMAT.format(Math.round(place.distance / 10) * 10)} m` : '?'}</p>
				</div>

				{place.address ? <div className={`${styles.rowItem} ${styles.isTopAligned}`}>
					<h4 className={styles.attributeHeading}>Address</h4>
					<p>{place.address}</p>
				</div> : null}

				<div className={styles.tripleRowItem}>
					<h4 className={styles.attributeHeading}>Opening hours</h4>
					{place.review && !place.review.isOperational ? <p>Permanently or temporarily closed</p> : <p className={styles.hasOnlyText + (place.openingHours && place.openingHours.length ? '' : ` ${styles.isUnknown}`)}>
						{place.openingHours && place.openingHours.length ? place.openingHours.map((openingHours) => <p className={styles.openingHours}><span className={styles.openingHoursLabel}>{openingHours.startTime ? openingHours.startTime.format('dddd') : null}</span><span className={styles.openingHoursValue}>{openingHours.startTime && openingHours.endTime ? `${openingHours.startTime.format('HH:mm')} – ${openingHours.endTime.format('HH:mm')}` : null}</span></p>) : '?'}
					</p>}
				</div>
			</div>

			<div className={styles.row}>
				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Ambient</h4>
					<p className={place.review && typeof place.review.ambient === 'number' ? '' : styles.isUnknown}>{place.review && typeof place.review.ambient === 'number' ? `${place.review.ambient} / 10` : '?'}</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Service informedness</h4>
					<p className={place.review && typeof place.review.serviceInformedness === 'number' ? '' : styles.isUnknown}>{place.review && typeof place.review.serviceInformedness === 'number' ? `${place.review.serviceInformedness} / 10` : '?'}</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Service amiability</h4>
					<p className={place.review && typeof place.review.serviceAmiability === 'number' ? '' : styles.isUnknown}>{place.review && typeof place.review.serviceAmiability === 'number' ? `${place.review.serviceAmiability} / 10` : '?'}</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Restroom cleanness</h4>
					<p className={place.review && typeof place.review.restroomCleanness === 'number' ? '' : styles.isUnknown}>{place.review && typeof place.review.restroomCleanness === 'number' ? `${place.review.restroomCleanness} / 10` : '?'}</p>
				</div>

				<div className={styles.rowItem}>
					<h4 className={styles.attributeHeading}>Towels</h4>
					<p className={place.review && place.review.towels ? '' : styles.isUnknown}>{place.review && place.review.towels ? place.review.towels : '?'}</p>
				</div>
			</div>

			<h3 className={styles.whiskiesHeading}>Whiskies</h3>
			
			{whiskyElements && whiskyElements.length ? <ol className={`${tableStyles.subitems} ${styles.tableSubitems}`}>
				{whiskyElements}
			</ol> : null}

			<h3 className={styles.whiskiesHeading}>Rating calculation</h3>

			<ul className={styles.points}>
				<li>
					<p className={styles.pointsLabel}>Number of bottles</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeWhiskiesCountScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeWhiskiesCountScore as number)}` : '–'}</p>
				</li>
				
				<li>
					<p className={styles.pointsLabel}>Glass</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeGlassScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeGlassScore as number)}` : '–'}</p>
				</li>
				
				<li>
					<p className={styles.pointsLabel}>Opening hours</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeOpeningHoursScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeOpeningHoursScore as number)}` : '–'}</p>
				</li>

				<li>
					<p className={styles.pointsLabel}>Card accepted?</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeIsCashOnlyScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeIsCashOnlyScore as number)}` : '–'}</p>
				</li>
				
				<li>
					<p className={styles.pointsLabel}>Non-smoking?</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeIsSmokingAllowedScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeIsSmokingAllowedScore as number)}` : '–'}</p>
				</li>

				<li>
					<p className={styles.pointsLabel}>Ambient</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeAmbientScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeAmbientScore as number)}` : '–'}</p>
				</li>
				
				<li>
					<p className={styles.pointsLabel}>Service amiability</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeServiceAmiabilityScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeServiceAmiabilityScore as number)}` : '–'}</p>
				</li>
				
				<li>
					<p className={styles.pointsLabel}>Service informedness</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeServiceInformednessScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeServiceInformednessScore as number)}` : '–'}</p>
				</li>
				
				<li>
					<p className={styles.pointsLabel}>Restroom cleanness</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeRestroomCleannessScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeRestroomCleannessScore as number)}` : '–'}</p>
				</li>
				
				<li>
					<p className={styles.pointsLabel}>Towels</p>
					<p className={styles.pointsValue}>{_.isFinite(place.placeTowelsScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeTowelsScore as number)}` : '–'}</p>
				</li>
				
				<li>
					<p className={styles.pointsLabel}><strong>Total</strong></p>
					<p className={styles.pointsValue}><strong>{_.isFinite(place.placeScore) ? SHORT_FLOAT_NUMBER_FORMAT.format(place.placeScore as number) : '–'}</strong></p>
				</li>
			</ul>
		</main>;
	}
}
