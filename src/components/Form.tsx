import Inferno from 'inferno';


export interface FormProps {
	className?: string;
}

export default function Form(props) {
	let formProps: {className?: string} = {};

	if (props.className) {
		formProps.className = props.className;
	}

	return <form {...formProps}>{props.children}</form>;
}
