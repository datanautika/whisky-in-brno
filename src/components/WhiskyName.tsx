import Inferno from 'inferno';
import Component from 'inferno-component';

import styles from './WhiskyName.css';
import getWhiskyName from '../utils/getWhiskyName';
import href from '../utils/href';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';
import isUrlInternal from '../internals/isUrlInternal';
import {Whisky} from '../types/data';


let browserRouter = new BrowserRouter(router);

export interface WhiskyNameProps {
	whisky: Whisky;
}

export default class WhiskyName extends Component<WhiskyNameProps, {}> {
	shouldComponentUpdate(nextProps) {
		if (nextProps && this.props) {
			if (nextProps.whisky.id !== this.props.whisky.id ||
				nextProps.whisky.versionOf !== this.props.whisky.versionOf ||
				nextProps.whisky.type !== this.props.whisky.type ||
				nextProps.whisky.country !== this.props.whisky.country ||
				nextProps.whisky.region !== this.props.whisky.region ||
				nextProps.whisky.district !== this.props.whisky.district ||
				nextProps.whisky.brand !== this.props.whisky.brand ||
				nextProps.whisky.distillery !== this.props.whisky.distillery ||
				nextProps.whisky.bottler !== this.props.whisky.bottler ||
				nextProps.whisky.name !== this.props.whisky.name ||
				nextProps.whisky.edition !== this.props.whisky.edition ||
				nextProps.whisky.batch !== this.props.whisky.batch ||
				nextProps.whisky.age !== this.props.whisky.age ||
				nextProps.whisky.vintage !== this.props.whisky.vintage ||
				nextProps.whisky.bottled !== this.props.whisky.bottled ||
				nextProps.whisky.caskType !== this.props.whisky.caskType ||
				nextProps.whisky.caskNumber !== this.props.whisky.caskNumber ||
				nextProps.whisky.strength !== this.props.whisky.strength ||
				nextProps.whisky.size !== this.props.whisky.size ||
				nextProps.whisky.bottles !== this.props.whisky.bottles ||
				nextProps.whisky.note !== this.props.whisky.note ||
				nextProps.whisky.label !== this.props.whisky.label ||
				nextProps.whisky.barcode !== this.props.whisky.barcode ||
				nextProps.whisky.isUncolored !== this.props.whisky.isUncolored ||
				nextProps.whisky.isNonChillfiltered !== this.props.whisky.isNonChillfiltered ||
				nextProps.whisky.isCaskStrength !== this.props.whisky.isCaskStrength ||
				nextProps.whisky.isSingleCask !== this.props.whisky.isSingleCask){
				return true;
			}

			if (nextProps.whisky.links !== this.props.whisky.links ||
				nextProps.whisky.images !== this.props.whisky.images ||
				nextProps.whisky.prices !== this.props.whisky.prices) {
				return true;
			}

			if (nextProps.whisky.links && this.props.whisky.links &&
				nextProps.whisky.images && this.props.whisky.images &&
				nextProps.whisky.prices && this.props.whisky.prices &&
				(nextProps.whisky.links.length !== this.props.whisky.links.length ||
				nextProps.whisky.images.length !== this.props.whisky.images.length ||
				nextProps.whisky.prices.length !== this.props.whisky.prices.length)) {
				return true;
			}

			if (nextProps.whisky.links && this.props.whisky.links) {
				for (let i = 0; i < nextProps.whisky.links.length; i++) {
					if (nextProps.whisky.links[i] !== this.props.whisky.links[i]) {
						return true;
					}
				}
			}

			if (nextProps.whisky.images && this.props.whisky.images) {
				for (let i = 0; i < nextProps.whisky.images.length; i++) {
					if (nextProps.whisky.images[i] !== this.props.whisky.images[i]) {
						return true;
					}
				}
			}

			if (nextProps.whisky.prices && this.props.whisky.prices) {
				for (let i = 0; i < nextProps.whisky.links.length; i++) {
					if (nextProps.whisky.prices[i].currency !== this.props.whisky.prices[i].currency ||
						nextProps.whisky.prices[i].value !== this.props.whisky.prices[i].value) {
						return true;
					}
				}
			}
		}

		return false;
	}

	render() {
		if (!this.props) {
			return null;
		}

		let {whisky} = this.props;

		if (!whisky) {
			return null;
		}

		let isSingleMalt = whisky.type === 'Single Malt Whisky';
		let [titlePart1, titlePart2, subtitle] = getWhiskyName(whisky);
		let regionClass;

		if (titlePart2 && subtitle) {
			titlePart2 = `${titlePart2} `;
		}

		if (whisky.region) {
			regionClass = styles[whisky.region.toLowerCase()];
		}

		return <a className={styles.root + (regionClass ? ` ${regionClass}` : '') + (isSingleMalt ? ` ${styles.isSingleMalt}` : '')} href={href('whiskies', whisky.readableId)} onClick={this.handleClick}>
			<span className={styles.title}>{titlePart1 ? titlePart1 : null}{titlePart2 ? <b>{titlePart2}</b> : null}</span>
			<span className={styles.subtitle}>{subtitle ? subtitle : null}</span>
		</a>;
	}

	handleClick = (event) => {
		if (!this.props) {
			return;
		}

		let {whisky} = this.props;

		if (whisky && event.button !== 1) {
			let url = href('whiskies', whisky.readableId);

			if (isUrlInternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}
	};
}
