import Inferno from 'inferno';
import Component from 'inferno-component';

import styles from './Updates.css';


export default class Updates extends Component<{}, {}> {
	render() {
		return <main className={styles.root}>
			<h2 className={styles.heading}>
				Updates
			</h2>

			<h3 className={styles.subtitle}>0.7.1 – 0.7.2</h3>
			<p>We fixed some bugs.</p>

			<h3 className={styles.subtitle}>0.7.0</h3>
			<p>New places rated:</p>
			<ul className={styles.list}>
				<li>Restaurace Rubín</li>
				<li>Restaurace STAVBA</li>
			</ul>

			<h3 className={styles.subtitle}>0.6.0 – 0.6.1</h3>
			<p>App performance was increased.</p>
			<p>We fixed some bugs.</p>

			<h3 className={styles.subtitle}>0.5.0</h3>
			<p>We added menu and about &amp; update pages</p>
			<p>New places rated:</p>
			<ul className={styles.list}>
				<li>Jedna báseň</li>
				<li>Campus River</li>
				<li>Charlie’s Square</li>
				<li>Pivnice U Čápa</li>
				<li>Stopkova Plzeňská Pivnice</li>
				<li>Dominik PUB</li>
				<li>Runway Bar</li>
			</ul>

			<h3 className={styles.subtitle}>0.4.0</h3>
			<p>Whiskies and places now have their own pages with all the information.</p>

			<h3 className={styles.subtitle}>0.3.0</h3>
			<p>App performance was increased.</p>
			<p>New places rated:</p>
			<ul className={styles.list}>
				<li>Zelená Kočka Pivárium</li>
			</ul>

			<h3 className={styles.subtitle}>0.2.0</h3>
			<p>You can now sort and filter places.</p>

			<h3 className={styles.subtitle}>0.1.4</h3>
			<p>New places rated:</p>
			<ul className={styles.list}>
				<li>The Whisky Shop</li>
			</ul>

			<h3 className={styles.subtitle}>0.1.2 – 0.1.3</h3>
			<p>We fixed some bugs.</p>

			<h3 className={styles.subtitle}>0.1.1</h3>
			<p>We added HTTPS support.</p>

			<h3 className={styles.subtitle}>0.1.0</h3>
			<p>Initial version.</p>
			<p>Places rated:</p>
			<ul className={styles.list}>
				<li>1920 London</li>
				<li>Air Cafe</li>
				<li>Bar, který neexistuje</li>
				<li>Bat bar</li>
				<li>Brňák bar</li>
				<li>Clubwash</li>
				<li>Cohiba Club Conti</li>
				<li>Craig Bar</li>
				<li>Fléda</li>
				<li>Jakoby</li>
				<li>JBM Brew Lab Pub</li>
				<li>Lucky Bastard Beerhouse</li>
				<li>Malej Velkej Bar</li>
				<li>Pivní panorama</li>
				<li>SHOT Bar</li>
				<li>SKØG Urban Hub</li>
				<li>Soul Bistro</li>
				<li>Spirit Bar</li>
				<li>St. Patrick Irish Pub</li>
				<li>Super Panda Circus</li>
				<li>Tusto</li>
				<li>U Třech Čertů</li>
			</ul>
		</main>;
	}
}
