import Inferno from 'inferno';
import Component from 'inferno-component';

import styles from './WhiskiesList.css';
import WhiskyName from './WhiskyName';
import tableStyles from './Table.css';
import languageStream from '../streams/languageStream';
import dataStream from '../streams/dataStream';
import Stream from '../libs/Stream';


export default class WhiskiesList extends Component<{}, {}> {
	onLanguageStream: Stream<{}>;
	onDataStream: Stream<{}>;

	componentDidMount() {
		this.onLanguageStream = languageStream.on(() => {
			this.forceUpdate();
		});
		this.onDataStream = dataStream.on(() => {
			this.forceUpdate();
		});
	}

	componentDidUnmount() {
		this.onLanguageStream.end();
		this.onDataStream.end();
	}

	render() {
		let {whiskies} = dataStream.value;

		let whiskyElements = whiskies.map((whisky, bottleIndex) => {
			return whisky ? <li key={whisky.id} className={`${tableStyles.subitem} ${styles.tableSubitem}`}>
				<WhiskyName whisky={whisky} />
			</li> : null;
		});

		return <main className={styles.root}>
			<h2 className={styles.heading}>
				<span className={styles.title}>Whiskies</span>
			</h2>
			
			{whiskyElements && whiskyElements.length ? <ol className={`${tableStyles.subitems} ${styles.tableSubitems}`}>
				{whiskyElements}
			</ol> : null}
		</main>;
	}
}
