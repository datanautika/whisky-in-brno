import Inferno from 'inferno';
import Component from 'inferno-component';

import styles from './Button.css';


export default function ButtonGroup(props) {
	return <div className={styles.buttonGroup}>{props.children}</div>;
}
