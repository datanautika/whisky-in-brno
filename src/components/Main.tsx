import _ from 'lodash';
import Inferno from 'inferno';
import Component from 'inferno-component';

import styles from './Main.css';
import dataStream from '../streams/dataStream';
import WhiskyDetail from './WhiskyDetail';
import WhiskiesList from './WhiskiesList';
import PlacesList from './PlacesList';
import PlaceDetail from './PlaceDetail';
import About from './About';
import Updates from './Updates';
import pageStream from '../streams/pageStream';
import subpageStream from '../streams/subpageStream';
import Stream from '../libs/Stream';


export default class Main extends Component<{}, {}> {
	onPageStream: Stream<{}>;
	onSubpageStream: Stream<{}>;

	componentDidMount() {
		this.onPageStream = pageStream.on(() => {
			requestAnimationFrame(() => this.forceUpdate());
		});
		this.onSubpageStream = subpageStream.on(() => {
			requestAnimationFrame(() => this.forceUpdate());
		});
	}

	componentDidUnmount() {
		this.onPageStream.end();
		this.onSubpageStream.end();
	}

	render() {
		let {places, whiskies} = dataStream.value;
		let {current: page} = pageStream.value;
		let {current: subpage} = subpageStream.value;
		let pageElement = null;

		if (page === 'whiskies' && subpage) {
			let whisky = _.find(whiskies, {readableId: subpage});

			pageElement = whisky ? <WhiskyDetail whisky={whisky} /> : null;
		} else if (page === 'whiskies' && !subpage) {
			pageElement = <WhiskiesList />;
		} else if (page === 'places' && subpage) {
			let place = _.find(places, {readableId: subpage});

			pageElement = place ? <PlaceDetail place={place} /> : null;
		} else if (page === 'about') {
			pageElement = <About />;
		} else if (page === 'updates') {
			pageElement = <Updates />;
		} else {
			pageElement = <PlacesList />;
		}

		return pageElement;
	}
}
