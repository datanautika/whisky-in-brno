import Inferno from 'inferno';
import Component from 'inferno-component';

import styles from './Header.css';
import href from '../utils/href';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';
import isUrlInternal from '../internals/isUrlInternal';


let browserRouter = new BrowserRouter(router);

export default class Header extends Component<{}, {}> {
	render() {
		return <div className={styles.root} onClick={this.handleClick}>
			<h1><a href={href('')}>Whisky in Brno</a></h1>
		</div>;
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (isUrlInternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}
	};
}
