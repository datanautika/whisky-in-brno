import Inferno from 'inferno';
import Component from 'inferno-component';

import styles from './Input.css';


const ENTER_KEY_CODE = 13;

export interface InputProps {
	type?: 'text' | 'email';
	id?: string;
	name?: string;
	autocomplete?: boolean;
	value?: string;
	isValid?: boolean;
	isInvalid?: boolean;
	isDisabled?: boolean;
	handleChange?: (value: any) => void;
	handleSave?: (value: any) => void;
	validator?: (value: any) => void;
}

export default class Input extends Component<InputProps, {}> {
	render() {
		let inputProps = {
			key: this.props ? this.props.id || this.props.name : '',
			className: styles.default + (this.props && this.props.isValid ? ' isValid' : '') + (this.props && this.props.isInvalid ? ' isInvalid' : '') + (this.props && this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
			type: 'text',
			name: this.props ? this.props.name || this.props.id : '',
			id: this.props ? this.props.id || this.props.name : '',
			value: this.props ? this.props.value : '',
			onBlur: this.handleFocusOut,
			onChange: this.handleInput,
			onKeyDown: this.handleKeyDown
		};

		if (this.props && this.props.type === 'email') {
			inputProps.type = this.props.type;
		}

		if (this.props && this.props.isDisabled) {
			(inputProps as any).disabled = 'disabled';
		}

		if (this.props && this.props.autocomplete === false) {
			(inputProps as any).autocomplete = 'off';
		}

		return <input {...inputProps} />;
	}

	handleInput = (event) => {
		if (this.props && this.props.handleChange) {
			this.props.handleChange(this.validate(event.target.value));
		}
	};

	handleFocusOut = (event) => {
		if (this.props && this.props.handleSave) {
			this.props.handleSave(this.validate(event.target.value));
		}
	};

	handleKeyDown = (event) => {
		if (event.keyCode === ENTER_KEY_CODE) {
			if (this.props && this.props.handleSave) {
				this.props.handleSave(this.validate(event.target.value));
			}
		}
	};

	validate(value) {
		return this.props && this.props.validator ? this.props.validator(value) : value;
	}
}
