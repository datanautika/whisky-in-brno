import Inferno from 'inferno';
import Component from 'inferno-component';

import styles from './About.css';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';
import href from '../utils/href';
import isUrlInternal from '../internals/isUrlInternal';


let browserRouter = new BrowserRouter(router);

export default class About extends Component<{}, {}> {
	render() {
		return <main className={styles.root} onClick={this.handleClick}>
			<h2 className={styles.heading}>About</h2>
			<p>We at Datanautika love two things: data and whisky. Every Friday we go out and enjoy a dram or two.</p>
			<p>There’s a lot of bars and pubs in Brno. That’s great! But with more options come more decisions to make. Which bar has the best atmosphere? Where is the most helpful staff? Where to go if we are in the mood for some whisky with sherry cask finish? <a href="http://algorithmstoliveby.com/">Algorithms</a> teach us that if we have enough time, we should try new stuff. So we explore, and you can enjoy our findings.</p>
			<p>Have you ever roamed late night Brno and wondered which bar is open, so you can sit, order Lagavulin and imagine Islay beaches?</p>
			<p>We created this web app to help you on such nights. We list <a href={href('places')}>all the places</a> we visit and record all the relevant data. Some of our ratings are subjective, but we focus primarily on the whiskies: where to get them and for what price.</p>

			<h2 className={styles.subheading}>Whisky rating</h2>
			<p>To measure how adequate the whisky prices are, we calculate <em>whisky rating</em> for each place. But how exactly?</p>
			<p>The price of a dram in an establishment is compared to the cost of a dram you would pay if you bought the whole bottle in a wholesale store. We average these ratios for each place in our database, and also compute an overall mean and standard deviation. Then we standardize them, so the overall mean is 100 and standard deviation is 50.</p>
			<p>We can compare these whisky ratings: if the whisky rating is 100, it means that the prices in the establishment are average compared to all of the places. If the whisky rating is e.g. 130, that means the whisky there is cheaper. But remember, we are still talking about relative prices. Some place could sell only low cost whiskies, but over-price them, and therefore its whisky rating would be low. Conversely, if some place sells expensive whiskies with low profit margin, the whisky rating will be higher.</p>
			<p>Also, if you select some filter – let’s say you are interested only in Islay whiskies – the whisky ratings is recalculated to only include this subset of dram prices.</p>

			<h2 className={styles.subheading}>Place rating</h2>
			<p>While the whisky rating is more or less an objective measure, we don’t just want to go where the prices are low. Good bar offers a space where you can really <em>enjoy</em> your dram.</p>
			<p>Therefore, we also rate the places and compute <em>place rating</em>. Instead of price ratios, we award points for various criteria: how informed the staff is, how nice the ambient is, what are the opening hours, etc. You can find all the details on each place’s page.</p>
			<p>Place ratings are standardized too: if the place rating is 100, it means that the establishment is average compared to all of the places. If the place rating is e.g. 130, that means we found the establishment better than the others for some reason.</p>

			<h2 className={styles.subheading}>Disclaimer</h2>
			<p>We try our best to be impartial, but we probably aren’t.</p>
			<p>None of our visits (nor the created content) are sponsored in any way.</p>
			<p>We don’t guarantee our data are complete and that everything is up-to-date.</p>

		</main>;
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (isUrlInternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}
	};
}
