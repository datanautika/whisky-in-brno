import Stream from '../libs/Stream';


export interface LocationStreamValue {
	latitude: number;
	longitude: number;
}

let locationStream: Stream<LocationStreamValue> = new Stream({
	latitude: 49.194924,
	longitude: 16.608363
});

let navigator = (global as any).navigator as Navigator;

if (navigator && navigator.geolocation) {
	navigator.geolocation.getCurrentPosition((position) => {
		locationStream.push({
			latitude: position.coords.latitude,
			longitude: position.coords.longitude
		});
	});

	let watchId = navigator.geolocation.watchPosition((position) => {
		locationStream.push({
			latitude: position.coords.latitude,
			longitude: position.coords.longitude
		});
	});
}

(global as any).locationStream = locationStream;

export default locationStream;
