import geolib from 'geolib';
import _ from 'lodash';

import Stream from '../libs/Stream';
import {places, placeTypes, whiskies, whiskyRegions, whiskyBrands, whiskyRatingsMean, whiskyRatingsSd} from '../data';
import filterStream from './filterStream';
import locationStream from './locationStream';
import getPlaceWhiskyRating from '../utils/getPlaceWhiskyRating';
import {Place, Places, Whisky, Whiskies, WhiskyRegions, PlaceTypes, WhiskyBrands} from '../types/data';



export interface DataStreamValue {
	places: Places;
	placeTypes: PlaceTypes;
	whiskies: Whiskies;
	whiskyRegions: WhiskyRegions;
	whiskyBrands: WhiskyBrands;
}

function sortPlacesByDistance(placesToSort) {
	return placesToSort.sort((placeA, placeB) => placeA.distance - placeB.distance);
}

let dataStream: Stream<DataStreamValue> = new Stream({places, placeTypes, whiskies, whiskyRegions, whiskyBrands});

dataStream.combine((self, changed) => {
	if (changed.includes(locationStream)) {
		let value = dataStream.value;
		let location = locationStream.value;
		let {filter} = filterStream.value;

		if (location.latitude && location.longitude) {
			places.forEach((place) => {
				place.distance = geolib.getDistance(location, place);
			});
		}

		if (filter && filter.sortPlacesBy === 'distance') {
			sortPlacesByDistance(value.places);
		}

		self.push({
			places: value.places,
			placeTypes, whiskies, whiskyRegions, whiskyBrands
		});
	}

	if (changed.includes(filterStream)/* || !self.hasValue*/) {
		let {filter} = filterStream.value;
		let sortedPlaces = places;

		sortedPlaces.forEach((place) => {
			if (place.review) {
				place.review.bottles = place.review.allBottles;
			}
		});

		if (filter.whiskyRegion) {
			sortedPlaces = sortedPlaces.filter((place) => place.review && place.review.bottles && place.review.bottles.some((bottle) => !!bottle.whisky && bottle.whisky.region === filter.whiskyRegion));
			sortedPlaces.forEach((place) => {
				if (place.review && place.review.bottles) {
					place.review.bottles = place.review.bottles.filter((bottle) => !!bottle.whisky && bottle.whisky.region === filter.whiskyRegion);
				}
			});
		}

		if (filter.whiskyBrand) {
			sortedPlaces = sortedPlaces.filter((place) => place.review && place.review.bottles && place.review.bottles.some((bottle) => !!bottle.whisky && (bottle.whisky.brand === filter.whiskyBrand || bottle.whisky.bottler === filter.whiskyBrand || bottle.whisky.distillery === filter.whiskyBrand)));
			sortedPlaces.forEach((place) => {
				if (place.review && place.review.bottles) {
					place.review.bottles = place.review.bottles.filter((bottle) => !!bottle.whisky && (bottle.whisky.brand === filter.whiskyBrand || bottle.whisky.bottler === filter.whiskyBrand || bottle.whisky.distillery === filter.whiskyBrand));
				}
			});
		}

		sortedPlaces.forEach((place) => {
			place.whiskyRating = getPlaceWhiskyRating(place, whiskyRatingsMean, whiskyRatingsSd);
		});

		if (filter.placeType) {
			sortedPlaces = sortedPlaces.filter((place) => place.type === filter.placeType);
		}

		if (filter.sortPlacesBy === 'rating') {
			sortedPlaces.sort((placeA, placeB) => {
				let isPlaceAWhiskyRatingFinite = _.isFinite(placeA.whiskyRating);
				let isPlaceBWhiskyRatingFinite = _.isFinite(placeB.whiskyRating);

				if (!isPlaceAWhiskyRatingFinite && !isPlaceBWhiskyRatingFinite) {
					return 0;
				}

				if (!isPlaceAWhiskyRatingFinite) {
					return 1;
				}

				if (!isPlaceBWhiskyRatingFinite) {
					return -1;
				}

				return (placeB.whiskyRating as number) - (placeA.whiskyRating as number);
			});
		} else if (filter.sortPlacesBy === 'distance') {
			sortPlacesByDistance(sortedPlaces);
		} else if (filter.sortPlacesBy === 'name') {
			sortedPlaces.sort((placeA, placeB) => placeA.name.localeCompare(placeB.name));
		}

		if (filter.sortWhiskiesBy === 'price') {
			sortedPlaces.forEach((place) => {
				if (place.review && place.review.bottles) {
					place.review.bottles.sort((bottleA, bottleB) => {
						let isBottleAWhiskyRatingFinite = bottleA.offers[0] ? _.isFinite(bottleA.offers[0].price.value) : false;
						let isBottleBWhiskyRatingFinite = bottleB.offers[0] ? _.isFinite(bottleB.offers[0].price.value) : false;

						if (!isBottleAWhiskyRatingFinite && !isBottleBWhiskyRatingFinite) {
							return 0;
						}

						if (!isBottleAWhiskyRatingFinite) {
							return 1;
						}

						if (!isBottleBWhiskyRatingFinite) {
							return -1;
						}

						return bottleA.offers[0].price.value - bottleB.offers[0].price.value;
					});
				}
			});
		} else if (filter.sortWhiskiesBy === 'score') {
			sortedPlaces.forEach((place) => {
				if (place.review && place.review.bottles) {
					place.review.bottles.sort((bottleA, bottleB) => {
						let isBottleAWhiskyRatingFinite = bottleA.offers[0] ? _.isFinite(bottleA.offers[0].score) : false;
						let isBottleBWhiskyRatingFinite = bottleB.offers[0] ? _.isFinite(bottleB.offers[0].score) : false;

						if (!isBottleAWhiskyRatingFinite && !isBottleBWhiskyRatingFinite) {
							return 0;
						}

						if (!isBottleAWhiskyRatingFinite) {
							return 1;
						}

						if (!isBottleBWhiskyRatingFinite) {
							return -1;
						}

						return (bottleA.offers[0].score as number) - (bottleB.offers[0].score as number);
					});
				}
			});
		} else if (filter.sortWhiskiesBy === 'name') {
			sortedPlaces.forEach((place) => {
				if (place.review && place.review.bottles) {
					place.review.bottles.sort((bottleA, bottleB) => {
						if (bottleA.whisky && bottleB.whisky && bottleA.whisky.sortName && bottleB.whisky.sortName) {
							return bottleA.whisky.sortName.localeCompare(bottleB.whisky.sortName);
						}

						return 0;
					});
				}
			});
		}

		self.push({
			places: sortedPlaces,
			placeTypes, whiskies, whiskyRegions, whiskyBrands
		});
	}
}, filterStream, locationStream);

(global as any).dataStream = dataStream;

export default dataStream;
