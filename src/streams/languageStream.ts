import moment from 'moment';

import I18n from '../libs/I18n';
import Stream from '../libs/Stream';
import routeStream from './routeStream';
import constants from '../internals/constants';
import config from '../config';


const EN_US = constants.EN_US;
const CS_CZ = constants.CS_CZ;
const EN = constants.EN;
const CS = constants.CS;

export interface LanguageStreamValue {
	previous: string | null;
	current: string | null;
}

// init i18n
let i18n = new I18n();

(global as any).i18n = i18n;

moment.locale(EN);
i18n.use({
	strings: config.i18nStrings,
	locale: EN_US,
	currency: '$'
});

let languageStram: Stream<LanguageStreamValue> = new Stream({
	previous: null,
	current: null
});

(global as any).languageStram = languageStram;

languageStram.combine((self, changed, dependency) => {
	let value = self.value;
	let {language} = dependency.value;

	if (language !== value.current && (language === CS_CZ || language === EN_US)) {
		let newValue = {
			current: language,
			previous: value.current
		};

		if (language === CS_CZ) {
			moment.locale(CS);
			i18n.use({
				strings: config.i18nStrings,
				locale: CS_CZ,
				currency: 'CZK'
			});
		} else if (language === EN_US) {
			moment.locale(EN);
			i18n.use({
				strings: config.i18nStrings,
				locale: EN_US,
				currency: '$'
			});
		}

		self.push(newValue);
	}
}, routeStream);

export default languageStram;
