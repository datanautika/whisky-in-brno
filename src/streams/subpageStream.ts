import routeStream from './routeStream';
import Stream from '../libs/Stream';


export interface SubpageStreamValue {
	previous: string | null;
	current: string | null;
}

let subpageStream: Stream<SubpageStreamValue> = new Stream({
	previous: null,
	current: null
});

subpageStream.combine((self, changed, dependency) => {
	let value = self.value;
	let {subpage} = dependency.value;

	if (subpage !== value.current) {
		let newValue = {
			current: subpage,
			previous: value.current
		};

		self.push(newValue);
	}
}, routeStream);

export default subpageStream;
