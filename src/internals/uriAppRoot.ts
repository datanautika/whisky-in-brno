const ROUTE_STRIPPER: RegExp = /^[#\/]|\s+$/g;

let uriAppRoot: string;

if (process.env.NODE_ENV === 'production') {
	uriAppRoot = ''.replace(ROUTE_STRIPPER, '');
} else {
	uriAppRoot = ''.replace(ROUTE_STRIPPER, '');
}

export default uriAppRoot;
