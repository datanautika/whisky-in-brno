export default {
	// platform
	CLIENT_PLATFORM: 'client',
	SERVER_PLATFORM: 'server',

	// locale
	EN_US: 'en-US',
	CS_CZ: 'cs-CZ',
	EN: 'en',
	CS: 'cs'
};
