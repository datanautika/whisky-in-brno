import * as url from 'url';


const HTTPS_PORT = 443;
const HTTP_OK = 200;
const HTTP_MOVED_PERMANENTLY = 301;
const HTTP_FOUND = 302;
const HTTP_FORBIDDEN = 403;
const HTTP_METHOD_NOT_ALLOWED = 405;

function portToUrlString({skipDefaultPort, port}) {
	return (skipDefaultPort && port === HTTPS_PORT) ? '' : `:${port}`;
}

export interface ForceHttpsMiddlewareOptions {
	trustProtoHeader?: boolean;
	trustAzureHeader?: boolean;
	port?: number;
	hostname?: string | null;
	skipDefaultPort?: boolean;
	ignoreUrl?: boolean;
	isTemporary?: boolean;
	redirectMethods?: Array<string>;
	internalRedirectMethods?: Array<string>;
	useSpecCompliantDisallow?: boolean;
}

export interface RedirectStatus {
	GET?: number;
	HEAD?: number;
	OPTIONS?: number;
}

export default function forceHttpsMiddleware({
	trustProtoHeader = false,
	trustAzureHeader = false,
	port = HTTPS_PORT,
	hostname = null,
	skipDefaultPort = true,
	ignoreUrl = false,
	isTemporary = false,
	redirectMethods = ['GET', 'HEAD'],
	internalRedirectMethods = [],
	useSpecCompliantDisallow = false
}: ForceHttpsMiddlewareOptions = {}) {
	let redirectStatus: RedirectStatus = {};

	redirectMethods.forEach((x) => {
		redirectStatus[x] = isTemporary ? HTTP_FOUND : HTTP_MOVED_PERMANENTLY;
	});
	internalRedirectMethods.forEach((x) => {
		redirectStatus[x] = 307;
	});
	
	redirectStatus.OPTIONS = 0;

	return async (context, next) => {
		// First, check if directly requested via https
		let secure = context.secure;

		// Second, if the request headers can be trusted (e.g. because they are send by a proxy), check if x-forward-proto is set to https
		if (!secure && trustProtoHeader) {
			secure = context.request.header['x-forwarded-proto'] === 'https';
		}

		// Third, if trustAzureHeader is set, check for Azure's headers indicating a SSL connection
		if (!secure && trustAzureHeader && context.request.header['x-arr-ssl']) {
			secure = true;
		}

		if (secure) {
			return await next();
		}

		// Check if method should be disallowed (and handle OPTIONS method)
		if (!redirectStatus[context.method]) {
			if (context.method === 'OPTIONS') {
				context.response.status = HTTP_OK;
			} else {
				context.response.status = useSpecCompliantDisallow ? HTTP_METHOD_NOT_ALLOWED : HTTP_FORBIDDEN;
			}

			context.response.set('Allow', Object.keys(redirectStatus).join());
			context.response.body = '';

			return null;
		}

		// build redirect url
		let httpsHost = hostname || url.parse(`http://${context.request.header.host}`).hostname;
		let redirectTo = `https://${httpsHost}${portToUrlString({skipDefaultPort, port})}`;

		if (!ignoreUrl) {
			redirectTo += context.request.url;
		}

		// redirect to secure
		context.response.status = redirectStatus[context.method];
		context.response.redirect(redirectTo);

		return null;
	};
}
