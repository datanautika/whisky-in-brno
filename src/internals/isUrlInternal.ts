import isUrlExternal from './isUrlExternal';


const DOMAIN_REGEXP = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;
const MAILTO_REGEXP = /^mailto:/i;

export default function isUrlInternal(url) {
	return url && !MAILTO_REGEXP.test(url) && (!DOMAIN_REGEXP.test(url) || !isUrlExternal(url));
}
