import {Moment} from 'moment';


export type WhiskyType = 'Single Malt Whisky' | 'Blended Malt Whisky' | 'Blended Whisky';
export type WhiskyCountry = 'Scotland';
export type WhiskyRegion = 'Highland' | 'Lowland' | 'Speyside' | 'Islay' | 'Islands' | 'Campbeltown';

export type OpeningHours = Array<{
	start: string;
	startTime?: Moment;
	end: string
	endTime?: Moment;
}>;

export interface Price {
	currency: string;
	value: number;
}

export interface Offer {
	size: number;
	price: Price;
	score?: number;
	rating?: number;
}

export interface Bottle {
	whiskyId: number;
	whisky?: Whisky;
	offers: Array<Offer>;
}

export interface PlaceReview {
	raterId: number;
	date: string | Moment;
	glass?: string | null;
	ambient?: number;
	serviceInformedness?: number;
	serviceAmiability?: number;
	restroomCleanness?: number;
	towels?: string;
	isCashOnly?: boolean;
	isSmokingAllowed?: boolean;
	isOperational?: boolean;
	bottles?: Array<Bottle>;
	allBottles?: Array<Bottle>;
}

export interface Place {
	id: number;
	readableId?: string;
	name: string;
	type: string;
	description: {
		[key: string]: string;
	};
	address: string;
	links: Array<string>;
	latitude: number;
	longitude: number;
	openingHours: OpeningHours;
	review?: PlaceReview;
	reviews: Array<PlaceReview>;
	distance?: number;
	isOpen?: boolean;
	whiskyRating?: number;
	placeOpeningHoursScore?: number;
	placeWhiskiesCountScore?: number;
	placeGlassScore?: number;
	placeAmbientScore?: number;
	placeServiceInformednessScore?: number;
	placeServiceAmiabilityScore?: number;
	placeRestroomCleannessScore?: number;
	placeTowelsScore?: number;
	placeIsCashOnlyScore?: number;
	placeIsSmokingAllowedScore?: number;
	placeScore?: number;
	placeRating?: number;
}

export interface Whisky {
	id: number;
	readableId?: string;
	versionOf: number | null;
	type: WhiskyType;
	country: WhiskyCountry;
	region: WhiskyRegion;
	district?: string;
	brand?: string;
	distillery?: string;
	bottler?: string;
	name?: string;
	fullName?: Array<string>;
	sortName?: string;
	edition?: string;
	batch?: string;
	age?: number;
	vintage?: string;
	bottled?: string;
	caskType?: string;
	caskNumber?: string;
	strength?: number;
	size?: number;
	bottles?: number;
	note?: string;
	label?: string;
	barcode?: string;
	bottledFor?: string;
	isUncolored?: boolean;
	isNonChillfiltered?: boolean;
	isCaskStrength?: boolean;
	isSingleCask?: boolean;
	links?: Array<string>;
	images?: Array<string>;
	prices?: Array<Price>;
	places?: Array<Place>;
}

export type Whiskies = Array<Whisky>;
export type Places = Array<Place>;

export type WhiskyBrands = Array<string>;
export type WhiskyRegions = Array<WhiskyRegion>;
export type PlaceTypes = Array<string>;
