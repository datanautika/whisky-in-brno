import './internals/polyfills';
import './internals/cssModules';

import * as fs from 'fs';
import * as path from 'path';
import * as http from 'http';
import * as https from 'https';
import Koa from 'koa';
import compress from 'koa-compress';
import logger from 'koa-logger';
import bodyParser from 'koa-bodyparser';
import serve from 'koa-static';
import Inferno from 'inferno';
import InfernoServer from 'inferno-server';
import Bluebird from 'bluebird';

import appRoot from './internals/appRoot';
import uriAppRoot from './internals/uriAppRoot';
import App from './components/App';
import routerMiddleware from './libs/routerMiddleware';
import router from './streams/router';
import forceHttpsMiddleware from './internals/forceHttpsMiddleware';


const HTTP_NOT_FOUND: number = 404;
const HTTP_INTERNAL_ERROR = 500;
const HTTPS_PORT = 443;

let readFile: (filename: string, encoding: string) => Promise<string> = Bluebird.promisify(fs.readFile) as (...all: any[]) => any;
let app: Koa = new Koa();

console.log('App root dir:', `"${appRoot}"`); // eslint-disable-line no-console
console.log('App root URL:', `"${uriAppRoot}"`); // eslint-disable-line no-console

// force https
app.use(forceHttpsMiddleware({
	trustAzureHeader: true
}));

// parse body
app.use(bodyParser());

// add the logger
app.use(logger());

// handle uncaught errors
app.use(async (context, next) => {
	try {
		await next();
	} catch (error) {
		console.warn(error); // eslint-disable-line no-console

		context.status = error.status || HTTP_INTERNAL_ERROR;
		context.body = {
			error: {
				message: error.message
			}
		};

		if (context.app) {
			context.app.emit('error', error, context);
		}
	}
});

// serve static fiiles
app.use(serve(path.resolve(path.join(appRoot, 'public'))));

// serve the app
app.use(routerMiddleware(router, async (context, next) => {
	if (context.method !== 'HEAD' && context.method !== 'GET') {
		await next();

		return;
	}

	// response is already handled
	if (context.body && context.body !== null || context.status !== HTTP_NOT_FOUND) {
		await next();

		return;
	}

	let componentHTML = InfernoServer.renderToString(<App />);

	context.body = await readFile(path.join(appRoot, 'assets/templates/index.html'), 'utf8');
	context.body = context.body.replace('%{content}', componentHTML).replace(new RegExp('%{appRoot}', 'g'), uriAppRoot);

	await next();
}));

// compress the files
app.use(compress());

// start the servers
const PORT = 8080;

let httpsOptions: {key?: Buffer; cert?: Buffer} = {};

if (process.env.NODE_ENV === 'development') {
	httpsOptions.key = fs.readFileSync('./localhost-key.pem'); // eslint-disable-line no-sync
	httpsOptions.cert = fs.readFileSync('./localhost-cert.pem'); // eslint-disable-line no-sync
}

let serverErrorListener = (error) => {
	console.log(error);
};

http.createServer(app.callback()).listen(process.env.PORT || PORT).on('error', serverErrorListener);
https.createServer(httpsOptions, app.callback()).listen(HTTPS_PORT).on('error', serverErrorListener);

console.log(`Listening on port ${process.env.PORT || PORT}...`); // eslint-disable-line no-console
