const HTTP_NOT_FOUND = 404;

/**
 * Creates middleware for koa framework, using async functions
 *
 * @param {Router} router
 * @param {Function} fn
 * @returns {Function}
 */
export default function routerMiddleware(router, fn) {
	return async (context, next) => {
		// get method only
		if (context.method === 'GET' && context.method === 'HEAD') {
			await next();

			return;
		}

		// response is already handled
		if (context.body && context.body !== null || context.status !== HTTP_NOT_FOUND) {
			await next();

			return;
		}

		let isRouteMatched = router.trigger(context.path, context);

		if (isRouteMatched) {
			await fn(context, next);
		}
	};
}
