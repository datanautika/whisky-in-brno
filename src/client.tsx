import './internals/polyfills';

import $ from 'jquery';
import Inferno from 'inferno';
import Immutable from 'immutable';
import moment from 'moment';

import './components/Page.css';
import App from './components/App';
import Stream from './libs/Stream';
import browserRouter from './streams/browserRouter';


if (process.env.NODE_ENV === 'development') {
	(global as any).Inferno = Inferno;
	(global as any).$ = $;
	(global as any).Immutable = Immutable;
	(global as any).Stream = Stream;
	(global as any).moment = moment;
}

// grid toggle
if (process.env.NODE_ENV === 'development') {
	const G_KEY_CODE = 71;

	$((global as any).document).on('keydown', (event) => {
		let tagName = event.target.tagName.toLowerCase();

		if (event.keyCode === G_KEY_CODE && event.target && tagName !== 'textarea' && tagName !== 'input') {
			$('body').toggleClass('hasGrid');
		}
	});
}

// init router
browserRouter.start();

// render app
let rootNode = document.getElementById('app');

if (rootNode) {
	Inferno.render(<App />, rootNode);
}

$('#app').addClass('isLoaded');

// Google Analytics
const GA_ID = 'UA-90001694-1';

if (process.env.NODE_ENV === 'development') {
	(global as any).ga('create', GA_ID, {
		cookieDomain: 'none'
	});
} else {
	(global as any).ga('create', GA_ID, 'auto');
}
