export default function link(...levels) {
	return `${process.env.APP_ROOT ? `${process.env.APP_ROOT}` : ''}/${levels.join('/')}`;
}
