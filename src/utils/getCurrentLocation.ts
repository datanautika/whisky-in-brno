import Promise from 'bluebird';


export default function getCurrentLocation() {
	return new Promise((resolve, reject) => {
		navigator.geolocation.getCurrentPosition((position) => {
			resolve({
				latitude: position.coords.latitude,
				longitude: position.coords.longitude
			});
		});
	});
}
