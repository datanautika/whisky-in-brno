import filterStream from '../streams/filterStream';


export default function filterToUri(newFilter) {
	return encodeURIComponent(JSON.stringify(Object.assign({}, filterStream.value.filter, newFilter)));
}
