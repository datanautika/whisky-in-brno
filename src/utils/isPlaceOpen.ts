import moment from 'moment';

import parsePlaceOpeningHours from './parsePlaceOpeningHours';


export default function isPlaceOpen(place) {
	if (place.reivew && !place.reivew.isOperational) {
		return false
	}

	let currentTime = moment();

	parsePlaceOpeningHours(place);

	for (let i = 0; i < place.openingHours.length; i++) {
		if (currentTime.isBetween(place.openingHours[i].startTime, place.openingHours[i].endTime) || currentTime.isBetween(place.openingHours[i].startTime.clone().add(1, 'week'), place.openingHours[i].endTime.clone().add(1, 'week')) || currentTime.isBetween(place.openingHours[i].startTime.clone().subtract(1, 'week'), place.openingHours[i].endTime.clone().subtract(1, 'week'))) {
			return true;
		}
	}

	return false;
}
