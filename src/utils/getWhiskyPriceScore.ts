import getWhiskyPrice from '../utils/getWhiskyPrice';


export default function getWhiskyPriceScore(whisky, offer) {
	if (whisky.prices.length) {
		let whiskyPrice = getWhiskyPrice(whisky);

		return offer.price.value / ((offer.size / whisky.size) * whiskyPrice);
	}

	return NaN;
}
