import _ from 'lodash';

import {Whisky, Whiskies} from '../types/data';


export default function findWhisky(whiskies: Whiskies, whiskyId: number): Whisky | undefined {
	return _.cloneDeep(_.find(whiskies, {id: whiskyId}));
}
