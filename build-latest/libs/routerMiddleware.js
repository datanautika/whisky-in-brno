'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = routerMiddleware;

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const HTTP_NOT_FOUND = 404;
function routerMiddleware(router, fn) {
    return (() => {
        var _ref = _asyncToGenerator(function* (context, next) {
            if (context.method === 'GET' && context.method === 'HEAD') {
                yield next();
                return;
            }
            if (context.body && context.body !== null || context.status !== HTTP_NOT_FOUND) {
                yield next();
                return;
            }
            let isRouteMatched = router.trigger(context.path, context);
            if (isRouteMatched) {
                yield fn(context, next);
            }
        });

        return function (_x, _x2) {
            return _ref.apply(this, arguments);
        };
    })();
}