'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
const TYPE_REGEX = /^:([a-z])(\((.+)\))?/;
let buildKey = literals => {
    let stripType = s => s.replace(TYPE_REGEX, '');
    let lastPartialKey = stripType(literals[literals.length - 1]);
    let prependPartialKey = (memo, curr, i) => `${stripType(curr)}{${i}}${memo}`;
    return literals.slice(0, -1).reduceRight(prependPartialKey, lastPartialKey);
};
let buildMessage = (string, ...values) => string.replace(/{(\d)}/g, (_, index) => values[Number(index)]);
let localizeString = (locale, string) => string.toLocaleString(locale);
let localizeCurrency = (locale, string, currency) => string.toLocaleString(locale, {
    style: 'currency',
    currency
});
let localizeNumber = (locale, string, fractionalDigits) => string.toLocaleString(locale, {
    minimumFractionDigits: fractionalDigits,
    maximumFractionDigits: fractionalDigits
});
let extractTypeInfo = literal => {
    let match = TYPE_REGEX.exec(literal);
    if (match) {
        return { type: match[1], options: match[3] };
    }
    return { type: 's', options: '' };
};
let localize = (value, i18n, { type, options }) => {
    let localizedValue;
    if (type === 's') {
        localizedValue = localizeString(i18n.locale, value);
    }
    if (type === 'c') {
        localizedValue = localizeCurrency(i18n.locale, value, options || i18n.currency);
    }
    if (type === 'n') {
        localizedValue = localizeNumber(i18n.locale, value, options);
    }
    return localizedValue;
};
let i18n;
class I18n {
    constructor() {
        this.strings = {};
        this.currency = '';
        this.locale = '';
        i18n = i18n ? i18n : this;
        return i18n;
    }
    use({ strings = {}, currency = '$', locale = 'en-US' } = {}) {
        this.strings = strings;
        this.currency = currency;
        this.locale = locale;
        return this;
    }
    translate(literals, ...values) {
        let translationKey = buildKey(literals);
        let translationString;
        if (this.strings[translationKey]) {
            translationString = this.strings[translationKey][this.locale];
        }
        if (!translationString) {
            translationString = translationKey;
        }
        if (translationString) {
            let typeInfoForValues = literals.slice(1).map(extractTypeInfo);
            let localizedValues = values.map((value, index) => localize(value, this, typeInfoForValues[index]));
            return buildMessage(translationString, ...localizedValues);
        }
        return 'Error: translation missing!';
    }
}
exports.default = I18n;