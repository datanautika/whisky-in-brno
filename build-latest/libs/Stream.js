'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _isFunction = require('../internals/isFunction');

var _isFunction2 = _interopRequireDefault(_isFunction);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let trueFn = () => true;
let streamsToUpdate = [];
let inStream;
let streamOrder = [];
let nextStreamOrderIndex = -1;
let isFlushing = false;
class Stream {
    constructor(value) {
        this.listeners = [];
        this.dependencies = [];
        this.values = [];
        this.isQueued = false;
        this.areDependenciesMet = false;
        this.changedDependencies = [];
        this.shouldUpdate = false;
        this.hasValue = false;
        this.endStream = undefined;
        this.isEndStream = false;
        this.fn = undefined;
        this.push = value => {
            if (value !== undefined && value !== null && (0, _isFunction2.default)(value.then)) {
                value.then(this.push);
                return this;
            }
            this.value = value;
            this.hasValue = true;
            if (inStream === undefined) {
                isFlushing = true;
                this.updateDependencies();
                if (streamsToUpdate.length > 0) {
                    Stream.flushUpdate();
                } else {
                    isFlushing = false;
                }
            } else if (inStream === this) {
                for (let i = 0; i < this.listeners.length; ++i) {
                    if (this.listeners[i].endStream === this) {
                        this.listeners[i].detachDependencies();
                        this.listeners[i].endStream.detachDependencies();
                    } else {
                        this.listeners[i].changedDependencies.push(this);
                        this.listeners[i].shouldUpdate = true;
                    }
                }
            } else {
                this.values.push(value);
                streamsToUpdate.push(this);
            }
            return this;
        };
        if (value === trueFn) {
            this.fn = value;
            this.isEndStream = true;
            this.endStream = undefined;
        } else {
            this.endStream = new Stream(trueFn);
            this.endStream.listeners.push(this);
            if (arguments.length > 0 && typeof value !== 'undefined') {
                this.push(value);
            }
        }
        this.push = this.push.bind(this);
    }
    static flushUpdate() {
        isFlushing = true;
        while (streamsToUpdate.length > 0) {
            let stream = streamsToUpdate.shift();
            if (stream) {
                if (stream.values.length > 0) {
                    stream.value = stream.values.shift();
                }
                stream.updateDependencies();
            }
        }
        isFlushing = false;
    }
    updateStream() {
        if (this.areDependenciesMet !== true && !this.dependencies.every(stream => stream.hasValue) || this.endStream !== undefined && this.endStream.value === true) {
            return this;
        }
        if (inStream !== undefined) {
            streamsToUpdate.push(this);
            return this;
        }
        inStream = this;
        let newValue;
        if (this.fn) {
            newValue = this.fn(this, this.changedDependencies, ...this.dependencies);
        }
        if (newValue !== undefined) {
            this.push(newValue);
        }
        inStream = undefined;
        this.changedDependencies = [];
        this.shouldUpdate = false;
        if (isFlushing === false) {
            Stream.flushUpdate();
        }
        return this;
    }
    updateDependencies() {
        for (let i = 0; i < this.listeners.length; ++i) {
            if (this.listeners[i].endStream === this) {
                this.listeners[i].detachDependencies();
                if (this.listeners[i].endStream) {
                    this.listeners[i].endStream.detachDependencies();
                }
            } else {
                this.listeners[i].changedDependencies.push(this);
                this.listeners[i].shouldUpdate = true;
                this.listeners[i].findDependencies();
            }
        }
        for (; nextStreamOrderIndex >= 0; --nextStreamOrderIndex) {
            if (streamOrder[nextStreamOrderIndex].shouldUpdate === true) {
                streamOrder[nextStreamOrderIndex].updateStream();
            }
            streamOrder[nextStreamOrderIndex].isQueued = false;
        }
    }
    findDependencies() {
        if (this.isQueued === false) {
            this.isQueued = true;
            for (let i = 0; i < this.listeners.length; ++i) {
                this.listeners[i].findDependencies();
            }
            streamOrder[++nextStreamOrderIndex] = this;
        }
    }
    detachDependencies() {
        for (let i = 0; i < this.dependencies.length; ++i) {
            this.dependencies[i].listeners[this.dependencies[i].listeners.indexOf(this)] = this.dependencies[i].listeners[this.dependencies[i].listeners.length - 1];
            this.dependencies[i].listeners.length--;
        }
        this.dependencies.length = 0;
    }
    get() {
        return this.value;
    }
    end() {
        if (this.endStream) {
            this.endStream.push(true);
        }
        return this;
    }
    toString() {
        return 'stream(' + this.value + ')';
    }
    combine(...streams) {
        let i = 0;
        let dependencies;
        let dependenciesEndStreams;
        this.detachDependencies();
        if (this.endStream) {
            this.endStream.detachDependencies();
        }
        dependencies = [];
        dependenciesEndStreams = [];
        if ((0, _isFunction2.default)(streams[i])) {
            this.fn = streams[i];
            i = 1;
        }
        for (; i < streams.length; ++i) {
            if (streams[i] !== undefined) {
                dependencies.push(streams[i]);
                if (streams[i].endStream) {
                    dependenciesEndStreams.push(streams[i].endStream);
                }
            }
        }
        if (dependencies.length) {
            this.dependencies = dependencies;
            this.areDependenciesMet = false;
            this.changedDependencies = [];
            this.shouldUpdate = false;
            for (let j = 0; j < dependencies.length; j++) {
                dependencies[j].listeners.push(this);
            }
            if (this.endStream) {
                this.endStream.listeners.push(this);
            }
            for (let j = 0; j < dependenciesEndStreams.length; j++) {
                dependenciesEndStreams[j].listeners.push(this.endStream);
            }
            if (this.endStream) {
                this.endStream.dependencies = dependenciesEndStreams;
            }
            this.updateStream();
        }
        return this;
    }
    static combine(fn, ...streams) {
        let newStream = new Stream();
        newStream.combine(fn, ...streams);
        return newStream;
    }
    immediate() {
        if (!this.areDependenciesMet) {
            this.areDependenciesMet = true;
            this.updateStream();
        }
        return this;
    }
    static map(fn, stream) {
        return Stream.combine((self, changed, streamDependency) => {
            self.push(fn(streamDependency.value));
        }, stream);
    }
    map(fn) {
        return Stream.map(fn, this);
    }
    static isStream(value) {
        return value instanceof Stream;
    }
    endsOn(endStream) {
        if (this.endStream) {
            this.endStream.detachDependencies();
            endStream.listeners.push(this.endStream);
            this.endStream.dependencies.push(endStream);
        }
        return this;
    }
    static on(fn, stream) {
        return Stream.combine((self, changed, streamDependency) => {
            fn(streamDependency.value);
        }, stream);
    }
    on(fn) {
        return Stream.on(fn, this);
    }
    static subscribe(fn, stream) {
        let omitFirstRun = stream.hasValue;
        let hasRun = false;
        return Stream.combine((self, changed, dependency) => {
            if (hasRun || !omitFirstRun && !hasRun) {
                fn(dependency.value);
            }
            hasRun = true;
        }, stream);
    }
    subscribe(fn) {
        return Stream.subscribe(fn, this);
    }
    static merge(stream1, stream2) {
        let newStream = Stream.combine((self, changed, dependencyStream1, dependencyStream2) => {
            if (changed[0]) {
                self.push(changed[0].get());
            } else if (dependencyStream1.hasValue) {
                self.push(dependencyStream1.value);
            } else if (dependencyStream2.hasValue) {
                self.push(dependencyStream2.value);
            }
        }, stream1, stream2).immediate();
        let endStream = Stream.combine(trueFn, stream1.endStream, stream2.endStream);
        newStream.endsOn(endStream);
        return newStream;
    }
    ap(stream) {
        return Stream.combine((self, changed, dependency1) => {
            if (dependency1.value) {
                self.push(dependency1.value(stream.value));
            }
        }, this, stream);
    }
    static of(value) {
        return new Stream(value);
    }
}
exports.default = Stream;