'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _dataStream = require('../streams/dataStream');

var _dataStream2 = _interopRequireDefault(_dataStream);

var _WhiskyDetail = require('./WhiskyDetail');

var _WhiskyDetail2 = _interopRequireDefault(_WhiskyDetail);

var _WhiskiesList = require('./WhiskiesList');

var _WhiskiesList2 = _interopRequireDefault(_WhiskiesList);

var _PlacesList = require('./PlacesList');

var _PlacesList2 = _interopRequireDefault(_PlacesList);

var _PlaceDetail = require('./PlaceDetail');

var _PlaceDetail2 = _interopRequireDefault(_PlaceDetail);

var _About = require('./About');

var _About2 = _interopRequireDefault(_About);

var _Updates = require('./Updates');

var _Updates2 = _interopRequireDefault(_Updates);

var _pageStream = require('../streams/pageStream');

var _pageStream2 = _interopRequireDefault(_pageStream);

var _subpageStream = require('../streams/subpageStream');

var _subpageStream2 = _interopRequireDefault(_subpageStream);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Main extends _infernoComponent2.default {
    componentDidMount() {
        this.onPageStream = _pageStream2.default.on(() => {
            requestAnimationFrame(() => this.forceUpdate());
        });
        this.onSubpageStream = _subpageStream2.default.on(() => {
            requestAnimationFrame(() => this.forceUpdate());
        });
    }
    componentDidUnmount() {
        this.onPageStream.end();
        this.onSubpageStream.end();
    }
    render() {
        let { places, whiskies } = _dataStream2.default.value;
        let { current: page } = _pageStream2.default.value;
        let { current: subpage } = _subpageStream2.default.value;
        let pageElement = null;
        if (page === 'whiskies' && subpage) {
            let whisky = _lodash2.default.find(whiskies, { readableId: subpage });
            pageElement = whisky ? (0, _inferno.createVNode)(16, _WhiskyDetail2.default, null, null, {
                'whisky': whisky
            }) : null;
        } else if (page === 'whiskies' && !subpage) {
            pageElement = (0, _inferno.createVNode)(16, _WhiskiesList2.default);
        } else if (page === 'places' && subpage) {
            let place = _lodash2.default.find(places, { readableId: subpage });
            pageElement = place ? (0, _inferno.createVNode)(16, _PlaceDetail2.default, null, null, {
                'place': place
            }) : null;
        } else if (page === 'about') {
            pageElement = (0, _inferno.createVNode)(16, _About2.default);
        } else if (page === 'updates') {
            pageElement = (0, _inferno.createVNode)(16, _Updates2.default);
        } else {
            pageElement = (0, _inferno.createVNode)(16, _PlacesList2.default);
        }
        return pageElement;
    }
}
exports.default = Main;