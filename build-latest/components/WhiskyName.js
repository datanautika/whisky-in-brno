'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _WhiskyName = require('./WhiskyName.css');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _getWhiskyName = require('../utils/getWhiskyName');

var _getWhiskyName2 = _interopRequireDefault(_getWhiskyName);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _isUrlInternal = require('../internals/isUrlInternal');

var _isUrlInternal2 = _interopRequireDefault(_isUrlInternal);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let browserRouter = new _BrowserRouter2.default(_router2.default);
class WhiskyName extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.handleClick = event => {
            if (!this.props) {
                return;
            }
            let { whisky } = this.props;
            if (whisky && event.button !== 1) {
                let url = (0, _href2.default)('whiskies', whisky.readableId);
                if ((0, _isUrlInternal2.default)(url)) {
                    event.preventDefault();
                    browserRouter.navigate(url);
                }
            }
        };
    }
    shouldComponentUpdate(nextProps) {
        if (nextProps && this.props) {
            if (nextProps.whisky.id !== this.props.whisky.id || nextProps.whisky.versionOf !== this.props.whisky.versionOf || nextProps.whisky.type !== this.props.whisky.type || nextProps.whisky.country !== this.props.whisky.country || nextProps.whisky.region !== this.props.whisky.region || nextProps.whisky.district !== this.props.whisky.district || nextProps.whisky.brand !== this.props.whisky.brand || nextProps.whisky.distillery !== this.props.whisky.distillery || nextProps.whisky.bottler !== this.props.whisky.bottler || nextProps.whisky.name !== this.props.whisky.name || nextProps.whisky.edition !== this.props.whisky.edition || nextProps.whisky.batch !== this.props.whisky.batch || nextProps.whisky.age !== this.props.whisky.age || nextProps.whisky.vintage !== this.props.whisky.vintage || nextProps.whisky.bottled !== this.props.whisky.bottled || nextProps.whisky.caskType !== this.props.whisky.caskType || nextProps.whisky.caskNumber !== this.props.whisky.caskNumber || nextProps.whisky.strength !== this.props.whisky.strength || nextProps.whisky.size !== this.props.whisky.size || nextProps.whisky.bottles !== this.props.whisky.bottles || nextProps.whisky.note !== this.props.whisky.note || nextProps.whisky.label !== this.props.whisky.label || nextProps.whisky.barcode !== this.props.whisky.barcode || nextProps.whisky.isUncolored !== this.props.whisky.isUncolored || nextProps.whisky.isNonChillfiltered !== this.props.whisky.isNonChillfiltered || nextProps.whisky.isCaskStrength !== this.props.whisky.isCaskStrength || nextProps.whisky.isSingleCask !== this.props.whisky.isSingleCask) {
                return true;
            }
            if (nextProps.whisky.links !== this.props.whisky.links || nextProps.whisky.images !== this.props.whisky.images || nextProps.whisky.prices !== this.props.whisky.prices) {
                return true;
            }
            if (nextProps.whisky.links && this.props.whisky.links && nextProps.whisky.images && this.props.whisky.images && nextProps.whisky.prices && this.props.whisky.prices && (nextProps.whisky.links.length !== this.props.whisky.links.length || nextProps.whisky.images.length !== this.props.whisky.images.length || nextProps.whisky.prices.length !== this.props.whisky.prices.length)) {
                return true;
            }
            if (nextProps.whisky.links && this.props.whisky.links) {
                for (let i = 0; i < nextProps.whisky.links.length; i++) {
                    if (nextProps.whisky.links[i] !== this.props.whisky.links[i]) {
                        return true;
                    }
                }
            }
            if (nextProps.whisky.images && this.props.whisky.images) {
                for (let i = 0; i < nextProps.whisky.images.length; i++) {
                    if (nextProps.whisky.images[i] !== this.props.whisky.images[i]) {
                        return true;
                    }
                }
            }
            if (nextProps.whisky.prices && this.props.whisky.prices) {
                for (let i = 0; i < nextProps.whisky.links.length; i++) {
                    if (nextProps.whisky.prices[i].currency !== this.props.whisky.prices[i].currency || nextProps.whisky.prices[i].value !== this.props.whisky.prices[i].value) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    render() {
        if (!this.props) {
            return null;
        }
        let { whisky } = this.props;
        if (!whisky) {
            return null;
        }
        let isSingleMalt = whisky.type === 'Single Malt Whisky';
        let [titlePart1, titlePart2, subtitle] = (0, _getWhiskyName2.default)(whisky);
        let regionClass;
        if (titlePart2 && subtitle) {
            titlePart2 = `${titlePart2} `;
        }
        if (whisky.region) {
            regionClass = _WhiskyName2.default[whisky.region.toLowerCase()];
        }
        return (0, _inferno.createVNode)(2, 'a', _WhiskyName2.default.root + (regionClass ? ` ${regionClass}` : '') + (isSingleMalt ? ` ${_WhiskyName2.default.isSingleMalt}` : ''), [(0, _inferno.createVNode)(2, 'span', _WhiskyName2.default.title, [titlePart1 ? titlePart1 : null, titlePart2 ? (0, _inferno.createVNode)(2, 'b', null, titlePart2) : null]), (0, _inferno.createVNode)(2, 'span', _WhiskyName2.default.subtitle, subtitle ? subtitle : null)], {
            'href': (0, _href2.default)('whiskies', whisky.readableId),
            'onClick': this.handleClick
        });
    }
}
exports.default = WhiskyName;