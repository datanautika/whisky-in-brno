'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _Header = require('./Header');

var _Header2 = _interopRequireDefault(_Header);

var _Breadcrumbs = require('./Breadcrumbs');

var _Breadcrumbs2 = _interopRequireDefault(_Breadcrumbs);

var _Main = require('./Main');

var _Main2 = _interopRequireDefault(_Main);

var _Footer = require('./Footer');

var _Footer2 = _interopRequireDefault(_Footer);

var _CookieLawBanner = require('./CookieLawBanner');

var _CookieLawBanner2 = _interopRequireDefault(_CookieLawBanner);

var _App = require('./App.css');

var _App2 = _interopRequireDefault(_App);

var _localStore = require('../libs/localStore');

var _localStore2 = _interopRequireDefault(_localStore);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class App extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.state = {
            isCookieLawBannerHidden: true
        };
        this.hideCookieLawBanner = () => {
            _localStore2.default.set('isCookieLawBannerHidden', true);
            this.setState({ isCookieLawBannerHidden: true });
            this.forceUpdate();
        };
    }
    render() {
        return (0, _inferno.createVNode)(2, 'div', _App2.default.root, [(0, _inferno.createVNode)(16, _Header2.default), (0, _inferno.createVNode)(16, _Breadcrumbs2.default), (0, _inferno.createVNode)(16, _Main2.default), this.state.isCookieLawBannerHidden === true ? null : (0, _inferno.createVNode)(16, _CookieLawBanner2.default, null, null, {
            'handleHideBanner': this.hideCookieLawBanner
        }), (0, _inferno.createVNode)(16, _Footer2.default)]);
    }
    componentDidMount() {
        if (!_localStore2.default.get('isCookieLawBannerHidden')) {
            this.setState({ isCookieLawBannerHidden: false });
        }
    }
}
exports.default = App;