'use strict';

Object.defineProperty(exports, "__esModule", {
				value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _About = require('./About.css');

var _About2 = _interopRequireDefault(_About);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _isUrlInternal = require('../internals/isUrlInternal');

var _isUrlInternal2 = _interopRequireDefault(_isUrlInternal);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let browserRouter = new _BrowserRouter2.default(_router2.default);
class About extends _infernoComponent2.default {
				constructor() {
								super(...arguments);
								this.handleClick = event => {
												if (event.button !== 1) {
																let url = event.target.getAttribute('href');
																if ((0, _isUrlInternal2.default)(url)) {
																				event.preventDefault();
																				browserRouter.navigate(url);
																}
												}
								};
				}
				render() {
								return (0, _inferno.createVNode)(2, 'main', _About2.default.root, [(0, _inferno.createVNode)(2, 'h2', _About2.default.heading, 'About'), (0, _inferno.createVNode)(2, 'p', null, 'We at Datanautika love two things: data and whisky. Every Friday we go out and enjoy a dram or two.'), (0, _inferno.createVNode)(2, 'p', null, ['There\u2019s a lot of bars and pubs in Brno. That\u2019s great! But with more options come more decisions to make. Which bar has the best atmosphere? Where is the most helpful staff? Where to go if we are in the mood for some whisky with sherry cask finish? ', (0, _inferno.createVNode)(2, 'a', null, 'Algorithms', {
												'href': 'http://algorithmstoliveby.com/'
								}), ' teach us that if we have enough time, we should try new stuff. So we explore, and you can enjoy our findings.']), (0, _inferno.createVNode)(2, 'p', null, 'Have you ever roamed late night Brno and wondered which bar is open, so you can sit, order Lagavulin and imagine Islay beaches?'), (0, _inferno.createVNode)(2, 'p', null, ['We created this web app to help you on such nights. We list ', (0, _inferno.createVNode)(2, 'a', null, 'all the places', {
												'href': (0, _href2.default)('places')
								}), ' we visit and record all the relevant data. Some of our ratings are subjective, but we focus primarily on the whiskies: where to get them and for what price.']), (0, _inferno.createVNode)(2, 'h2', _About2.default.subheading, 'Whisky rating'), (0, _inferno.createVNode)(2, 'p', null, ['To measure how adequate the whisky prices are, we calculate ', (0, _inferno.createVNode)(2, 'em', null, 'whisky rating'), ' for each place. But how exactly?']), (0, _inferno.createVNode)(2, 'p', null, 'The price of a dram in an establishment is compared to the cost of a dram you would pay if you bought the whole bottle in a wholesale store. We average these ratios for each place in our database, and also compute an overall mean and standard deviation. Then we standardize them, so the overall mean is 100 and standard deviation is 50.'), (0, _inferno.createVNode)(2, 'p', null, 'We can compare these whisky ratings: if the whisky rating is 100, it means that the prices in the establishment are average compared to all of the places. If the whisky rating is e.g. 130, that means the whisky there is cheaper. But remember, we are still talking about relative prices. Some place could sell only low cost whiskies, but over-price them, and therefore its whisky rating would be low. Conversely, if some place sells expensive whiskies with low profit margin, the whisky rating will be higher.'), (0, _inferno.createVNode)(2, 'p', null, 'Also, if you select some filter\u202F\u2013\u200Alet\u2019s say you are interested only in Islay whiskies\u202F\u2013\u200Athe whisky ratings is recalculated to only include this subset of dram prices.'), (0, _inferno.createVNode)(2, 'h2', _About2.default.subheading, 'Place rating'), (0, _inferno.createVNode)(2, 'p', null, ['While the whisky rating is more or less an objective measure, we don\u2019t just want to go where the prices are low. Good bar offers a space where you can really ', (0, _inferno.createVNode)(2, 'em', null, 'enjoy'), ' your dram.']), (0, _inferno.createVNode)(2, 'p', null, ['Therefore, we also rate the places and compute ', (0, _inferno.createVNode)(2, 'em', null, 'place rating'), '. Instead of price ratios, we award points for various criteria: how informed the staff is, how nice the ambient is, what are the opening hours, etc. You can find all the details on each place\u2019s page.']), (0, _inferno.createVNode)(2, 'p', null, 'Place ratings are standardized too: if the place rating is 100, it means that the establishment is average compared to all of the places. If the place rating is e.g. 130, that means we found the establishment better than the others for some reason.'), (0, _inferno.createVNode)(2, 'h2', _About2.default.subheading, 'Disclaimer'), (0, _inferno.createVNode)(2, 'p', null, 'We try our best to be impartial, but we probably aren\u2019t.'), (0, _inferno.createVNode)(2, 'p', null, 'None of our visits (nor the created content) are sponsored in any way.'), (0, _inferno.createVNode)(2, 'p', null, 'We don\u2019t guarantee our data are complete and that everything is up-to-date.')], {
												'onClick': this.handleClick
								});
				}
}
exports.default = About;