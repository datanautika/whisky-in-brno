'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _Button = require('./Button.css');

var _Button2 = _interopRequireDefault(_Button);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const MAILTO_REGEX = /^mailto:/;
const ROUTE_LINK_REGEX = /^\//;
let browserRouter = new _BrowserRouter2.default(_router2.default);
class Button extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.handleClick = event => {
            if (this.props && !this.props.isDisabled && !(this.props.link || this.props.link && MAILTO_REGEX.test(this.props.link))) {
                event.preventDefault();
                if (this.props.handleClick) {
                    this.props.handleClick();
                }
            } else if (this.props && !this.props.isDisabled && this.props.link && ROUTE_LINK_REGEX.test(this.props.link) && this.props.useRouter !== false) {
                event.preventDefault();
                browserRouter.navigate(this.props.link);
            } else if (this.props && this.props.isDisabled) {
                event.preventDefault();
            }
        };
    }
    render() {
        let buttonClass = _Button2.default.default;
        if (this.props && this.props.type === 'flat') {
            buttonClass = _Button2.default.flat;
        } else if (this.props && this.props.type === 'invisible') {
            buttonClass = _Button2.default.invisible;
        }
        if (this.props && this.props.size === 'small') {
            buttonClass += ' isSmall';
        } else if (this.props && this.props.size === 'large') {
            buttonClass += ' isLarge';
        }
        buttonClass += this.props && this.props.isSelected ? ' isSelected' : '';
        buttonClass += this.props && (this.props.icon || this.props.selectedIcon) ? ' hasIcon' : '';
        buttonClass += this.props && this.props.selectedIcon ? ' hasSelectableIcon' : '';
        buttonClass += this.props && (this.props.iconAfter || this.props.selectedIconAfter) ? ' hasIconAfter' : '';
        buttonClass += this.props && (this.props.icon || this.props.selectedIcon || this.props.iconAfter || this.props.selectedIconAfter) && !this.props.label && !this.props.badge ? ' hasOnlyIcon' : '';
        let iconElements = [];
        if (this.props && this.props.icon) {
            iconElements.push((0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': this.props.icon
            }, 'icon'));
        }
        if (this.props && this.props.selectedIcon) {
            iconElements.push((0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': this.props.selectedIcon
            }, 'selectedIcon'));
        }
        let iconAfterElements = [];
        if (this.props && this.props.iconAfter) {
            iconAfterElements.push((0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': this.props.iconAfter
            }, 'iconAfter'));
        }
        if (this.props && this.props.selectedIconAfter) {
            iconAfterElements.push((0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': this.props.selectedIconAfter
            }, 'selectedIconAfter'));
        }
        let buttonElement = (0, _inferno.createVNode)(2, 'button', buttonClass + (this.props && this.props.isDisabled ? ' isDisabled' : ' isEnabled'), [(0, _inferno.createVNode)(2, 'span', _Button2.default.icon, iconElements), this.props && this.props.label ? this.props.label : null, this.props && this.props.badge ? (0, _inferno.createVNode)(2, 'span', _Button2.default.badge, this.props.badge) : null, (0, _inferno.createVNode)(2, 'span', _Button2.default.iconAfter, iconAfterElements)], {
            'onClick': this.handleClick
        });
        if (this.props && this.props.isSubmit) {
            buttonElement = (0, _inferno.createVNode)(512, 'input', buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'), null, {
                'type': 'submit',
                'name': this.props.name || this.props.id || '',
                'id': this.props.id || this.props.name || '',
                'value': this.props.label || '',
                'onClick': this.handleClick
            });
        } else if (this.props && this.props.link) {
            buttonElement = (0, _inferno.createVNode)(2, 'a', buttonClass + (this.props.isDisabled ? ' isDisabled' : ' isEnabled'), [(0, _inferno.createVNode)(2, 'span', _Button2.default.icon, iconElements), this.props.label || null, this.props.badge ? (0, _inferno.createVNode)(2, 'span', _Button2.default.badge, this.props.badge) : null, (0, _inferno.createVNode)(2, 'span', _Button2.default.iconAfter, iconAfterElements)], {
                'href': this.props.link ? this.props.link : '#',
                'target': !this.props.isDisabled && this.props.link && ROUTE_LINK_REGEX.test(this.props.link) && this.props.useRouter !== false ? '' : '_blank',
                'onClick': this.handleClick
            });
        }
        return buttonElement;
    }
}
exports.default = Button;