'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _Header = require('./Header.css');

var _Header2 = _interopRequireDefault(_Header);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _isUrlInternal = require('../internals/isUrlInternal');

var _isUrlInternal2 = _interopRequireDefault(_isUrlInternal);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let browserRouter = new _BrowserRouter2.default(_router2.default);
class Header extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.handleClick = event => {
            if (event.button !== 1) {
                let url = event.target.getAttribute('href');
                if ((0, _isUrlInternal2.default)(url)) {
                    event.preventDefault();
                    browserRouter.navigate(url);
                }
            }
        };
    }
    render() {
        return (0, _inferno.createVNode)(2, 'div', _Header2.default.root, (0, _inferno.createVNode)(2, 'h1', null, (0, _inferno.createVNode)(2, 'a', null, 'Whisky in Brno', {
            'href': (0, _href2.default)('')
        })), {
            'onClick': this.handleClick
        });
    }
}
exports.default = Header;