'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ButtonGroup;

var _Button = require('./Button.css');

var _Button2 = _interopRequireDefault(_Button);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ButtonGroup(props) {
    return (0, _inferno.createVNode)(2, 'div', _Button2.default.buttonGroup, props.children);
}