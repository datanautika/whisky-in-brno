'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _WhiskiesList = require('./WhiskiesList.css');

var _WhiskiesList2 = _interopRequireDefault(_WhiskiesList);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _Table = require('./Table.css');

var _Table2 = _interopRequireDefault(_Table);

var _languageStream = require('../streams/languageStream');

var _languageStream2 = _interopRequireDefault(_languageStream);

var _dataStream = require('../streams/dataStream');

var _dataStream2 = _interopRequireDefault(_dataStream);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class WhiskiesList extends _infernoComponent2.default {
    componentDidMount() {
        this.onLanguageStream = _languageStream2.default.on(() => {
            this.forceUpdate();
        });
        this.onDataStream = _dataStream2.default.on(() => {
            this.forceUpdate();
        });
    }
    componentDidUnmount() {
        this.onLanguageStream.end();
        this.onDataStream.end();
    }
    render() {
        let { whiskies } = _dataStream2.default.value;
        let whiskyElements = whiskies.map((whisky, bottleIndex) => {
            return whisky ? (0, _inferno.createVNode)(2, 'li', `${_Table2.default.subitem} ${_WhiskiesList2.default.tableSubitem}`, (0, _inferno.createVNode)(16, _WhiskyName2.default, null, null, {
                'whisky': whisky
            }), null, whisky.id) : null;
        });
        return (0, _inferno.createVNode)(2, 'main', _WhiskiesList2.default.root, [(0, _inferno.createVNode)(2, 'h2', _WhiskiesList2.default.heading, (0, _inferno.createVNode)(2, 'span', _WhiskiesList2.default.title, 'Whiskies')), whiskyElements && whiskyElements.length ? (0, _inferno.createVNode)(2, 'ol', `${_Table2.default.subitems} ${_WhiskiesList2.default.tableSubitems}`, whiskyElements) : null]);
    }
}
exports.default = WhiskiesList;