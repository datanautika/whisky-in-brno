'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Breadcrumbs = require('./Breadcrumbs.css');

var _Breadcrumbs2 = _interopRequireDefault(_Breadcrumbs);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _routeStream = require('../streams/routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _isUrlInternal = require('../internals/isUrlInternal');

var _isUrlInternal2 = _interopRequireDefault(_isUrlInternal);

var _dataStream = require('../streams/dataStream');

var _dataStream2 = _interopRequireDefault(_dataStream);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let browserRouter = new _BrowserRouter2.default(_router2.default);
class Breadcrumbs extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.handleClick = event => {
            if (event.button !== 1) {
                let url = event.target.getAttribute('href');
                if ((0, _isUrlInternal2.default)(url)) {
                    event.preventDefault();
                    browserRouter.navigate(url);
                }
            }
        };
    }
    render() {
        let { page, subpage } = _routeStream2.default.value;
        let { whiskies, places } = _dataStream2.default.value;
        let level1;
        let level2;
        if (page === 'whiskies') {
            level1 = 'Whiskies';
            if (subpage) {
                let whisky = _lodash2.default.find(whiskies, { readableId: subpage });
                if (whisky) {
                    level2 = whisky.sortName;
                }
            }
        } else if (page === 'places') {
            level1 = 'Places';
            if (subpage) {
                let place = _lodash2.default.find(places, { readableId: subpage });
                if (place) {
                    level2 = place.name;
                }
            }
        }
        return (0, _inferno.createVNode)(2, 'nav', _Breadcrumbs2.default.root, [(0, _inferno.createVNode)(2, 'ol', _Breadcrumbs2.default.menu, [(0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', null, 'About', {
            'href': (0, _href2.default)('about')
        })), (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', null, 'Places', {
            'href': (0, _href2.default)('places')
        })), (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', null, 'Whiskies', {
            'href': (0, _href2.default)('whiskies')
        }))]), (0, _inferno.createVNode)(2, 'ol', _Breadcrumbs2.default.breadcrumbs, [(0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', null, '\u25CA', {
            'href': (0, _href2.default)('')
        })), level1 ? (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', null, level1, {
            'href': (0, _href2.default)(page)
        })) : null, level2 ? (0, _inferno.createVNode)(2, 'li', null, level2) : null])], {
            'onClick': this.handleClick
        });
    }
    componentDidMount() {
        _routeStream2.default.subscribe(() => {
            this.forceUpdate();
        });
    }
}
exports.default = Breadcrumbs;