'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _PlaceDetail = require('./PlaceDetail.css');

var _PlaceDetail2 = _interopRequireDefault(_PlaceDetail);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _Table = require('./Table.css');

var _Table2 = _interopRequireDefault(_Table);

var _I18n = require('../libs/I18n');

var _I18n2 = _interopRequireDefault(_I18n);

var _languageStream = require('../streams/languageStream');

var _languageStream2 = _interopRequireDefault(_languageStream);

var _dataStream = require('../streams/dataStream');

var _dataStream2 = _interopRequireDefault(_dataStream);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const INTEGER_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 0, maximumFractionDigits: 0 });
const FLOAT_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
const SHORT_FLOAT_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 1, maximumFractionDigits: 1 });
let i18n = new _I18n2.default();
let renderDate = dateString => {
    let result = [];
    let thResult = /(\d)(th)/.exec(dateString);
    let stResult = /(\d)(st)/.exec(dateString);
    let ndResult = /(\d)(nd)/.exec(dateString);
    let rdResult = /(\d)(rd)/.exec(dateString);
    if (thResult && thResult.length) {
        dateString.split(/(\d+)(?:th)/).filter(value => !!value).forEach((value, index, array) => {
            result.push(value);
            if (index < array.length - 1) {
                result.push((0, _inferno.createVNode)(2, 'sup', null, 'th'));
            }
        });
    } else if (stResult && stResult.length) {
        dateString.split(/(\d+)(?:st)/).filter(value => !!value).forEach((value, index, array) => {
            result.push(value);
            if (index < array.length - 1) {
                result.push((0, _inferno.createVNode)(2, 'sup', null, 'st'));
            }
        });
    } else if (ndResult && ndResult.length) {
        dateString.split(/(\d+)(?:nd)/).filter(value => !!value).forEach((value, index, array) => {
            result.push(value);
            if (index < array.length - 1) {
                result.push((0, _inferno.createVNode)(2, 'sup', null, 'nd'));
            }
        });
    } else if (rdResult && rdResult.length) {
        dateString.split(/(\d+)(?:rd)/).filter(value => !!value).forEach((value, index, array) => {
            result.push(value);
            if (index < array.length - 1) {
                result.push((0, _inferno.createVNode)(2, 'sup', null, 'rd'));
            }
        });
    }
    return result;
};
class PlaceDetail extends _infernoComponent2.default {
    componentDidMount() {
        this.onLanguageStream = _languageStream2.default.on(() => {
            this.forceUpdate();
        });
        this.onDataStream = _dataStream2.default.on(() => {
            this.forceUpdate();
        });
    }
    componentDidUnmount() {
        this.onLanguageStream.end();
        this.onDataStream.end();
    }
    render() {
        if (!this.props) {
            return null;
        }
        let { place } = this.props;
        let whiskyElements = place.review && place.review.bottles ? place.review.bottles.map((bottle, bottleIndex) => {
            let whiskyScoreClassName = '';
            let score = bottle.offers[0].score;
            if (score < 2) {
                whiskyScoreClassName = _PlaceDetail2.default.rating11;
            }
            if (score < 1.5) {
                whiskyScoreClassName = _PlaceDetail2.default.rating11;
            } else if (score >= 1.5 && score < 1.7) {
                whiskyScoreClassName = _PlaceDetail2.default.rating10;
            } else if (score >= 1.7 && score < 1.9) {
                whiskyScoreClassName = _PlaceDetail2.default.rating9;
            } else if (score >= 1.9 && score < 2.1) {
                whiskyScoreClassName = _PlaceDetail2.default.rating8;
            } else if (score >= 2.1 && score < 2.3) {
                whiskyScoreClassName = _PlaceDetail2.default.rating7;
            } else if (score >= 2.3 && score < 2.5) {
                whiskyScoreClassName = _PlaceDetail2.default.rating6;
            } else if (score >= 2.5 && score < 2.7) {
                whiskyScoreClassName = _PlaceDetail2.default.rating5;
            } else if (score >= 2.7 && score < 2.9) {
                whiskyScoreClassName = _PlaceDetail2.default.rating4;
            } else if (score >= 2.9 && score < 3.1) {
                whiskyScoreClassName = _PlaceDetail2.default.rating3;
            } else if (score >= 3.1 && score < 3.3) {
                whiskyScoreClassName = _PlaceDetail2.default.rating2;
            } else if (score >= 3.3) {
                whiskyScoreClassName = _PlaceDetail2.default.rating1;
            }
            return bottle.whisky ? (0, _inferno.createVNode)(2, 'li', `${_Table2.default.subitem} ${_PlaceDetail2.default.tableSubitem}`, [(0, _inferno.createVNode)(16, _WhiskyName2.default, null, null, {
                'whisky': bottle.whisky
            }), (0, _inferno.createVNode)(2, 'ul', _PlaceDetail2.default.whiskyInfo, [(0, _inferno.createVNode)(2, 'li', null, [bottle.offers[0].price.value, ' ', bottle.offers[0].price.currency === 'CZK' ? 'Kč' : '']), _lodash2.default.isFinite(score) ? (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'b', `${_PlaceDetail2.default.ratingBadge} ${whiskyScoreClassName}`, FLOAT_NUMBER_FORMAT.format(score))) : null])], null, bottle.whisky.id) : null;
        }) : null;
        let whiskyRatingClassName = '';
        let placeRatingClassName = '';
        let whiskyRating = place.whiskyRating;
        let placeRating = place.placeRating;
        if (whiskyRating < 50) {
            whiskyRatingClassName = _PlaceDetail2.default.rating1;
        } else if (whiskyRating >= 50 && whiskyRating < 65) {
            whiskyRatingClassName = _PlaceDetail2.default.rating2;
        } else if (whiskyRating >= 65 && whiskyRating < 75) {
            whiskyRatingClassName = _PlaceDetail2.default.rating3;
        } else if (whiskyRating >= 75 && whiskyRating < 85) {
            whiskyRatingClassName = _PlaceDetail2.default.rating4;
        } else if (whiskyRating >= 85 && whiskyRating < 95) {
            whiskyRatingClassName = _PlaceDetail2.default.rating5;
        } else if (whiskyRating >= 95 && whiskyRating < 105) {
            whiskyRatingClassName = _PlaceDetail2.default.rating6;
        } else if (whiskyRating >= 105 && whiskyRating < 115) {
            whiskyRatingClassName = _PlaceDetail2.default.rating7;
        } else if (whiskyRating >= 115 && whiskyRating < 125) {
            whiskyRatingClassName = _PlaceDetail2.default.rating8;
        } else if (whiskyRating >= 125 && whiskyRating < 135) {
            whiskyRatingClassName = _PlaceDetail2.default.rating9;
        } else if (whiskyRating >= 135 && whiskyRating < 150) {
            whiskyRatingClassName = _PlaceDetail2.default.rating10;
        } else if (whiskyRating >= 150) {
            whiskyRatingClassName = _PlaceDetail2.default.rating11;
        }
        if (placeRating < 50) {
            placeRatingClassName = _PlaceDetail2.default.rating1;
        } else if (placeRating >= 50 && placeRating < 65) {
            placeRatingClassName = _PlaceDetail2.default.rating2;
        } else if (placeRating >= 65 && placeRating < 75) {
            placeRatingClassName = _PlaceDetail2.default.rating3;
        } else if (placeRating >= 75 && placeRating < 85) {
            placeRatingClassName = _PlaceDetail2.default.rating4;
        } else if (placeRating >= 85 && placeRating < 95) {
            placeRatingClassName = _PlaceDetail2.default.rating5;
        } else if (placeRating >= 95 && placeRating < 105) {
            placeRatingClassName = _PlaceDetail2.default.rating6;
        } else if (placeRating >= 105 && placeRating < 115) {
            placeRatingClassName = _PlaceDetail2.default.rating7;
        } else if (placeRating >= 115 && placeRating < 125) {
            placeRatingClassName = _PlaceDetail2.default.rating8;
        } else if (placeRating >= 125 && placeRating < 135) {
            placeRatingClassName = _PlaceDetail2.default.rating9;
        } else if (placeRating >= 135 && placeRating < 150) {
            placeRatingClassName = _PlaceDetail2.default.rating10;
        } else if (placeRating >= 150) {
            placeRatingClassName = _PlaceDetail2.default.rating11;
        }
        let icon = '';
        if (place.type === 'bar') {
            icon = 'cocktail';
        } else if (place.type === 'pub') {
            icon = 'beer';
        } else if (place.type === 'coffeehouse') {
            icon = 'coffee';
        } else if (place.type === 'winehouse') {
            icon = 'wine';
        } else if (place.type === 'bistro') {
            icon = 'noodles';
        } else if (place.type === 'restaurant') {
            icon = 'dinner';
        }
        return (0, _inferno.createVNode)(2, 'main', _PlaceDetail2.default.root, [(0, _inferno.createVNode)(2, 'h2', _PlaceDetail2.default.heading, (0, _inferno.createVNode)(2, 'span', _PlaceDetail2.default.title, place.name)), place.description && place.description[i18n.locale] ? (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.row, (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.tripleRowItem, (0, _inferno.createVNode)(2, 'p', null, place.description[i18n.locale]))) : null, (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.row, [(0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Whisky rating'), (0, _inferno.createVNode)(2, 'p', null, (0, _inferno.createVNode)(2, 'span', `${_PlaceDetail2.default.ratingBadge} ${whiskyRatingClassName}`, _lodash2.default.isFinite(whiskyRating) ? INTEGER_NUMBER_FORMAT.format(whiskyRating) : '–'))]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Place rating'), (0, _inferno.createVNode)(2, 'p', null, (0, _inferno.createVNode)(2, 'span', `${_PlaceDetail2.default.ratingBadge} ${placeRatingClassName}`, _lodash2.default.isFinite(placeRating) ? INTEGER_NUMBER_FORMAT.format(placeRating) : '–'))]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.doubleRowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Latest rated on'), (0, _inferno.createVNode)(2, 'p', place.review && place.review.date ? '' : _PlaceDetail2.default.isUnknown, place.review && typeof place.review.date !== 'string' ? renderDate(place.review.date.format('Do MMMM YYYY')) : '?')])]), (0, _inferno.createVNode)(2, 'h3', _PlaceDetail2.default.whiskiesHeading, 'Information'), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.row, [(0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Type'), (0, _inferno.createVNode)(2, 'p', icon ? '' : `${_PlaceDetail2.default.isUnknown} ${_PlaceDetail2.default.hasOnlyText}`, icon ? (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
            'id': icon,
            'size': 'medium'
        }) : '?')]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Is open?'), (0, _inferno.createVNode)(2, 'p', (typeof place.isOpen === 'boolean' && place.openingHours && place.openingHours.length ? '' : `${_PlaceDetail2.default.isUnknown} ${_PlaceDetail2.default.hasOnlyText}`) + (place.isOpen ? ` ${_PlaceDetail2.default.isTrue}` : ` ${_PlaceDetail2.default.isFalse}`), typeof place.isOpen === 'boolean' && place.openingHours && place.openingHours.length ? place.isOpen ? (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
            'id': 'check-medium',
            'size': 'medium'
        }) : (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
            'id': 'cancel-medium',
            'size': 'medium'
        }) : '?')]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Card accepted?'), (0, _inferno.createVNode)(2, 'p', (place.review && typeof place.review.isCashOnly === 'boolean' ? '' : `${_PlaceDetail2.default.isUnknown} ${_PlaceDetail2.default.hasOnlyText}`) + (place.review && place.review.isCashOnly ? ` ${_PlaceDetail2.default.isFalse}` : ` ${_PlaceDetail2.default.isTrue}`), place.review && typeof place.review.isCashOnly === 'boolean' ? place.review && place.review.isCashOnly ? (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
            'id': 'cancel-medium',
            'size': 'medium'
        }) : (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
            'id': 'check-medium',
            'size': 'medium'
        }) : '?')]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Non-smoking?'), (0, _inferno.createVNode)(2, 'p', (place.review && typeof place.review.isSmokingAllowed === 'boolean' ? '' : `${_PlaceDetail2.default.isUnknown} ${_PlaceDetail2.default.hasOnlyText}`) + (place.review && place.review.isSmokingAllowed ? ` ${_PlaceDetail2.default.isFalse}` : ` ${_PlaceDetail2.default.isTrue}`), place.review && typeof place.review.isSmokingAllowed === 'boolean' ? place.review && place.review.isSmokingAllowed ? (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
            'id': 'cancel-medium',
            'size': 'medium'
        }) : (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
            'id': 'check-medium',
            'size': 'medium'
        }) : '?')]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Glass'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.hasOnlyText + (place.review && place.review.glass ? '' : ` ${_PlaceDetail2.default.isUnknown}`), place.review && place.review.glass ? place.review.glass : '?')])]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.row, [(0, _inferno.createVNode)(2, 'div', `${_PlaceDetail2.default.rowItem} ${_PlaceDetail2.default.isTopAligned}`, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Distance'), (0, _inferno.createVNode)(2, 'p', typeof place.distance !== 'undefined' ? '' : _PlaceDetail2.default.isUnknown, typeof place.distance !== 'undefined' ? `${INTEGER_NUMBER_FORMAT.format(Math.round(place.distance / 10) * 10)} m` : '?')]), place.address ? (0, _inferno.createVNode)(2, 'div', `${_PlaceDetail2.default.rowItem} ${_PlaceDetail2.default.isTopAligned}`, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Address'), (0, _inferno.createVNode)(2, 'p', null, place.address)]) : null, (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.tripleRowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Opening hours'), place.review && !place.review.isOperational ? (0, _inferno.createVNode)(2, 'p', null, 'Permanently or temporarily closed') : (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.hasOnlyText + (place.openingHours && place.openingHours.length ? '' : ` ${_PlaceDetail2.default.isUnknown}`), place.openingHours && place.openingHours.length ? place.openingHours.map(openingHours => (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.openingHours, [(0, _inferno.createVNode)(2, 'span', _PlaceDetail2.default.openingHoursLabel, openingHours.startTime ? openingHours.startTime.format('dddd') : null), (0, _inferno.createVNode)(2, 'span', _PlaceDetail2.default.openingHoursValue, openingHours.startTime && openingHours.endTime ? `${openingHours.startTime.format('HH:mm')} – ${openingHours.endTime.format('HH:mm')}` : null)])) : '?')])]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.row, [(0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Ambient'), (0, _inferno.createVNode)(2, 'p', place.review && typeof place.review.ambient === 'number' ? '' : _PlaceDetail2.default.isUnknown, place.review && typeof place.review.ambient === 'number' ? `${place.review.ambient} / 10` : '?')]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Service informedness'), (0, _inferno.createVNode)(2, 'p', place.review && typeof place.review.serviceInformedness === 'number' ? '' : _PlaceDetail2.default.isUnknown, place.review && typeof place.review.serviceInformedness === 'number' ? `${place.review.serviceInformedness} / 10` : '?')]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Service amiability'), (0, _inferno.createVNode)(2, 'p', place.review && typeof place.review.serviceAmiability === 'number' ? '' : _PlaceDetail2.default.isUnknown, place.review && typeof place.review.serviceAmiability === 'number' ? `${place.review.serviceAmiability} / 10` : '?')]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Restroom cleanness'), (0, _inferno.createVNode)(2, 'p', place.review && typeof place.review.restroomCleanness === 'number' ? '' : _PlaceDetail2.default.isUnknown, place.review && typeof place.review.restroomCleanness === 'number' ? `${place.review.restroomCleanness} / 10` : '?')]), (0, _inferno.createVNode)(2, 'div', _PlaceDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _PlaceDetail2.default.attributeHeading, 'Towels'), (0, _inferno.createVNode)(2, 'p', place.review && place.review.towels ? '' : _PlaceDetail2.default.isUnknown, place.review && place.review.towels ? place.review.towels : '?')])]), (0, _inferno.createVNode)(2, 'h3', _PlaceDetail2.default.whiskiesHeading, 'Whiskies'), whiskyElements && whiskyElements.length ? (0, _inferno.createVNode)(2, 'ol', `${_Table2.default.subitems} ${_PlaceDetail2.default.tableSubitems}`, whiskyElements) : null, (0, _inferno.createVNode)(2, 'h3', _PlaceDetail2.default.whiskiesHeading, 'Rating calculation'), (0, _inferno.createVNode)(2, 'ul', _PlaceDetail2.default.points, [(0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Number of bottles'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeWhiskiesCountScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeWhiskiesCountScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Glass'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeGlassScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeGlassScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Opening hours'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeOpeningHoursScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeOpeningHoursScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Card accepted?'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeIsCashOnlyScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeIsCashOnlyScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Non-smoking?'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeIsSmokingAllowedScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeIsSmokingAllowedScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Ambient'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeAmbientScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeAmbientScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Service amiability'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeServiceAmiabilityScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeServiceAmiabilityScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Service informedness'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeServiceInformednessScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeServiceInformednessScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Restroom cleanness'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeRestroomCleannessScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeRestroomCleannessScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, 'Towels'), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, _lodash2.default.isFinite(place.placeTowelsScore) ? `+${SHORT_FLOAT_NUMBER_FORMAT.format(place.placeTowelsScore)}` : '–')]), (0, _inferno.createVNode)(2, 'li', null, [(0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsLabel, (0, _inferno.createVNode)(2, 'strong', null, 'Total')), (0, _inferno.createVNode)(2, 'p', _PlaceDetail2.default.pointsValue, (0, _inferno.createVNode)(2, 'strong', null, _lodash2.default.isFinite(place.placeScore) ? SHORT_FLOAT_NUMBER_FORMAT.format(place.placeScore) : '–'))])])]);
    }
}
exports.default = PlaceDetail;