'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _Icon = require('./Icon.css');

var _Icon2 = _interopRequireDefault(_Icon);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const OLD_EDGE_ID = 10547;
const OLD_WEBKIT_ID = 537;
function embed(svg, target) {
    if (target) {
        let fragment = document.createDocumentFragment();
        let viewBox = !svg.getAttribute('viewBox') && target.getAttribute('viewBox');
        if (viewBox) {
            svg.setAttribute('viewBox', viewBox);
        }
        let clone = target.cloneNode(true);
        while (clone.childNodes.length) {
            fragment.appendChild(clone.firstChild);
        }
        svg.appendChild(fragment);
    }
}
function loadReadyStateChange(xhr) {
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            let cachedDocument = xhr._cachedDocument;
            if (!cachedDocument) {
                cachedDocument = xhr._cachedDocument = document.implementation.createHTMLDocument('');
                cachedDocument.body.innerHTML = xhr.responseText;
                xhr._cachedTarget = {};
            }
            xhr._embeds.splice(0).map(item => {
                let target = xhr._cachedTarget[item.id];
                if (!target) {
                    target = xhr._cachedTarget[item.id] = cachedDocument.getElementById(item.id);
                }
                embed(item.svg, target);
            });
        }
    };
    xhr.onreadystatechange();
}
class Icon extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.refs = {};
    }
    render() {
        let styleSuffix = '';
        if (this.props && this.props.size === 'medium') {
            styleSuffix = ` ${_Icon2.default.medium}`;
        } else if (this.props && this.props.size === 'large') {
            styleSuffix = ` ${_Icon2.default.large}`;
        }
        return (0, _inferno.createVNode)(2, 'span', _Icon2.default.root + styleSuffix, (0, _inferno.createVNode)(128, 'svg', null, this.props && this.props.id ? (0, _inferno.createVNode)(2, 'use', null, null, {
            'xlink:href': (0, _href2.default)('assets', `icons.svg#${this.props.id}`)
        }, null, node => {
            this.refs.use = node;
        }) : null, {
            'xmlns': 'http://www.w3.org/2000/svg'
        }), null, null, node => {
            this.refs.root = node;
        });
    }
    componentDidMount() {
        let domNode = this.refs.root;
        let useNode = this.refs.use;
        if (!domNode || !useNode) {
            return;
        }
        let polyfill;
        let newerIEUA = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/;
        let webkitUA = /\bAppleWebKit\/(\d+)\b/;
        let olderEdgeUA = /\bEdge\/12\.(\d+)\b/;
        polyfill = newerIEUA.test(navigator.userAgent) || parseInt((navigator.userAgent.match(olderEdgeUA) || [])[1], 10) < OLD_EDGE_ID || parseInt((navigator.userAgent.match(webkitUA) || [])[1], 10) < OLD_WEBKIT_ID;
        let requests = {};
        if (polyfill) {
            requestAnimationFrame(() => {
                if (useNode) {
                    let svg = useNode.parentNode;
                    if (svg && /svg/i.test(svg.nodeName)) {
                        let src = useNode.getAttribute('xlink:href') || useNode.getAttribute('href');
                        svg.removeChild(useNode);
                        let srcSplit = src ? src.split('#') : null;
                        let url = srcSplit ? srcSplit.shift() : null;
                        let id = srcSplit ? srcSplit.join('#') : null;
                        if (url && url.length) {
                            let xhr = requests[url];
                            if (!xhr) {
                                xhr = requests[url] = new XMLHttpRequest();
                                xhr.open('GET', url);
                                xhr.send();
                                xhr._embeds = [];
                            }
                            xhr._embeds.push({
                                svg,
                                id
                            });
                            loadReadyStateChange(xhr);
                        } else if (id) {
                            embed(svg, document.getElementById(id));
                        }
                    }
                }
            });
        }
    }
}
exports.default = Icon;