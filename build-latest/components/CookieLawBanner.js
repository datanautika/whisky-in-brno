'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = CookieLawBanner;

var _CookieLawBanner = require('./CookieLawBanner.css');

var _CookieLawBanner2 = _interopRequireDefault(_CookieLawBanner);

var _Button = require('./Button');

var _Button2 = _interopRequireDefault(_Button);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function CookieLawBanner(props) {
	return (0, _inferno.createVNode)(2, 'div', _CookieLawBanner2.default.root, [(0, _inferno.createVNode)(2, 'p', null, 'Cookies are very small text files that are stored on your computer when you visit some websites. We use cookies to help identify your computer so we can tailor your user experience and to analyse traffic to this site. You can disable any cookies already stored on your computer.'), (0, _inferno.createVNode)(2, 'p', _CookieLawBanner2.default.button, (0, _inferno.createVNode)(16, _Button2.default, null, null, {
		'type': 'flat',
		'label': 'I understand',
		'handleClick': props.handleHideBanner
	}))]);
}