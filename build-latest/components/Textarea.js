'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _Textarea = require('./Textarea.css');

var _Textarea2 = _interopRequireDefault(_Textarea);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Textarea extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.handleInput = event => {
            if (this.props && this.props.handleChange) {
                this.props.handleChange(this.validate(event.target.value));
            }
        };
        this.handleFocusOut = event => {
            if (this.props && this.props.handleSave) {
                this.props.handleSave(this.validate(event.target.value));
            }
        };
    }
    shouldComponentUpdate(newProps) {
        if (this.props !== newProps) {
            return true;
        }
        return this.props && newProps && (newProps.id !== this.props.id || newProps.name !== this.props.name || newProps.rows !== this.props.rows || newProps.isValid !== this.props.isValid || newProps.isInvalid !== this.props.isInvalid || newProps.isDisabled !== this.props.isDisabled || newProps.handleChange !== this.props.handleChange || newProps.handleSave !== this.props.handleSave || newProps.validator !== this.props.validator || newProps.value !== this.props.value);
    }
    validate(value) {
        return this.props && this.props.validator ? this.props.validator(value) : value;
    }
    render() {
        let textareaProps = {
            key: this.props ? this.props.id || this.props.name : '',
            className: _Textarea2.default.default + (this.props && this.props.isInvalid ? ' isInvalid' : '') + (this.props && this.props.isValid ? ' isValid' : '') + (this.props && this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
            name: this.props ? this.props.name || this.props.id : '',
            id: this.props ? this.props.id || this.props.name : '',
            rows: this.props ? this.props.rows : 5,
            onBlur: this.handleFocusOut,
            onChange: this.handleInput,
            value: this.props ? this.props.value : ''
        };
        if (this.props && this.props.isDisabled) {
            textareaProps.disabled = 'disabled';
        }
        return (0, _inferno.createVNode)(1024, 'textarea', null, null, _extends({}, textareaProps));
    }
}
exports.default = Textarea;