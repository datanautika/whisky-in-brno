'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = FormRow;

var _FormRow = require('./FormRow.css');

var _FormRow2 = _interopRequireDefault(_FormRow);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function FormRow(props) {
	let labelElement = props.id ? (0, _inferno.createVNode)(2, 'label', null, `${props.label}`, {
		'for': props.id
	}) : (0, _inferno.createVNode)(2, 'label', null, `${props.label}`);
	return (0, _inferno.createVNode)(2, 'div', _FormRow2.default.root + (props.className ? ` ${props.className}` : ''), [props.label ? (0, _inferno.createVNode)(2, 'div', _FormRow2.default.label, [labelElement, props.hint ? (0, _inferno.createVNode)(2, 'span', _FormRow2.default.hint, props.hint) : null]) : null, (0, _inferno.createVNode)(2, 'div', _FormRow2.default.fields, [props.children, (0, _inferno.createVNode)(2, 'div', _FormRow2.default.errorMessage + (props.showError ? ' ${styles.isVisible}' : ''), props.errorMessage || null)])]);
}