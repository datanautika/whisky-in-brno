'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _Input = require('./Input.css');

var _Input2 = _interopRequireDefault(_Input);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ENTER_KEY_CODE = 13;
class Input extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.handleInput = event => {
            if (this.props && this.props.handleChange) {
                this.props.handleChange(this.validate(event.target.value));
            }
        };
        this.handleFocusOut = event => {
            if (this.props && this.props.handleSave) {
                this.props.handleSave(this.validate(event.target.value));
            }
        };
        this.handleKeyDown = event => {
            if (event.keyCode === ENTER_KEY_CODE) {
                if (this.props && this.props.handleSave) {
                    this.props.handleSave(this.validate(event.target.value));
                }
            }
        };
    }
    render() {
        let inputProps = {
            key: this.props ? this.props.id || this.props.name : '',
            className: _Input2.default.default + (this.props && this.props.isValid ? ' isValid' : '') + (this.props && this.props.isInvalid ? ' isInvalid' : '') + (this.props && this.props.isDisabled ? ' isDisabled' : ' isEnabled'),
            type: 'text',
            name: this.props ? this.props.name || this.props.id : '',
            id: this.props ? this.props.id || this.props.name : '',
            value: this.props ? this.props.value : '',
            onBlur: this.handleFocusOut,
            onChange: this.handleInput,
            onKeyDown: this.handleKeyDown
        };
        if (this.props && this.props.type === 'email') {
            inputProps.type = this.props.type;
        }
        if (this.props && this.props.isDisabled) {
            inputProps.disabled = 'disabled';
        }
        if (this.props && this.props.autocomplete === false) {
            inputProps.autocomplete = 'off';
        }
        return (0, _inferno.createVNode)(512, 'input', null, null, _extends({}, inputProps));
    }
    validate(value) {
        return this.props && this.props.validator ? this.props.validator(value) : value;
    }
}
exports.default = Input;