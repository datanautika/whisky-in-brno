'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var _PlacesList = require('./PlacesList.css');

var _PlacesList2 = _interopRequireDefault(_PlacesList);

var _dataStream = require('../streams/dataStream');

var _dataStream2 = _interopRequireDefault(_dataStream);

var _locationStream = require('../streams/locationStream');

var _locationStream2 = _interopRequireDefault(_locationStream);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _Icon = require('./Icon');

var _Icon2 = _interopRequireDefault(_Icon);

var _Table = require('./Table.css');

var _Table2 = _interopRequireDefault(_Table);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _filterToUri = require('../utils/filterToUri');

var _filterToUri2 = _interopRequireDefault(_filterToUri);

var _filterStream = require('../streams/filterStream');

var _filterStream2 = _interopRequireDefault(_filterStream);

var _browserRouter = require('../streams/browserRouter');

var _browserRouter2 = _interopRequireDefault(_browserRouter);

var _isUrlInternal = require('../internals/isUrlInternal');

var _isUrlInternal2 = _interopRequireDefault(_isUrlInternal);

var _support = require('../internals/support');

var _support2 = _interopRequireDefault(_support);

var _constants = require('../internals/constants');

var _constants2 = _interopRequireDefault(_constants);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CLIENT_PLATFORM = _constants2.default.CLIENT_PLATFORM;
const INTEGER_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 0, maximumFractionDigits: 0 });
const FLOAT_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
let $map;
let map;
let youMarker;
let google = global.google;
if (_support2.default.platform === CLIENT_PLATFORM) {
    $map = (0, _jquery2.default)('<div></div>');
    map = new google.maps.Map($map[0], {
        zoom: 17,
        center: new google.maps.LatLng(_locationStream2.default.value.latitude, _locationStream2.default.value.longitude)
    });
    youMarker = new google.maps.Marker({
        position: new google.maps.LatLng(_locationStream2.default.value.latitude, _locationStream2.default.value.longitude),
        label: 'Y',
        map
    });
    window.$map = $map;
    _dataStream2.default.value.places.map(place => new google.maps.Marker({
        position: new google.maps.LatLng(place.latitude, place.longitude),
        map
    }));
}
class PlacesList extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.state = {
            openPlaces: _immutable2.default.Set()
        };
        this.refs = {
            map: null
        };
        this.handleClick = event => {
            if (event.button !== 1) {
                let url = event.target.getAttribute('href');
                if ((0, _isUrlInternal2.default)(url)) {
                    event.preventDefault();
                    if (/filter\/.+\//.test(url)) {
                        _browserRouter2.default.navigate(url);
                    } else {
                        _browserRouter2.default.navigate(url);
                    }
                }
            }
        };
        this.handleFilterClick = event => {
            if (event.button !== 1) {
                let url = event.target.getAttribute('href');
                if ((0, _isUrlInternal2.default)(url)) {
                    event.preventDefault();
                    if (/places\/%7B/.test(url)) {
                        _browserRouter2.default.navigate(url, { resetScrollPosition: false });
                    } else {
                        _browserRouter2.default.navigate(url);
                    }
                }
            }
        };
    }
    componentDidMount() {
        this.onDataStream = _dataStream2.default.on(() => {
            this.forceUpdate();
        });
        this.onLocationStream = _locationStream2.default.on(value => {
            map.setCenter(new google.maps.LatLng(value.latitude, value.longitude));
            youMarker.setPosition(new google.maps.LatLng(value.latitude, value.longitude));
            this.forceUpdate();
        });
        $map.appendTo(this.refs.map);
    }
    componentDidUnmount() {
        this.onDataStream.end();
        this.onLocationStream.end();
        $map.detach();
    }
    render() {
        let { places, placeTypes, whiskyRegions, whiskyBrands } = _dataStream2.default.value;
        let { filter } = _filterStream2.default.value;
        let placesElements = places.map(place => {
            let whiskyElements = this.state.openPlaces.has(place.id) && place.review && place.review.bottles ? place.review.bottles.map((bottle, bottleIndex) => {
                let whiskyScoreClassName = '';
                let score = bottle.offers[0].score;
                if (score < 2) {
                    whiskyScoreClassName = _PlacesList2.default.rating11;
                }
                if (score < 1.5) {
                    whiskyScoreClassName = _PlacesList2.default.rating11;
                } else if (score >= 1.5 && score < 1.7) {
                    whiskyScoreClassName = _PlacesList2.default.rating10;
                } else if (score >= 1.7 && score < 1.9) {
                    whiskyScoreClassName = _PlacesList2.default.rating9;
                } else if (score >= 1.9 && score < 2.1) {
                    whiskyScoreClassName = _PlacesList2.default.rating8;
                } else if (score >= 2.1 && score < 2.3) {
                    whiskyScoreClassName = _PlacesList2.default.rating7;
                } else if (score >= 2.3 && score < 2.5) {
                    whiskyScoreClassName = _PlacesList2.default.rating6;
                } else if (score >= 2.5 && score < 2.7) {
                    whiskyScoreClassName = _PlacesList2.default.rating5;
                } else if (score >= 2.7 && score < 2.9) {
                    whiskyScoreClassName = _PlacesList2.default.rating4;
                } else if (score >= 2.9 && score < 3.1) {
                    whiskyScoreClassName = _PlacesList2.default.rating3;
                } else if (score >= 3.1 && score < 3.3) {
                    whiskyScoreClassName = _PlacesList2.default.rating2;
                } else if (score >= 3.3) {
                    whiskyScoreClassName = _PlacesList2.default.rating1;
                }
                return bottle.whisky ? (0, _inferno.createVNode)(2, 'li', `${_Table2.default.subitem} ${_PlacesList2.default.tableSubitem}`, [(0, _inferno.createVNode)(16, _WhiskyName2.default, null, null, {
                    'whisky': bottle.whisky
                }), (0, _inferno.createVNode)(2, 'ul', _PlacesList2.default.whiskyInfo, [(0, _inferno.createVNode)(2, 'li', null, [bottle.offers[0].price.value, ' ', bottle.offers[0].price.currency === 'CZK' ? 'Kč' : '']), _lodash2.default.isFinite(bottle.offers[0].score) ? (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'b', `${_PlacesList2.default.ratingBadge} ${whiskyScoreClassName}`, FLOAT_NUMBER_FORMAT.format(score))) : null])], null, bottle.whisky.id) : null;
            }) : null;
            let whiskyRatingClassName = '';
            let placeRatingClassName = '';
            let whiskyRating = place.whiskyRating;
            let placeRating = place.placeRating;
            if (whiskyRating < 50) {
                whiskyRatingClassName = _PlacesList2.default.rating1;
            } else if (whiskyRating >= 50 && whiskyRating < 65) {
                whiskyRatingClassName = _PlacesList2.default.rating2;
            } else if (whiskyRating >= 65 && whiskyRating < 75) {
                whiskyRatingClassName = _PlacesList2.default.rating3;
            } else if (whiskyRating >= 75 && whiskyRating < 85) {
                whiskyRatingClassName = _PlacesList2.default.rating4;
            } else if (whiskyRating >= 85 && whiskyRating < 95) {
                whiskyRatingClassName = _PlacesList2.default.rating5;
            } else if (whiskyRating >= 95 && whiskyRating < 105) {
                whiskyRatingClassName = _PlacesList2.default.rating6;
            } else if (whiskyRating >= 105 && whiskyRating < 115) {
                whiskyRatingClassName = _PlacesList2.default.rating7;
            } else if (whiskyRating >= 115 && whiskyRating < 125) {
                whiskyRatingClassName = _PlacesList2.default.rating8;
            } else if (whiskyRating >= 125 && whiskyRating < 135) {
                whiskyRatingClassName = _PlacesList2.default.rating9;
            } else if (whiskyRating >= 135 && whiskyRating < 150) {
                whiskyRatingClassName = _PlacesList2.default.rating10;
            } else if (whiskyRating >= 150) {
                whiskyRatingClassName = _PlacesList2.default.rating11;
            }
            if (placeRating < 50) {
                placeRatingClassName = _PlacesList2.default.rating1;
            } else if (placeRating >= 50 && placeRating < 65) {
                placeRatingClassName = _PlacesList2.default.rating2;
            } else if (placeRating >= 65 && placeRating < 75) {
                placeRatingClassName = _PlacesList2.default.rating3;
            } else if (placeRating >= 75 && placeRating < 85) {
                placeRatingClassName = _PlacesList2.default.rating4;
            } else if (placeRating >= 85 && placeRating < 95) {
                placeRatingClassName = _PlacesList2.default.rating5;
            } else if (placeRating >= 95 && placeRating < 105) {
                placeRatingClassName = _PlacesList2.default.rating6;
            } else if (placeRating >= 105 && placeRating < 115) {
                placeRatingClassName = _PlacesList2.default.rating7;
            } else if (placeRating >= 115 && placeRating < 125) {
                placeRatingClassName = _PlacesList2.default.rating8;
            } else if (placeRating >= 125 && placeRating < 135) {
                placeRatingClassName = _PlacesList2.default.rating9;
            } else if (placeRating >= 135 && placeRating < 150) {
                placeRatingClassName = _PlacesList2.default.rating10;
            } else if (placeRating >= 150) {
                placeRatingClassName = _PlacesList2.default.rating11;
            }
            let icon = '';
            if (place.type === 'bar') {
                icon = 'cocktail';
            } else if (place.type === 'pub') {
                icon = 'beer';
            } else if (place.type === 'coffeehouse') {
                icon = 'coffee';
            } else if (place.type === 'winehouse') {
                icon = 'wine';
            } else if (place.type === 'bistro') {
                icon = 'noodles';
            } else if (place.type === 'restaurant') {
                icon = 'dinner';
            }
            return (0, _inferno.createVNode)(2, 'section', _Table2.default.root, [(0, _inferno.createVNode)(2, 'header', _Table2.default.itemHeader, [(0, _inferno.createVNode)(2, 'h3', `${_Table2.default.itemHeading} ${_PlacesList2.default.tableItemHeading}`, [(0, _inferno.createVNode)(2, 'span', _Table2.default.label, 'Name'), (0, _inferno.createVNode)(2, 'a', null, place.name, {
                'href': (0, _href2.default)('places', place.readableId)
            }), ' ', place.review && place.review.bottles && place.review.bottles.length ? (0, _inferno.createVNode)(2, 'a', null, this.state.openPlaces.has(place.id) ? (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': 'subtract'
            }) : (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': 'add'
            }), {
                'href': '#',
                'onClick': this.handleOpenPlaceClick.bind(this, place.id)
            }) : null]), icon ? (0, _inferno.createVNode)(2, 'p', `${_Table2.default.itemInfo} ${_Table2.default.isCenterAligned}`, [(0, _inferno.createVNode)(2, 'span', _Table2.default.label, 'Type'), (0, _inferno.createVNode)(2, 'span', _Table2.default.value, (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': icon,
                'size': 'medium'
            }))]) : null, place.openingHours && place.openingHours.length ? (0, _inferno.createVNode)(2, 'p', `${_Table2.default.itemInfo} ${_Table2.default.isCenterAligned}`, [(0, _inferno.createVNode)(2, 'span', _Table2.default.label, 'Is open?'), (0, _inferno.createVNode)(2, 'span', _Table2.default.value + (place.isOpen ? ` ${_PlacesList2.default.isPlaceOpen}` : ` ${_PlacesList2.default.isPlaceClosed}`), place.isOpen ? (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': 'check-medium',
                'size': 'medium'
            }) : (0, _inferno.createVNode)(16, _Icon2.default, null, null, {
                'id': 'cancel-medium',
                'size': 'medium'
            }))]) : null, typeof place.distance !== 'undefined' ? (0, _inferno.createVNode)(2, 'p', `${_Table2.default.itemInfo} ${_Table2.default.isRightAligned}`, [(0, _inferno.createVNode)(2, 'span', _Table2.default.label, 'Distance'), (0, _inferno.createVNode)(2, 'span', _Table2.default.value, `${INTEGER_NUMBER_FORMAT.format(Math.round(place.distance / 10) * 10)} m`)]) : null, (0, _inferno.createVNode)(2, 'p', _Table2.default.itemInfo + (_lodash2.default.isFinite(whiskyRating) ? '' : ` ${_PlacesList2.default.isHidden}`), [(0, _inferno.createVNode)(2, 'span', _Table2.default.label, 'Whisky rating'), (0, _inferno.createVNode)(2, 'span', `${_Table2.default.value} ${_PlacesList2.default.ratingBadge} ${whiskyRatingClassName}`, _lodash2.default.isFinite(whiskyRating) ? INTEGER_NUMBER_FORMAT.format(whiskyRating) : '–')])]), whiskyElements && whiskyElements.length ? (0, _inferno.createVNode)(2, 'ol', `${_Table2.default.subitems} ${_PlacesList2.default.tableSubitems}`, whiskyElements) : null], {
                'onClick': this.handleClick
            }, place.id);
        });
        return (0, _inferno.createVNode)(2, 'main', _PlacesList2.default.root, [(0, _inferno.createVNode)(2, 'div', _PlacesList2.default.map, null, null, null, node => {
            this.refs.map = node;
        }), (0, _inferno.createVNode)(2, 'nav', _Table2.default.filters, [(0, _inferno.createVNode)(2, 'div', _Table2.default.filter, [(0, _inferno.createVNode)(2, 'h4', _Table2.default.filterHeading, 'Filter places by whisky region'), (0, _inferno.createVNode)(2, 'ol', _Table2.default.filterOptions, [whiskyRegions.map(whiskyRegion => (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', whiskyRegion === filter.whiskyRegion ? _Table2.default.isSelected : '', whiskyRegion, {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ whiskyRegion }))
        }), null, whiskyRegion)), filter.whiskyRegion ? (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', null, '(cancel)', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ whiskyRegion: null }))
        })) : null])]), (0, _inferno.createVNode)(2, 'div', _Table2.default.filter, [(0, _inferno.createVNode)(2, 'h4', _Table2.default.filterHeading, 'Filter places by whisky brand'), (0, _inferno.createVNode)(2, 'ol', _Table2.default.filterOptions, [whiskyBrands.map(whiskyBrand => (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', whiskyBrand === filter.whiskyBrand ? _Table2.default.isSelected : '', whiskyBrand, {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ whiskyBrand }))
        }), null, whiskyBrand)), filter.whiskyBrand ? (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', null, '(cancel)', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ whiskyBrand: null }))
        })) : null])]), (0, _inferno.createVNode)(2, 'div', _Table2.default.filter, [(0, _inferno.createVNode)(2, 'h4', _Table2.default.filterHeading, 'Filter places by type'), (0, _inferno.createVNode)(2, 'ol', _Table2.default.filterOptions, [placeTypes.map(placeType => (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', placeType === filter.placeType ? _Table2.default.isSelected : '', _lodash2.default.startCase(placeType), {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ placeType }))
        }), null, placeType)), filter.placeType ? (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', null, '(cancel)', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ placeType: null }))
        })) : null])]), (0, _inferno.createVNode)(2, 'div', _Table2.default.filter, [(0, _inferno.createVNode)(2, 'h4', _Table2.default.filterHeading, 'Sort places by\u2026'), (0, _inferno.createVNode)(2, 'ol', _Table2.default.filterOptions, [(0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', filter.sortPlacesBy === 'rating' ? _Table2.default.isSelected : '', 'Rating', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ sortPlacesBy: 'rating' }))
        })), (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', filter.sortPlacesBy === 'distance' ? _Table2.default.isSelected : '', 'Distance', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ sortPlacesBy: 'distance' }))
        })), (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', filter.sortPlacesBy === 'name' ? _Table2.default.isSelected : '', 'Name', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ sortPlacesBy: 'name' }))
        }))])]), (0, _inferno.createVNode)(2, 'div', _Table2.default.filter, [(0, _inferno.createVNode)(2, 'h4', _Table2.default.filterHeading, 'Sort whiskies by\u2026'), (0, _inferno.createVNode)(2, 'ol', _Table2.default.filterOptions, [(0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', filter.sortWhiskiesBy === 'score' ? _Table2.default.isSelected : '', 'Score', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ sortWhiskiesBy: 'score' }))
        })), (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', filter.sortWhiskiesBy === 'price' ? _Table2.default.isSelected : '', 'Price', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ sortWhiskiesBy: 'price' }))
        })), (0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', filter.sortWhiskiesBy === 'name' ? _Table2.default.isSelected : '', 'Name', {
            'href': (0, _href2.default)('places', (0, _filterToUri2.default)({ sortWhiskiesBy: 'name' }))
        }))])])], {
            'onClick': this.handleFilterClick
        }), placesElements]);
    }
    handleOpenPlaceClick(placeId, event) {
        let { places } = _dataStream2.default.value;
        event.preventDefault();
        if (this.state.openPlaces.has(placeId)) {
            this.state.openPlaces = this.state.openPlaces.delete(placeId);
            let place = _lodash2.default.find(places, { id: placeId });
            if (place) {
                global.ga('send', 'event', 'Places list', 'Close place', place.name, placeId);
            }
        } else {
            this.state.openPlaces = this.state.openPlaces.add(placeId);
            let place = _lodash2.default.find(places, { id: placeId });
            if (place) {
                global.ga('send', 'event', 'Places list', 'Open place', place.name, placeId);
            }
        }
        this.forceUpdate();
    }
}
exports.default = PlacesList;