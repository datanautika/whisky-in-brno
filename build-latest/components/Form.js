"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = Form;

var _inferno = require("inferno");

function Form(props) {
    let formProps = {};
    if (props.className) {
        formProps.className = props.className;
    }
    return (0, _inferno.createVNode)(2, "form", null, props.children, _extends({}, formProps));
}