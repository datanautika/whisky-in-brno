'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _Footer = require('./Footer.css');

var _Footer2 = _interopRequireDefault(_Footer);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _isUrlInternal = require('../internals/isUrlInternal');

var _isUrlInternal2 = _interopRequireDefault(_isUrlInternal);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let browserRouter = new _BrowserRouter2.default(_router2.default);
class Footer extends _infernoComponent2.default {
    constructor() {
        super(...arguments);
        this.handleClick = event => {
            if (event.button !== 1) {
                let url = event.target.getAttribute('href');
                if ((0, _isUrlInternal2.default)(url)) {
                    event.preventDefault();
                    browserRouter.navigate(url);
                }
            }
        };
    }
    render() {
        return (0, _inferno.createVNode)(2, 'div', _Footer2.default.root, [(0, _inferno.createVNode)(2, 'ol', null, [(0, _inferno.createVNode)(2, 'li', null, (0, _inferno.createVNode)(2, 'a', _Footer2.default.hasNoUnderline, 'v0.7.2', {
            'href': (0, _href2.default)('updates')
        })), (0, _inferno.createVNode)(2, 'li', null, ['Do you have any advice? ', (0, _inferno.createVNode)(2, 'a', null, 'Let us know!', {
            'href': 'mailto:whiskyinbrno@datanautika.com'
        })])]), (0, _inferno.createVNode)(2, 'p', _Footer2.default.logo, (0, _inferno.createVNode)(2, 'a', null, (0, _inferno.createVNode)(2, 'img', null, null, {
            'src': (0, _href2.default)('assets', 'datanautika.svg'),
            'alt': 'Made by Datanautika'
        }), {
            'href': 'http://www.datanautika.com'
        }))], {
            'onClick': this.handleClick
        });
    }
}
exports.default = Footer;