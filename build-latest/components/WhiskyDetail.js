'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _infernoComponent = require('inferno-component');

var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _WhiskyDetail = require('./WhiskyDetail.css');

var _WhiskyDetail2 = _interopRequireDefault(_WhiskyDetail);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 1, maximumFractionDigits: 1 });
const ML_IN_L = 1000;
let name = whisky => {
    let [titlePart1, titlePart2, subtitle] = whisky.fullName;
    return (0, _inferno.createVNode)(2, 'h2', _WhiskyDetail2.default.heading, [(0, _inferno.createVNode)(2, 'span', _WhiskyDetail2.default.title, [titlePart1 ? titlePart1 : null, titlePart2 ? (0, _inferno.createVNode)(2, 'b', null, titlePart2) : null]), (0, _inferno.createVNode)(2, 'span', _WhiskyDetail2.default.subtitle, subtitle ? subtitle : null)]);
};
let parseDate = dateString => {
    let partsCount = dateString.match(/-/g);
    if (partsCount <= 1) {
        return dateString;
    }
    let date = (0, _moment2.default)(dateString);
    if (partsCount > 1) {
        return date.format('Do MMMM YYYY');
    }
    return date.format('MMMM YYYY');
};
class WhiskyDetail extends _infernoComponent2.default {
    render() {
        if (!this.props) {
            return null;
        }
        let { whisky } = this.props;
        let regionClass;
        let regionName;
        let districtName;
        let countryName = whisky.country;
        let isSingleMalt = whisky.type === 'Single Malt Whisky';
        if (whisky.region) {
            regionClass = _WhiskyDetail2.default[whisky.region.toLowerCase()];
            if (whisky.region === 'Islay') {
                regionName = 'Islay';
            } else if (whisky.region === 'Islands') {
                regionName = 'Islands';
            } else if (whisky.region === 'Highland') {
                regionName = 'Highland';
            } else if (whisky.region === 'Lowland') {
                regionName = 'Lowland';
            } else if (whisky.region === 'Speyside') {
                regionName = 'Speyside';
            } else if (whisky.region === 'Campbeltown') {
                regionName = 'Campbeltown';
            }
        }
        if (whisky.district) {
            districtName = whisky.district;
        }
        if (whisky.country === 'Scotland') {
            countryName = 'Skotsko';
        }
        return (0, _inferno.createVNode)(2, 'main', _WhiskyDetail2.default.root + (regionClass ? ` ${regionClass}` : '') + (isSingleMalt ? ` ${_WhiskyDetail2.default.isSingleMalt}` : ''), [name(whisky), (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.leftColumn, [(0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.row, [countryName ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Country'), (0, _inferno.createVNode)(2, 'p', null, countryName)]) : null, regionName ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Region'), (0, _inferno.createVNode)(2, 'p', null, regionName)]) : null, districtName ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'District'), (0, _inferno.createVNode)(2, 'p', null, districtName)]) : null]), whisky.distillery || whisky.bottler ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.row, [whisky.distillery ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Distillery'), (0, _inferno.createVNode)(2, 'p', null, whisky.distillery)]) : null, whisky.bottler ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Bottler'), (0, _inferno.createVNode)(2, 'p', null, whisky.bottler)]) : null]) : null, whisky.age || whisky.vintage || whisky.bottled ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.row, [whisky.age ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Age'), (0, _inferno.createVNode)(2, 'p', null, `${whisky.age} let`)]) : null, whisky.vintage ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Vintage'), (0, _inferno.createVNode)(2, 'p', null, parseDate(whisky.vintage))]) : null, whisky.bottled ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Bottled'), (0, _inferno.createVNode)(2, 'p', null, parseDate(whisky.bottled))]) : null]) : null, (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.row, [whisky.strength ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Strength'), (0, _inferno.createVNode)(2, 'p', null, `${NUMBER_FORMAT.format(whisky.strength)} %`)]) : null, whisky.size ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Size'), (0, _inferno.createVNode)(2, 'p', null, `${whisky.size * ML_IN_L} ml`)]) : null, whisky.caskNumber && whisky.caskType ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Cask'), (0, _inferno.createVNode)(2, 'p', null, `${whisky.caskNumber} (${whisky.caskType})`)]) : null, !whisky.caskNumber && whisky.caskType ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.rowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Cask'), (0, _inferno.createVNode)(2, 'p', null, `${whisky.caskType}`)]) : null]), whisky.links && whisky.links.length ? (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.row, (0, _inferno.createVNode)(2, 'div', _WhiskyDetail2.default.wideRowItem, [(0, _inferno.createVNode)(2, 'h4', _WhiskyDetail2.default.attributeHeading, 'Links'), (0, _inferno.createVNode)(2, 'p', null, whisky.links.map((link, index) => (0, _inferno.createVNode)(2, 'a', null, link, {
            'href': link
        }, index)))])) : null]), whisky.images && whisky.images[0] ? (0, _inferno.createVNode)(2, 'figure', _WhiskyDetail2.default.rightColumn, (0, _inferno.createVNode)(2, 'img', null, null, {
            'src': (0, _href2.default)('assets', whisky.images[0]),
            'alt': ''
        }), {
            'style': {
                backgroundImage: `url("${(0, _href2.default)('assets', whisky.images[0])}")`
            }
        }) : null]);
    }
}
exports.default = WhiskyDetail;