'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = tintHex;

var _hexToRgb = require('./hexToRgb');

var _hexToRgb2 = _interopRequireDefault(_hexToRgb);

var _rgbToHex = require('./rgbToHex');

var _rgbToHex2 = _interopRequireDefault(_rgbToHex);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function tintHex(value, factor = 0) {
    let rgbValue = (0, _hexToRgb2.default)(value);
    rgbValue[0] += (255 - rgbValue[0]) * factor;
    rgbValue[1] += (255 - rgbValue[1]) * factor;
    rgbValue[2] += (255 - rgbValue[2]) * factor;
    return (0, _rgbToHex2.default)(rgbValue);
}