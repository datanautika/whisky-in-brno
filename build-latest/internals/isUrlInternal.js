'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = isUrlInternal;

var _isUrlExternal = require('./isUrlExternal');

var _isUrlExternal2 = _interopRequireDefault(_isUrlExternal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DOMAIN_REGEXP = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;
const MAILTO_REGEXP = /^mailto:/i;
function isUrlInternal(url) {
    return url && !MAILTO_REGEXP.test(url) && (!DOMAIN_REGEXP.test(url) || !(0, _isUrlExternal2.default)(url));
}