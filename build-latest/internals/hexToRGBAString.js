'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = hexToRgbaString;

var _hexToRgb = require('./hexToRgb');

var _hexToRgb2 = _interopRequireDefault(_hexToRgb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function hexToRgbaString(value, transparency = 1) {
    let rgbValue = (0, _hexToRgb2.default)(value);
    return `rgba(${rgbValue[0]}, ${rgbValue[1]}, ${rgbValue[2]}, ${transparency})`;
}