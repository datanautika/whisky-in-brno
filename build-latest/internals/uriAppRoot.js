'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
const ROUTE_STRIPPER = /^[#\/]|\s+$/g;
let uriAppRoot;
if (process.env.NODE_ENV === 'production') {
    uriAppRoot = ''.replace(ROUTE_STRIPPER, '');
} else {
    uriAppRoot = ''.replace(ROUTE_STRIPPER, '');
}
exports.default = uriAppRoot;