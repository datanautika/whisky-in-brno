'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    CLIENT_PLATFORM: 'client',
    SERVER_PLATFORM: 'server',
    EN_US: 'en-US',
    CS_CZ: 'cs-CZ',
    EN: 'en',
    CS: 'cs'
};