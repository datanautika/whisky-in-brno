'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _router = require('./router');

var _router2 = _interopRequireDefault(_router);

var _constants = require('../internals/constants');

var _constants2 = _interopRequireDefault(_constants);

var _uriAppRoot = require('../internals/uriAppRoot');

var _uriAppRoot2 = _interopRequireDefault(_uriAppRoot);

var _support = require('../internals/support');

var _support2 = _interopRequireDefault(_support);

var _isProbablyJson = require('../internals/isProbablyJson');

var _isProbablyJson2 = _interopRequireDefault(_isProbablyJson);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const EN_US = _constants2.default.EN_US;
const CS_CZ = _constants2.default.CS_CZ;
const CS = _constants2.default.CS;
const LANGUAGE_REGEX = new RegExp(CS);
const CLIENT_PLATFORM = _constants2.default.CLIENT_PLATFORM;
let routeStream = _router2.default.add(`${_uriAppRoot2.default.length ? `${_uriAppRoot2.default}(/)` : ''}(:language)(/:page)(/:subpage)(/:filter)(/)`).map(value => {
    let { language, page, subpage, filter, context } = value;
    if (process.env.NODE_ENV === 'development' && _support2.default.platform === CLIENT_PLATFORM) {
        let routeString = '';
        if (language) {
            routeString += `/${language}`;
        }
        if (page) {
            routeString += `/${page}`;
        }
        if (subpage) {
            routeString += `/${subpage}`;
        }
        if (filter) {
            routeString += `/${filter}`;
        }
        if (!routeString) {
            routeString = '/';
        }
        global.ga('set', 'page', routeString);
        global.ga('send', 'pageview');
    }
    if (typeof window !== 'undefined' && window.history && window.history.state && typeof window.history.state.scroll !== 'undefined') {
        if (window.history.state.scroll <= 10) {
            requestAnimationFrame(() => {
                window.scrollTo(0, window.history.state.scroll);
            });
        } else {
            setTimeout(() => {
                window.scrollTo(0, window.history.state.scroll);
            }, 40);
        }
    }
    if (language !== CS_CZ && language !== EN_US) {
        filter = subpage;
        subpage = page;
        page = language;
        language = EN_US;
        if (context) {
            if (LANGUAGE_REGEX.test(context.headers['accept-language'])) {
                language = CS_CZ;
            } else {
                language = EN_US;
            }
        }
    }
    if (subpage && (0, _isProbablyJson2.default)(subpage)) {
        filter = subpage;
        subpage = null;
    }
    if (filter && !(0, _isProbablyJson2.default)(filter)) {
        filter = null;
    }
    return { language, page, subpage, filter };
});
global.routeStream = routeStream;
exports.default = routeStream;