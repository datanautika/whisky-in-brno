'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _routeStream = require('./routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

var _Stream = require('../libs/Stream');

var _Stream2 = _interopRequireDefault(_Stream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let subpageStream = new _Stream2.default({
    previous: null,
    current: null
});
subpageStream.combine((self, changed, dependency) => {
    let value = self.value;
    let { subpage } = dependency.value;
    if (subpage !== value.current) {
        let newValue = {
            current: subpage,
            previous: value.current
        };
        self.push(newValue);
    }
}, _routeStream2.default);
exports.default = subpageStream;