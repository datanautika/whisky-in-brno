'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Stream = require('../libs/Stream');

var _Stream2 = _interopRequireDefault(_Stream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let locationStream = new _Stream2.default({
    latitude: 49.194924,
    longitude: 16.608363
});
let navigator = global.navigator;
if (navigator && navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
        locationStream.push({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        });
    });
    let watchId = navigator.geolocation.watchPosition(position => {
        locationStream.push({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        });
    });
}
global.locationStream = locationStream;
exports.default = locationStream;