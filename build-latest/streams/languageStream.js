'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _I18n = require('../libs/I18n');

var _I18n2 = _interopRequireDefault(_I18n);

var _Stream = require('../libs/Stream');

var _Stream2 = _interopRequireDefault(_Stream);

var _routeStream = require('./routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

var _constants = require('../internals/constants');

var _constants2 = _interopRequireDefault(_constants);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const EN_US = _constants2.default.EN_US;
const CS_CZ = _constants2.default.CS_CZ;
const EN = _constants2.default.EN;
const CS = _constants2.default.CS;
let i18n = new _I18n2.default();
global.i18n = i18n;
_moment2.default.locale(EN);
i18n.use({
    strings: _config2.default.i18nStrings,
    locale: EN_US,
    currency: '$'
});
let languageStram = new _Stream2.default({
    previous: null,
    current: null
});
global.languageStram = languageStram;
languageStram.combine((self, changed, dependency) => {
    let value = self.value;
    let { language } = dependency.value;
    if (language !== value.current && (language === CS_CZ || language === EN_US)) {
        let newValue = {
            current: language,
            previous: value.current
        };
        if (language === CS_CZ) {
            _moment2.default.locale(CS);
            i18n.use({
                strings: _config2.default.i18nStrings,
                locale: CS_CZ,
                currency: 'CZK'
            });
        } else if (language === EN_US) {
            _moment2.default.locale(EN);
            i18n.use({
                strings: _config2.default.i18nStrings,
                locale: EN_US,
                currency: '$'
            });
        }
        self.push(newValue);
    }
}, _routeStream2.default);
exports.default = languageStram;