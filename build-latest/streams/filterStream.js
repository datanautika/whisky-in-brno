'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _routeStream = require('./routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

var _Stream = require('../libs/Stream');

var _Stream2 = _interopRequireDefault(_Stream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DEFAULT_FILTER = {
    whiskyRegion: null,
    whiskyBrand: null,
    placeType: null,
    sortPlacesBy: 'distance',
    sortWhiskiesBy: 'score'
};
let filterStream = new _Stream2.default({
    previous: null,
    current: null,
    filter: DEFAULT_FILTER
});
global.filterStream = filterStream;
filterStream.combine((self, changed, dependency) => {
    let { current } = self.value;
    let { filter, page, subpage } = dependency.value;
    if (filter === null) {
        filter = encodeURIComponent(JSON.stringify(Object.assign({}, DEFAULT_FILTER, {
            sortWhiskiesBy: page === 'places' && subpage ? 'name' : 'score'
        })));
    }
    if (filter !== current) {
        let parsedFilter;
        try {
            parsedFilter = JSON.parse(decodeURIComponent(filter));
        } catch (error) {
            console.warn(error);
        }
        if (!parsedFilter) {
            parsedFilter = {
                whiskyRegion: null,
                whiskyBrand: null,
                placeType: null,
                sortPlacesBy: 'distance',
                sortWhiskiesBy: page === 'places' && subpage ? 'name' : 'score'
            };
        }
        if (!parsedFilter.whiskyRegion) {
            parsedFilter.whiskyRegion = null;
        }
        if (!parsedFilter.placeType) {
            parsedFilter.placeType = null;
        }
        if (!parsedFilter.whiskyBrand) {
            parsedFilter.whiskyBrand = null;
        }
        if (!parsedFilter.sortPlacesBy) {
            parsedFilter.sortPlacesBy = 'distance';
        }
        if (!parsedFilter.sortWhiskiesBy) {
            parsedFilter.sortWhiskiesBy = 'score';
        }
        self.push({
            current: filter,
            previous: current,
            filter: parsedFilter
        });
    }
}, _routeStream2.default);
exports.default = filterStream;