'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _geolib = require('geolib');

var _geolib2 = _interopRequireDefault(_geolib);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Stream = require('../libs/Stream');

var _Stream2 = _interopRequireDefault(_Stream);

var _data = require('../data');

var _filterStream = require('./filterStream');

var _filterStream2 = _interopRequireDefault(_filterStream);

var _locationStream = require('./locationStream');

var _locationStream2 = _interopRequireDefault(_locationStream);

var _getPlaceWhiskyRating = require('../utils/getPlaceWhiskyRating');

var _getPlaceWhiskyRating2 = _interopRequireDefault(_getPlaceWhiskyRating);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function sortPlacesByDistance(placesToSort) {
    return placesToSort.sort((placeA, placeB) => placeA.distance - placeB.distance);
}
let dataStream = new _Stream2.default({ places: _data.places, placeTypes: _data.placeTypes, whiskies: _data.whiskies, whiskyRegions: _data.whiskyRegions, whiskyBrands: _data.whiskyBrands });
dataStream.combine((self, changed) => {
    if (changed.includes(_locationStream2.default)) {
        let value = dataStream.value;
        let location = _locationStream2.default.value;
        let { filter } = _filterStream2.default.value;
        if (location.latitude && location.longitude) {
            _data.places.forEach(place => {
                place.distance = _geolib2.default.getDistance(location, place);
            });
        }
        if (filter && filter.sortPlacesBy === 'distance') {
            sortPlacesByDistance(value.places);
        }
        self.push({
            places: value.places,
            placeTypes: _data.placeTypes, whiskies: _data.whiskies, whiskyRegions: _data.whiskyRegions, whiskyBrands: _data.whiskyBrands
        });
    }
    if (changed.includes(_filterStream2.default)) {
        let { filter } = _filterStream2.default.value;
        let sortedPlaces = _data.places;
        sortedPlaces.forEach(place => {
            if (place.review) {
                place.review.bottles = place.review.allBottles;
            }
        });
        if (filter.whiskyRegion) {
            sortedPlaces = sortedPlaces.filter(place => place.review && place.review.bottles && place.review.bottles.some(bottle => !!bottle.whisky && bottle.whisky.region === filter.whiskyRegion));
            sortedPlaces.forEach(place => {
                if (place.review && place.review.bottles) {
                    place.review.bottles = place.review.bottles.filter(bottle => !!bottle.whisky && bottle.whisky.region === filter.whiskyRegion);
                }
            });
        }
        if (filter.whiskyBrand) {
            sortedPlaces = sortedPlaces.filter(place => place.review && place.review.bottles && place.review.bottles.some(bottle => !!bottle.whisky && (bottle.whisky.brand === filter.whiskyBrand || bottle.whisky.bottler === filter.whiskyBrand || bottle.whisky.distillery === filter.whiskyBrand)));
            sortedPlaces.forEach(place => {
                if (place.review && place.review.bottles) {
                    place.review.bottles = place.review.bottles.filter(bottle => !!bottle.whisky && (bottle.whisky.brand === filter.whiskyBrand || bottle.whisky.bottler === filter.whiskyBrand || bottle.whisky.distillery === filter.whiskyBrand));
                }
            });
        }
        sortedPlaces.forEach(place => {
            place.whiskyRating = (0, _getPlaceWhiskyRating2.default)(place, _data.whiskyRatingsMean, _data.whiskyRatingsSd);
        });
        if (filter.placeType) {
            sortedPlaces = sortedPlaces.filter(place => place.type === filter.placeType);
        }
        if (filter.sortPlacesBy === 'rating') {
            sortedPlaces.sort((placeA, placeB) => {
                let isPlaceAWhiskyRatingFinite = _lodash2.default.isFinite(placeA.whiskyRating);
                let isPlaceBWhiskyRatingFinite = _lodash2.default.isFinite(placeB.whiskyRating);
                if (!isPlaceAWhiskyRatingFinite && !isPlaceBWhiskyRatingFinite) {
                    return 0;
                }
                if (!isPlaceAWhiskyRatingFinite) {
                    return 1;
                }
                if (!isPlaceBWhiskyRatingFinite) {
                    return -1;
                }
                return placeB.whiskyRating - placeA.whiskyRating;
            });
        } else if (filter.sortPlacesBy === 'distance') {
            sortPlacesByDistance(sortedPlaces);
        } else if (filter.sortPlacesBy === 'name') {
            sortedPlaces.sort((placeA, placeB) => placeA.name.localeCompare(placeB.name));
        }
        if (filter.sortWhiskiesBy === 'price') {
            sortedPlaces.forEach(place => {
                if (place.review && place.review.bottles) {
                    place.review.bottles.sort((bottleA, bottleB) => {
                        let isBottleAWhiskyRatingFinite = bottleA.offers[0] ? _lodash2.default.isFinite(bottleA.offers[0].price.value) : false;
                        let isBottleBWhiskyRatingFinite = bottleB.offers[0] ? _lodash2.default.isFinite(bottleB.offers[0].price.value) : false;
                        if (!isBottleAWhiskyRatingFinite && !isBottleBWhiskyRatingFinite) {
                            return 0;
                        }
                        if (!isBottleAWhiskyRatingFinite) {
                            return 1;
                        }
                        if (!isBottleBWhiskyRatingFinite) {
                            return -1;
                        }
                        return bottleA.offers[0].price.value - bottleB.offers[0].price.value;
                    });
                }
            });
        } else if (filter.sortWhiskiesBy === 'score') {
            sortedPlaces.forEach(place => {
                if (place.review && place.review.bottles) {
                    place.review.bottles.sort((bottleA, bottleB) => {
                        let isBottleAWhiskyRatingFinite = bottleA.offers[0] ? _lodash2.default.isFinite(bottleA.offers[0].score) : false;
                        let isBottleBWhiskyRatingFinite = bottleB.offers[0] ? _lodash2.default.isFinite(bottleB.offers[0].score) : false;
                        if (!isBottleAWhiskyRatingFinite && !isBottleBWhiskyRatingFinite) {
                            return 0;
                        }
                        if (!isBottleAWhiskyRatingFinite) {
                            return 1;
                        }
                        if (!isBottleBWhiskyRatingFinite) {
                            return -1;
                        }
                        return bottleA.offers[0].score - bottleB.offers[0].score;
                    });
                }
            });
        } else if (filter.sortWhiskiesBy === 'name') {
            sortedPlaces.forEach(place => {
                if (place.review && place.review.bottles) {
                    place.review.bottles.sort((bottleA, bottleB) => {
                        if (bottleA.whisky && bottleB.whisky && bottleA.whisky.sortName && bottleB.whisky.sortName) {
                            return bottleA.whisky.sortName.localeCompare(bottleB.whisky.sortName);
                        }
                        return 0;
                    });
                }
            });
        }
        self.push({
            places: sortedPlaces,
            placeTypes: _data.placeTypes, whiskies: _data.whiskies, whiskyRegions: _data.whiskyRegions, whiskyBrands: _data.whiskyBrands
        });
    }
}, _filterStream2.default, _locationStream2.default);
global.dataStream = dataStream;
exports.default = dataStream;