'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _nconf = require('nconf');

var _nconf2 = _interopRequireDefault(_nconf);

var _secrets = require('./secrets');

var _secrets2 = _interopRequireDefault(_secrets);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_nconf2.default.argv();
_nconf2.default.env();
_nconf2.default.add('secrets', { type: 'literal', store: { secrets: _secrets2.default } });
exports.default = _nconf2.default;