'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _colors = require('./colors');

var _colors2 = _interopRequireDefault(_colors);

var _typographicScale = require('./typographicScale');

var _typographicScale2 = _interopRequireDefault(_typographicScale);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let heading = {
    color: _colors2.default.text,
    fontFamily: `Brandon Grotesque, Frutiger, 'Frutiger Linotype', Univers, Calibri, 'Gill Sans', 'Gill Sans MT', 'Myriad Pro', Myriad, 'DejaVu Sans Condensed', 'Liberation Sans', 'Nimbus Sans L', Tahoma, Geneva, 'Helvetica Neue', Helvetica, Arial, sans-serif`,
    fontFeatures: `'kern', 'liga', 'clig', 'calt', 'dlig', 'lnum', 'pnum'`,
    storyTitle: {
        fontSize: _typographicScale2.default[15],
        fontWeight: 700
    },
    storyLevel1: {
        fontSize: _typographicScale2.default[13],
        fontWeight: 700
    },
    storyLevel2: {
        fontSize: _typographicScale2.default[10],
        fontWeight: 700
    },
    storyLevel3: {
        fontSize: _typographicScale2.default[6],
        fontWeight: 700
    },
    storyLevel4: {
        fontSize: _typographicScale2.default[5],
        fontWeight: 700
    },
    sectionTitle: {
        fontSize: _typographicScale2.default[14],
        fontWeight: 700
    },
    sectionLevel1: {
        fontSize: _typographicScale2.default[6],
        fontWeight: 600
    },
    sectionLevel2: {
        fontSize: _typographicScale2.default[5],
        fontWeight: 600
    },
    sectionLevel3: {
        fontSize: _typographicScale2.default[4],
        fontWeight: 600
    },
    sectionLevel4: {
        fontSize: _typographicScale2.default[3],
        fontWeight: 600
    }
};
exports.default = heading;