"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
let breakpoints = {
    tinyMenu: {
        start: 1,
        end: 640
    },
    compactMenu: {
        start: 641,
        end: 1024
    },
    compactPage: {
        start: 1,
        end: 480
    },
    singleColumnPage: {
        start: 481,
        middle: 800,
        end: 1280
    }
};
exports.default = breakpoints;