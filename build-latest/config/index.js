'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _i18nStrings = require('./i18nStrings');

var _i18nStrings2 = _interopRequireDefault(_i18nStrings);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    styles: _styles2.default,
    i18nStrings: _i18nStrings2.default
};