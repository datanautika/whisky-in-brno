'use strict';

require('./internals/polyfills');

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _inferno = require('inferno');

var _inferno2 = _interopRequireDefault(_inferno);

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('./components/Page.css');

var _App = require('./components/App');

var _App2 = _interopRequireDefault(_App);

var _Stream = require('./libs/Stream');

var _Stream2 = _interopRequireDefault(_Stream);

var _browserRouter = require('./streams/browserRouter');

var _browserRouter2 = _interopRequireDefault(_browserRouter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (process.env.NODE_ENV === 'development') {
    global.Inferno = _inferno2.default;
    global.$ = _jquery2.default;
    global.Immutable = _immutable2.default;
    global.Stream = _Stream2.default;
    global.moment = _moment2.default;
}
if (process.env.NODE_ENV === 'development') {
    const G_KEY_CODE = 71;
    (0, _jquery2.default)(global.document).on('keydown', event => {
        let tagName = event.target.tagName.toLowerCase();
        if (event.keyCode === G_KEY_CODE && event.target && tagName !== 'textarea' && tagName !== 'input') {
            (0, _jquery2.default)('body').toggleClass('hasGrid');
        }
    });
}
_browserRouter2.default.start();
let rootNode = document.getElementById('app');
if (rootNode) {
    _inferno2.default.render((0, _inferno.createVNode)(16, _App2.default), rootNode);
}
(0, _jquery2.default)('#app').addClass('isLoaded');
const GA_ID = 'UA-90001694-1';
if (process.env.NODE_ENV === 'development') {
    global.ga('create', GA_ID, {
        cookieDomain: 'none'
    });
} else {
    global.ga('create', GA_ID, 'auto');
}