'use strict';

require('./internals/polyfills');

require('./internals/cssModules');

var _fs = require('fs');

var fs = _interopRequireWildcard(_fs);

var _path = require('path');

var path = _interopRequireWildcard(_path);

var _http = require('http');

var http = _interopRequireWildcard(_http);

var _https = require('https');

var https = _interopRequireWildcard(_https);

var _koa = require('koa');

var _koa2 = _interopRequireDefault(_koa);

var _koaCompress = require('koa-compress');

var _koaCompress2 = _interopRequireDefault(_koaCompress);

var _koaLogger = require('koa-logger');

var _koaLogger2 = _interopRequireDefault(_koaLogger);

var _koaBodyparser = require('koa-bodyparser');

var _koaBodyparser2 = _interopRequireDefault(_koaBodyparser);

var _koaStatic = require('koa-static');

var _koaStatic2 = _interopRequireDefault(_koaStatic);

var _infernoServer = require('inferno-server');

var _infernoServer2 = _interopRequireDefault(_infernoServer);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _appRoot = require('./internals/appRoot');

var _appRoot2 = _interopRequireDefault(_appRoot);

var _uriAppRoot = require('./internals/uriAppRoot');

var _uriAppRoot2 = _interopRequireDefault(_uriAppRoot);

var _App = require('./components/App');

var _App2 = _interopRequireDefault(_App);

var _routerMiddleware = require('./libs/routerMiddleware');

var _routerMiddleware2 = _interopRequireDefault(_routerMiddleware);

var _router = require('./streams/router');

var _router2 = _interopRequireDefault(_router);

var _forceHttpsMiddleware = require('./internals/forceHttpsMiddleware');

var _forceHttpsMiddleware2 = _interopRequireDefault(_forceHttpsMiddleware);

var _inferno = require('inferno');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const HTTP_NOT_FOUND = 404;
const HTTP_INTERNAL_ERROR = 500;
const HTTPS_PORT = 443;
let readFile = _bluebird2.default.promisify(fs.readFile);
let app = new _koa2.default();
console.log('App root dir:', `"${_appRoot2.default}"`);
console.log('App root URL:', `"${_uriAppRoot2.default}"`);
app.use((0, _forceHttpsMiddleware2.default)({
    trustAzureHeader: true
}));
app.use((0, _koaBodyparser2.default)());
app.use((0, _koaLogger2.default)());
app.use((() => {
    var _ref = _asyncToGenerator(function* (context, next) {
        try {
            yield next();
        } catch (error) {
            console.warn(error);
            context.status = error.status || HTTP_INTERNAL_ERROR;
            context.body = {
                error: {
                    message: error.message
                }
            };
            if (context.app) {
                context.app.emit('error', error, context);
            }
        }
    });

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
})());
app.use((0, _koaStatic2.default)(path.resolve(path.join(_appRoot2.default, 'public'))));
app.use((0, _routerMiddleware2.default)(_router2.default, (() => {
    var _ref2 = _asyncToGenerator(function* (context, next) {
        if (context.method !== 'HEAD' && context.method !== 'GET') {
            yield next();
            return;
        }
        if (context.body && context.body !== null || context.status !== HTTP_NOT_FOUND) {
            yield next();
            return;
        }
        let componentHTML = _infernoServer2.default.renderToString((0, _inferno.createVNode)(16, _App2.default));
        context.body = yield readFile(path.join(_appRoot2.default, 'assets/templates/index.html'), 'utf8');
        context.body = context.body.replace('%{content}', componentHTML).replace(new RegExp('%{appRoot}', 'g'), _uriAppRoot2.default);
        yield next();
    });

    return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
    };
})()));
app.use((0, _koaCompress2.default)());
const PORT = 8080;
let httpsOptions = {};
if (process.env.NODE_ENV === 'development') {
    httpsOptions.key = fs.readFileSync('./localhost-key.pem');
    httpsOptions.cert = fs.readFileSync('./localhost-cert.pem');
}
let serverErrorListener = error => {
    console.log(error);
};
http.createServer(app.callback()).listen(process.env.PORT || PORT).on('error', serverErrorListener);
https.createServer(httpsOptions, app.callback()).listen(HTTPS_PORT).on('error', serverErrorListener);
console.log(`Listening on port ${process.env.PORT || PORT}...`);