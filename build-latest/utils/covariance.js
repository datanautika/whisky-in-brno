'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mean = require('./mean');

var _mean2 = _interopRequireDefault(_mean);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function covariance(array1, array2) {
    let n = array1.length;
    if (n < 2) {
        return 0;
    }
    let m1 = (0, _mean2.default)(array1);
    let m2 = (0, _mean2.default)(array2);
    let result = 0;
    for (let i = 0; i < array1.length; i++) {
        let a = array1[i] - m1;
        let b = array2[i] - m2;
        result += a * b / (n - 1);
    }
    return result;
}
exports.default = covariance;