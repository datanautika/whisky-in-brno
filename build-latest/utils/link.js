'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = link;
function link(...levels) {
    return `${process.env.APP_ROOT ? `${process.env.APP_ROOT}` : ''}/${levels.join('/')}`;
}