'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getPlaceWhiskyRating;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const WHISKY_RATING_MEAN = 100;
const WHISKY_RATING_SD = 50;
function getPlaceWhiskyRating(place, mean, sd) {
    let whiskyRatingsSum = 0;
    let whiskyRatingsCount = 0;
    place.review.bottles.forEach(bottle => {
        bottle.offers.forEach(offer => {
            let rating = 1 / offer.score;
            if (rating && _lodash2.default.isFinite(rating)) {
                whiskyRatingsSum += rating;
                whiskyRatingsCount++;
            }
        });
    });
    if (whiskyRatingsCount && _lodash2.default.isFinite(whiskyRatingsSum)) {
        return (whiskyRatingsSum / whiskyRatingsCount - mean) / sd * WHISKY_RATING_SD + WHISKY_RATING_MEAN;
    }
    return NaN;
}