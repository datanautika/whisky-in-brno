'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getCurrentLocation;

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getCurrentLocation() {
    return new _bluebird2.default((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(position => {
            resolve({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            });
        });
    });
}