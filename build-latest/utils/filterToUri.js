'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = filterToUri;

var _filterStream = require('../streams/filterStream');

var _filterStream2 = _interopRequireDefault(_filterStream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function filterToUri(newFilter) {
    return encodeURIComponent(JSON.stringify(Object.assign({}, _filterStream2.default.value.filter, newFilter)));
}