'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = variance;

var _covariance = require('./covariance');

var _covariance2 = _interopRequireDefault(_covariance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function variance(array) {
    return (0, _covariance2.default)(array, array);
}