'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = flattenPlaceReviews;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function flattenPlaceReviews(place) {
    place.review = _lodash2.default.assign({}, ...place.reviews);
    return place;
}