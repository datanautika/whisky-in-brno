'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = mean;

var _product = require('./product');

var _product2 = _interopRequireDefault(_product);

var _sum = require('./sum');

var _sum2 = _interopRequireDefault(_sum);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function mean(array) {
    return (0, _product2.default)([(0, _sum2.default)(array), 1 / array.length]);
}