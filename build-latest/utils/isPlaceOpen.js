'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = isPlaceOpen;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _parsePlaceOpeningHours = require('./parsePlaceOpeningHours');

var _parsePlaceOpeningHours2 = _interopRequireDefault(_parsePlaceOpeningHours);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function isPlaceOpen(place) {
    if (place.reivew && !place.reivew.isOperational) {
        return false;
    }
    let currentTime = (0, _moment2.default)();
    (0, _parsePlaceOpeningHours2.default)(place);
    for (let i = 0; i < place.openingHours.length; i++) {
        if (currentTime.isBetween(place.openingHours[i].startTime, place.openingHours[i].endTime) || currentTime.isBetween(place.openingHours[i].startTime.clone().add(1, 'week'), place.openingHours[i].endTime.clone().add(1, 'week')) || currentTime.isBetween(place.openingHours[i].startTime.clone().subtract(1, 'week'), place.openingHours[i].endTime.clone().subtract(1, 'week'))) {
            return true;
        }
    }
    return false;
}