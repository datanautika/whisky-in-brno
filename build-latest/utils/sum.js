"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = sum;
function sum(array) {
    let s = 0;
    let c = 0;
    for (let i = 0; i < array.length; i++) {
        if (!isFinite(array[i])) {
            continue;
        }
        let y = array[i] - c;
        let t = s + y;
        c = t - s - y;
        s = t;
    }
    return s;
}