'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = stringLocaleDayOfWeekToNumber;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function stringLocaleDayOfWeekToNumber(value) {
    let dayName = value.toLowerCase().trim();
    let day;
    switch (dayName) {
        case 'monday':
            day = 1;
            break;
        case 'tuesday':
            day = 2;
            break;
        case 'wednesday':
            day = 3;
            break;
        case 'thursday':
            day = 4;
            break;
        case 'friday':
            day = 5;
            break;
        case 'saturday':
            day = 6;
            break;
        default:
            day = 0;
            break;
    }
    day -= _moment2.default.localeData().firstDayOfWeek();
    if (day < 0) {
        day = 6;
    }
    if (day > 6) {
        day = 0;
    }
    return day;
}