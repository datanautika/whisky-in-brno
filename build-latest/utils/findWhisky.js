'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = findWhisky;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function findWhisky(whiskies, whiskyId) {
    return _lodash2.default.cloneDeep(_lodash2.default.find(whiskies, { id: whiskyId }));
}