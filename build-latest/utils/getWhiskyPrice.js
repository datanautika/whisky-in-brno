'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getWhiskyName;
const EUR_IN_CZK = 27.02;
const USD_IN_CZK = 25.97;
const GBP_IN_CZK = 31.78;
function getWhiskyName(whisky) {
    let sum = 0;
    let count = 0;
    whisky.prices.forEach(price => {
        if (price.currency === 'CZK') {
            sum += price.value;
            count++;
        } else if (price.currency === 'EUR') {
            sum += price.value * EUR_IN_CZK;
            count++;
        } else if (price.currency === 'USD') {
            sum += price.value * USD_IN_CZK;
            count++;
        } else if (price.currency === 'GBP') {
            sum += price.value * GBP_IN_CZK;
            count++;
        }
    });
    return sum / count;
}