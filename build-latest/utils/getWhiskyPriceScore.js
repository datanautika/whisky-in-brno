'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getWhiskyPriceScore;

var _getWhiskyPrice = require('../utils/getWhiskyPrice');

var _getWhiskyPrice2 = _interopRequireDefault(_getWhiskyPrice);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getWhiskyPriceScore(whisky, offer) {
    if (whisky.prices.length) {
        let whiskyPrice = (0, _getWhiskyPrice2.default)(whisky);
        return offer.price.value / (offer.size / whisky.size * whiskyPrice);
    }
    return NaN;
}