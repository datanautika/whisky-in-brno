'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = href;

var _uriAppRoot = require('../internals/uriAppRoot');

var _uriAppRoot2 = _interopRequireDefault(_uriAppRoot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function href(...levels) {
    return `${_uriAppRoot2.default ? `${_uriAppRoot2.default}` : ''}/${levels.join('/')}`;
}