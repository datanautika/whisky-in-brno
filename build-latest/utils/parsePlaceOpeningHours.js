'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = parsePlaceOpeningHours;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _stringLocaleDayOfWeekToNumber = require('./stringLocaleDayOfWeekToNumber');

var _stringLocaleDayOfWeekToNumber2 = _interopRequireDefault(_stringLocaleDayOfWeekToNumber);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function parsePlaceOpeningHours(place) {
    for (let i = 0; i < place.openingHours.length; i++) {
        let [startDay, startTime] = place.openingHours[i].start.split(' ');
        let [endDay, endTime] = place.openingHours[i].end.split(' ');
        place.openingHours[i].startTime = (0, _moment2.default)(`${(0, _stringLocaleDayOfWeekToNumber2.default)(startDay)} ${startTime}`, 'e HH:mm');
        place.openingHours[i].endTime = (0, _moment2.default)(`${(0, _stringLocaleDayOfWeekToNumber2.default)(endDay)} ${endTime}`, 'e HH:mm');
        if (place.openingHours[i].endTime.isBefore(place.openingHours[i].startTime)) {
            place.openingHours[i].endTime.add(1, 'week');
        }
    }
}