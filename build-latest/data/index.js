'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.whiskyRegions = exports.placeTypes = exports.whiskyBrands = exports.whiskies = exports.places = exports.placeRatingsSd = exports.placeRatingsMean = exports.whiskyRatingsSd = exports.whiskyRatingsMean = undefined;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _rawWhiskies = require('./rawWhiskies');

var _rawWhiskies2 = _interopRequireDefault(_rawWhiskies);

var _rawPlaces = require('./rawPlaces');

var _rawPlaces2 = _interopRequireDefault(_rawPlaces);

var _flattenPlaceReviews = require('../utils/flattenPlaceReviews');

var _flattenPlaceReviews2 = _interopRequireDefault(_flattenPlaceReviews);

var _getWhiskyPriceScore = require('../utils/getWhiskyPriceScore');

var _getWhiskyPriceScore2 = _interopRequireDefault(_getWhiskyPriceScore);

var _findWhisky = require('../utils/findWhisky');

var _findWhisky2 = _interopRequireDefault(_findWhisky);

var _mean = require('../utils/mean');

var _mean2 = _interopRequireDefault(_mean);

var _sd = require('../utils/sd');

var _sd2 = _interopRequireDefault(_sd);

var _parsePlaceOpeningHours = require('../utils/parsePlaceOpeningHours');

var _parsePlaceOpeningHours2 = _interopRequireDefault(_parsePlaceOpeningHours);

var _isPlaceOpen = require('../utils/isPlaceOpen');

var _isPlaceOpen2 = _interopRequireDefault(_isPlaceOpen);

var _getWhiskyName = require('../utils/getWhiskyName');

var _getWhiskyName2 = _interopRequireDefault(_getWhiskyName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const WHISKY_RATING_MEAN = 100;
const WHISKY_RATING_SD = 50;
const HOURS_IN_DAY = 24;
const DAYS_IN_WEEK = 7;
let rawPlaces = _rawPlaces2.default;
let rawWhiskies = _rawWhiskies2.default;
rawWhiskies.forEach(whisky => {
    whisky.fullName = (0, _getWhiskyName2.default)(whisky);
    whisky.sortName = whisky.fullName.map(name => name.trim()).join(' ');
    whisky.readableId = `${whisky.id}-${_lodash2.default.kebabCase(`${whisky.fullName[0].trim()} ${whisky.fullName[1].trim()}`)}`;
    if (whisky.links) {
        whisky.links = whisky.links.filter(link => !!link);
    }
});
rawPlaces.forEach(place => {
    place.reviews.forEach(review => {
        review.date = (0, _moment2.default)(review.date);
    });
    place.reviews.sort((ratingA, ratingB) => ratingA.date.valueOf() - ratingB.date.valueOf());
    place.isOpen = (0, _isPlaceOpen2.default)(place);
    place.readableId = `${place.id}-${_lodash2.default.kebabCase(place.name.trim())}`;
});
rawPlaces = rawPlaces.map(_flattenPlaceReviews2.default);
let whiskyRatings = [];
let placeRatings = [];
rawPlaces.forEach(place => {
    let whiskyRatingsSum = 0;
    let whiskyRatingsCount = 0;
    if (place.review && place.review.bottles) {
        place.review.bottles.map(bottle => {
            bottle.whisky = (0, _findWhisky2.default)(rawWhiskies, bottle.whiskyId);
            if (bottle.whisky) {
                bottle.offers.forEach(offer => {
                    let score = bottle.whisky ? (0, _getWhiskyPriceScore2.default)(bottle.whisky, offer) : NaN;
                    if (_lodash2.default.isFinite(1 / score)) {
                        whiskyRatingsSum += 1 / score;
                        whiskyRatingsCount++;
                        offer.score = score;
                        whiskyRatings.push(1 / score);
                    } else {
                        offer.score = NaN;
                    }
                });
            }
        });
        place.review.allBottles = place.review.bottles;
    }
    if (_lodash2.default.isFinite(whiskyRatingsSum)) {
        place.whiskyRating = whiskyRatingsSum / whiskyRatingsCount;
    } else {
        place.whiskyRating = NaN;
    }
    let placeScore = 0;
    let openingHoursSum = 0;
    (0, _parsePlaceOpeningHours2.default)(place);
    for (let i = 0; i < place.openingHours.length; i++) {
        if (_moment2.default.isMoment(place.openingHours[i].endTime)) {
            openingHoursSum += place.openingHours[i].endTime.diff(place.openingHours[i].startTime, 'hours');
        }
    }
    place.placeOpeningHoursScore = openingHoursSum / (HOURS_IN_DAY * DAYS_IN_WEEK / 10);
    placeScore += place.placeOpeningHoursScore;
    if (place.review) {
        if (place.review.bottles && place.review.bottles.length) {
            place.placeWhiskiesCountScore = place.review.bottles.length * 0.25;
            placeScore += place.placeWhiskiesCountScore;
        }
        if (place.review.glass === 'glencairn') {
            place.placeGlassScore = 2;
            placeScore += place.placeGlassScore;
        } else if (place.review.glass === 'tulip tasting') {
            place.placeGlassScore = 1;
            placeScore += place.placeGlassScore;
        } else {
            place.placeGlassScore = 0;
        }
        if (_lodash2.default.isNumber(place.review.ambient) && _lodash2.default.isFinite(place.review.ambient)) {
            place.placeAmbientScore = place.review.ambient;
            placeScore += place.placeAmbientScore;
        }
        if (_lodash2.default.isNumber(place.review.serviceInformedness) && _lodash2.default.isFinite(place.review.serviceInformedness)) {
            place.placeServiceInformednessScore = place.review.serviceInformedness;
            placeScore += place.placeServiceInformednessScore;
        }
        if (_lodash2.default.isNumber(place.review.serviceAmiability) && _lodash2.default.isFinite(place.review.serviceAmiability)) {
            place.placeServiceAmiabilityScore = place.review.serviceAmiability;
            placeScore += place.placeServiceAmiabilityScore;
        }
        if (_lodash2.default.isNumber(place.review.restroomCleanness) && _lodash2.default.isFinite(place.review.restroomCleanness)) {
            place.placeRestroomCleannessScore = place.review.restroomCleanness;
            placeScore += place.placeRestroomCleannessScore;
        }
        if (place.review.towels === 'paper') {
            place.placeTowelsScore = 1;
            placeScore += place.placeTowelsScore;
        } else if (place.review.towels === 'cloth') {
            place.placeTowelsScore = 2;
            placeScore += place.placeTowelsScore;
        } else {
            place.placeTowelsScore = 0;
        }
        if (place.review.isCashOnly) {
            place.placeIsCashOnlyScore = 0;
        } else {
            place.placeIsCashOnlyScore = 1;
            placeScore += place.placeIsCashOnlyScore;
        }
        if (place.review.isSmokingAllowed) {
            place.placeIsSmokingAllowedScore = 0;
        } else {
            place.placeIsSmokingAllowedScore = 1;
            placeScore += place.placeIsSmokingAllowedScore;
        }
    }
    if (_lodash2.default.isFinite(placeScore)) {
        place.placeScore = placeScore;
        place.placeRating = placeScore;
        placeRatings.push(place.placeRating);
    } else {
        place.placeScore = NaN;
    }
});
let whiskyRatingsMean = exports.whiskyRatingsMean = (0, _mean2.default)(whiskyRatings);
let whiskyRatingsSd = exports.whiskyRatingsSd = (0, _sd2.default)(whiskyRatings);
let placeRatingsMean = exports.placeRatingsMean = (0, _mean2.default)(placeRatings);
let placeRatingsSd = exports.placeRatingsSd = (0, _sd2.default)(placeRatings);
rawPlaces.forEach(place => {
    if (place.review && place.review.bottles) {
        place.review.bottles.map(bottle => {
            bottle.offers.forEach(offer => {
                if (_lodash2.default.isNumber(offer.rating) && _lodash2.default.isFinite(offer.rating)) {
                    offer.rating = (1 / offer.score - whiskyRatingsMean) / whiskyRatingsSd * WHISKY_RATING_SD + WHISKY_RATING_MEAN;
                }
            });
        });
    }
    if (_lodash2.default.isNumber(place.whiskyRating) && _lodash2.default.isFinite(place.whiskyRating)) {
        place.whiskyRating = (place.whiskyRating - whiskyRatingsMean) / whiskyRatingsSd * WHISKY_RATING_SD + WHISKY_RATING_MEAN;
    }
    if (_lodash2.default.isNumber(place.placeRating) && _lodash2.default.isFinite(place.placeRating)) {
        place.placeRating = (place.placeRating - placeRatingsMean) / placeRatingsSd * WHISKY_RATING_SD + WHISKY_RATING_MEAN;
    }
    if (place.review && place.review.bottles) {
        place.review.bottles.sort((a, b) => a.offers[0].score - b.offers[0].score);
    }
});
rawPlaces.forEach(place => {
    if (place.review && place.review.bottles) {
        place.review.bottles.map(bottle => {
            if (bottle.whisky) {
                let whisky = _lodash2.default.find(rawWhiskies, { id: bottle.whiskyId });
                if (whisky && whisky.places) {
                    whisky.places.push(place);
                } else if (whisky) {
                    whisky.places = [place];
                }
            }
        });
    }
});
let places = exports.places = rawPlaces;
let whiskies = exports.whiskies = rawWhiskies;
let whiskyBrands = rawWhiskies.map(whisky => {
    if (whisky.brand) {
        return whisky.brand;
    }
    if (whisky.bottler) {
        return whisky.bottler;
    }
    if (whisky.distillery) {
        return whisky.distillery;
    }
    return '';
});
exports.whiskyBrands = whiskyBrands = whiskyBrands.filter(whiskyBrand => !!whiskyBrand);
exports.whiskyBrands = whiskyBrands = _lodash2.default.uniq(whiskyBrands);
exports.whiskyBrands = whiskyBrands = _lodash2.default.sortBy(whiskyBrands);
let placeTypes = rawPlaces.map(place => place.type.toLowerCase());
exports.placeTypes = placeTypes = _lodash2.default.uniq(placeTypes);
exports.placeTypes = placeTypes = _lodash2.default.sortBy(placeTypes);
let whiskyRegions = rawWhiskies.map(whisky => whisky.region);
exports.whiskyRegions = whiskyRegions = _lodash2.default.uniq(whiskyRegions);
exports.whiskyRegions = whiskyRegions = whiskyRegions.filter(whiskyRegion => !!whiskyRegion);
exports.whiskyRegions = whiskyRegions = _lodash2.default.sortBy(whiskyRegions);
exports.whiskyBrands = whiskyBrands;
exports.placeTypes = placeTypes;
exports.whiskyRegions = whiskyRegions;