"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
let rawPlaces = [{
    "id": 0,
    "name": "Bar, který neexistuje",
    "type": "bar",
    "description": {
        "cs-CZ": "Bar s velkým B, alespoň pro nás. Velký výběr skotských a jiných druhů alkoholu, uklidňující atmosféra a skvělá obsluha. Není divu, že mají vždy narváno.",
        "en-US": "The Bar of Brno, at least for us. Large selection of Scotch and other types of beverages, relaxing atmosphere and great service. No wonder it’s always packed."
    },
    "address": "",
    "links": [],
    "latitude": 49.195870,
    "longitude": 16.609730,
    "openingHours": [{
        "start": "monday 17:00",
        "end": "tuesday 02:00"
    }, {
        "start": "tuesday 17:00",
        "end": "wednesday 02:00"
    }, {
        "start": "wednesday 17:00",
        "end": "thursday 03:00"
    }, {
        "start": "thursday 17:00",
        "end": "friday 03:00"
    }, {
        "start": "friday 17:00",
        "end": "saturday 04:00"
    }, {
        "start": "saturday 17:00",
        "end": "sunday 04:00"
    }, {
        "start": "sunday 17:00",
        "end": "monday 02:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-10-14",
        "glass": "tulip tasting",
        "ambient": 9,
        "serviceInformedness": 9,
        "serviceAmiability": 10,
        "restroomCleanness": 8,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 3,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 0,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 270
                }
            }]
        }, {
            "whiskyId": 40,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 23,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 72,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 270
                }
            }]
        }, {
            "whiskyId": 73,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 500
                }
            }]
        }, {
            "whiskyId": 41,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 74,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 75,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 36,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 500
                }
            }]
        }, {
            "whiskyId": 42,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 34,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 64,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 10,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 76,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 380
                }
            }]
        }, {
            "whiskyId": 19,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 77,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 78,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 270
                }
            }]
        }, {
            "whiskyId": 79,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 380
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 11,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 80,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 81,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 270
                }
            }]
        }, {
            "whiskyId": 17,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 82,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 850
                }
            }]
        }, {
            "whiskyId": 9,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 71,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 83,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 84,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 85,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 86,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 87,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 270
                }
            }]
        }, {
            "whiskyId": 88,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 270
                }
            }]
        }, {
            "whiskyId": 44,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 8,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 89,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 54,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 90,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 850
                }
            }]
        }]
    }]
}, {
    "id": 1,
    "name": "1920 London",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.193409,
    "longitude": 16.607005,
    "openingHours": [{
        "start": "monday 18:00",
        "end": "tuesday 02:00"
    }, {
        "start": "tuesday 18:00",
        "end": "wednesday 02:00"
    }, {
        "start": "wednesday 18:00",
        "end": "thursday 02:00"
    }, {
        "start": "thursday 18:00",
        "end": "friday 02:00"
    }, {
        "start": "friday 18:00",
        "end": "saturday 04:00"
    }, {
        "start": "saturday 18:00",
        "end": "sunday 04:00"
    }, {
        "start": "sunday 18:00",
        "end": "monday 02:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-15",
        "glass": "tulip tasting",
        "ambient": 9,
        "serviceInformedness": 7,
        "serviceAmiability": 9,
        "restroomCleanness": 7,
        "towels": "paper",
        "isCashOnly": true,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 3,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 50,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 51,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 52,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 480
                }
            }]
        }, {
            "whiskyId": 53,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 380
                }
            }]
        }, {
            "whiskyId": 54,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 55,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 320
                }
            }]
        }, {
            "whiskyId": 56,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 57,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 200
                }
            }]
        }, {
            "whiskyId": 58,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 36,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 59,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 420
                }
            }]
        }, {
            "whiskyId": 60,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 61,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 260
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 62,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 34,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 63,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 30,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 48,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 64,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 65,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 66,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 67,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 68,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 300
                }
            }]
        }, {
            "whiskyId": 69,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 70,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 200
                }
            }]
        }, {
            "whiskyId": 71,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }]
    }, {
        "raterId": 0,
        "date": "2017-02-11",
        "glass": "tulip tasting",
        "ambient": 9,
        "serviceInformedness": 8,
        "serviceAmiability": 9,
        "restroomCleanness": 7,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 50,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 51,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 3,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 52,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 480
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 53,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 400
                }
            }]
        }, {
            "whiskyId": 54,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 55,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 360
                }
            }]
        }, {
            "whiskyId": 56,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 57,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 200
                }
            }]
        }, {
            "whiskyId": 58,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 36,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 59,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 440
                }
            }]
        }, {
            "whiskyId": 60,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 61,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 280
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 34,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 63,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 30,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 48,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 64,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 100
                }
            }]
        }, {
            "whiskyId": 65,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 66,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 67,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 68,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 300
                }
            }]
        }, {
            "whiskyId": 69,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 70,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 200
                }
            }]
        }, {
            "whiskyId": 71,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }]
    }, {
        "raterId": 0,
        "date": "2017-03-23",
        "isOperational": false
    }, {
        "raterId": 0,
        "date": "2017-05-18",
        "glass": "tulip tasting",
        "ambient": 9,
        "serviceInformedness": 6,
        "serviceAmiability": 7,
        "restroomCleanness": 6,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true
    }]
}, {
    "id": 2,
    "name": "Rotor bar",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.196035,
    "longitude": 16.611179,
    "openingHours": [{
        "start": "monday 08:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 08:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 08:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 08:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 08:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 18:00",
        "end": "sunday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-02-10",
        "glass": "tulip tasting",
        "ambient": 7,
        "serviceInformedness": 10,
        "serviceAmiability": 8,
        "restroomCleanness": 9,
        "towels": "paper",
        "isCashOnly": true,
        "isSmokingAllowed": true,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 11,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 98
                }
            }]
        }, {
            "whiskyId": 46,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 93,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 94,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 13,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 9,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 8,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 63,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 98
                }
            }]
        }, {
            "whiskyId": 20,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 98
                }
            }]
        }, {
            "whiskyId": 85,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 0,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 23,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 98
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 98
                }
            }]
        }, {
            "whiskyId": 29,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 98
                }
            }]
        }, {
            "whiskyId": 95,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 34,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 96,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 290
                }
            }]
        }, {
            "whiskyId": 42,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 97,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 25,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 98
                }
            }]
        }, {
            "whiskyId": 98,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 88,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 3,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 99,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 100,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 250
                }
            }]
        }]
    }]
}, {
    "id": 3,
    "name": "Super Panda Circus",
    "type": "bar",
    "description": {
        "cs-CZ": "Jeden z nejzvláštnějších a nejlepších barů v Česku. Výběr skotských je základní, milovníky whisky z jiných zemí však potěší nabídkou lahví z Japonska či Indie.",
        "en-US": "One of the strangest and finest bars in Czechia. Selection of Scotch is basic, but the lovers of whiskies from other countries will be pleased with an assortment of bottles form Japan or India."
    },
    "address": "Šilingrovo náměstí 257/3",
    "links": ["https://www.facebook.com/superpandabrno"],
    "latitude": 49.192662,
    "longitude": 16.605160,
    "openingHours": [{
        "start": "monday 18:00",
        "end": "tuesday 02:00"
    }, {
        "start": "tuesday 18:00",
        "end": "wednesday 02:00"
    }, {
        "start": "wednesday 18:00",
        "end": "thursday 02:00"
    }, {
        "start": "thursday 18:00",
        "end": "friday 02:00"
    }, {
        "start": "friday 18:00",
        "end": "saturday 02:00"
    }, {
        "start": "saturday 18:00",
        "end": "sunday 02:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-11-12",
        "glass": "tulip tasting",
        "ambient": 10,
        "serviceInformedness": 10,
        "serviceAmiability": 8,
        "restroomCleanness": 8,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 3,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 40,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 41,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 36,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 42,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 34,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 10,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 44,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 500
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }]
    }]
}, {
    "id": 4,
    "name": "Cohiba Club Conti",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.200431,
    "longitude": 16.604495,
    "openingHours": [{
        "start": "monday 16:00",
        "end": "tuesday 02:00"
    }, {
        "start": "tuesday 16:00",
        "end": "wednesday 02:00"
    }, {
        "start": "wednesday 16:00",
        "end": "thursday 02:00"
    }, {
        "start": "thursday 16:00",
        "end": "friday 02:00"
    }, {
        "start": "friday 16:00",
        "end": "saturday 02:00"
    }, {
        "start": "saturday 16:00",
        "end": "sunday 02:00"
    }, {
        "start": "sunday 16:00",
        "end": "monday 02:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-10-15",
        "glass": "glencairn",
        "ambient": 9,
        "serviceInformedness": 9,
        "serviceAmiability": 10,
        "restroomCleanness": 10,
        "towels": "cloth",
        "isCashOnly": false,
        "isSmokingAllowed": true,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 3,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 28,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 29,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 19,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 30,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 31,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 8,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 20,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 33,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 34,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 35,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 36,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 14,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 37,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 38,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 220
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 260
                }
            }]
        }, {
            "whiskyId": 25,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 290
                }
            }]
        }]
    }]
}, {
    "id": 5,
    "name": "Spirit Bar",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.192483,
    "longitude": 16.611226,
    "openingHours": [{
        "start": "monday 17:00",
        "end": "tuesday 01:00"
    }, {
        "start": "tuesday 17:00",
        "end": "wednesday 02:00"
    }, {
        "start": "wednesday 17:00",
        "end": "thursday 02:00"
    }, {
        "start": "thursday 17:00",
        "end": "friday 02:00"
    }, {
        "start": "friday 17:00",
        "end": "saturday 03:00"
    }, {
        "start": "saturday 17:00",
        "end": "sunday 03:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-08",
        "glass": "vysoká sherry glass, skoro jako šampaňské",
        "ambient": 8,
        "serviceInformedness": 7,
        "serviceAmiability": 6,
        "restroomCleanness": null,
        "towels": null,
        "isCashOnly": true,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 36,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 46,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 47,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 42,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 380
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 48,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 49,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }]
    }]
}, {
    "id": 6,
    "name": "SKØG Urban Hub",
    "type": "coffeehouse",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.193822,
    "longitude": 16.607291,
    "openingHours": [{
        "start": "monday 08:00",
        "end": "tuesday 01:00"
    }, {
        "start": "tuesday 08:00",
        "end": "wednesday 01:00"
    }, {
        "start": "wednesday 08:00",
        "end": "thursday 01:00"
    }, {
        "start": "thursday 08:00",
        "end": "friday 01:00"
    }, {
        "start": "friday 08:00",
        "end": "saturday 02:00"
    }, {
        "start": "saturday 10:00",
        "end": "sunday 02:00"
    }, {
        "start": "sunday 12:00",
        "end": "sunday 22:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-10-12",
        "glass": "glencairn",
        "ambient": 8,
        "serviceInformedness": 9,
        "serviceAmiability": 9,
        "restroomCleanness": 7,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 97
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 107
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 157
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 297
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 97
                }
            }]
        }, {
            "whiskyId": 8,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 127
                }
            }]
        }, {
            "whiskyId": 9,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 127
                }
            }]
        }, {
            "whiskyId": 10,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 107
                }
            }]
        }, {
            "whiskyId": 11,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 97
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 97
                }
            }]
        }]
    }, {
        "raterId": 1,
        "date": "2017-02-04",
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 107
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 117
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 167
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 317
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 107
                }
            }]
        }, {
            "whiskyId": 8,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 137
                }
            }]
        }, {
            "whiskyId": 9,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 137
                }
            }]
        }, {
            "whiskyId": 10,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 117
                }
            }]
        }, {
            "whiskyId": 11,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 107
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 137
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 107
                }
            }]
        }]
    }]
}, {
    "id": 7,
    "name": "Air Cafe",
    "type": "coffeehouse",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.191974,
    "longitude": 16.608418,
    "openingHours": [{
        "start": "monday 10:00",
        "end": "monday 23:00"
    }, {
        "start": "tuesday 10:00",
        "end": "tuesday 23:00"
    }, {
        "start": "wednesday 10:00",
        "end": "wednesday 23:00"
    }, {
        "start": "thursday 10:00",
        "end": "thursday 23:00"
    }, {
        "start": "friday 10:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 10:00",
        "end": "saturday 23:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-08",
        "glass": "grappa",
        "ambient": 8,
        "serviceInformedness": 6,
        "serviceAmiability": 7,
        "restroomCleanness": 7,
        "towels": "none",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 95
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 95
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 19,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 95
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 8,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 20,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }]
    }]
}, {
    "id": 8,
    "name": "Brňák bar",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.200366,
    "longitude": 16.610735,
    "openingHours": [{
        "start": "monday 15:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 15:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 15:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 15:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 15:00",
        "end": "saturday 02:00"
    }, {
        "start": "saturday 18:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 18:00",
        "end": "monday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-03",
        "glass": "whisky tumbler",
        "ambient": 7,
        "serviceInformedness": 7,
        "serviceAmiability": 7,
        "restroomCleanness": null,
        "towels": null,
        "isCashOnly": null,
        "isSmokingAllowed": null,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 100
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 45,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 100
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 380
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 11,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 100
                }
            }]
        }, {
            "whiskyId": 17,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 9,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }]
    }]
}, {
    "id": 9,
    "name": "SHOT Bar",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.196541,
    "longitude": 16.609469,
    "openingHours": [{
        "start": "monday 17:00",
        "end": "tuesday 02:00"
    }, {
        "start": "tuesday 17:00",
        "end": "wednesday 02:00"
    }, {
        "start": "wednesday 17:00",
        "end": "thursday 02:00"
    }, {
        "start": "thursday 17:00",
        "end": "friday 02:00"
    }, {
        "start": "friday 17:00",
        "end": "saturday 03:00"
    }, {
        "start": "saturday 17:00",
        "end": "sunday 02:00"
    }, {
        "start": "sunday 17:00",
        "end": "monday 02:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-10-14",
        "glass": "glencairn",
        "ambient": 7,
        "serviceInformedness": 5,
        "serviceAmiability": 6,
        "restroomCleanness": null,
        "towels": null,
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 3,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 17,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 22,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 21,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 250
                }
            }]
        }, {
            "whiskyId": 23,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 24,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 25,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 260
                }
            }]
        }, {
            "whiskyId": 26,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 300
                }
            }]
        }, {
            "whiskyId": 13,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }]
    }]
}, {
    "id": 10,
    "name": "Malej Velkej Bar",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.196298,
    "longitude": 16.608460,
    "openingHours": [{
        "start": "monday 16:00",
        "end": "tuesday 01:00"
    }, {
        "start": "tuesday 16:00",
        "end": "wednesday 01:00"
    }, {
        "start": "wednesday 16:00",
        "end": "thursday 01:00"
    }, {
        "start": "thursday 16:00",
        "end": "friday 01:00"
    }, {
        "start": "friday 16:00",
        "end": "saturday 03:00"
    }, {
        "start": "saturday 18:00",
        "end": "sunday 03:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-10-14",
        "glass": "Sherry",
        "ambient": 4,
        "serviceInformedness": 6,
        "serviceAmiability": 5,
        "restroomCleanness": null,
        "towels": null,
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 115
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 13,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 19,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }]
    }]
}, {
    "id": 11,
    "name": "Craig Bar",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.195998,
    "longitude": 16.611009,
    "openingHours": [{
        "start": "tuesday 17:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 17:00",
        "end": "thursday 01:00"
    }, {
        "start": "thursday 17:00",
        "end": "friday 01:00"
    }, {
        "start": "friday 17:00",
        "end": "saturday 02:00"
    }, {
        "start": "saturday 18:00",
        "end": "sunday 02:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-10-11",
        "glass": "Sherry",
        "ambient": 7,
        "serviceInformedness": 4,
        "serviceAmiability": 7,
        "restroomCleanness": null,
        "towels": null,
        "isCashOnly": true,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 13,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 14,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 15,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 250
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 145
                }
            }]
        }, {
            "whiskyId": 17,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }]
    }]
}, {
    "id": 12,
    "name": "Pivní panorama",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.191718,
    "longitude": 16.654628,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "monday 22:00"
    }, {
        "start": "tuesday 11:00",
        "end": "tuesday 22:00"
    }, {
        "start": "wednesday 11:00",
        "end": "wednesday 22:00"
    }, {
        "start": "thursday 11:00",
        "end": "thursday 22:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 11:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 11:00",
        "end": "sunday 22:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-10-11",
        "glass": "tumbler",
        "ambient": 6,
        "serviceInformedness": 3,
        "serviceAmiability": 7,
        "restroomCleanness": 8,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": null,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 80
                }
            }]
        }]
    }]
}, {
    "id": 13,
    "name": "Bat bar",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.191765,
    "longitude": 16.654649,
    "openingHours": [{
        "start": "monday 15:00",
        "end": "monday 23:00"
    }, {
        "start": "tuesday 15:00",
        "end": "tuesday 23:00"
    }, {
        "start": "wednesday 15:00",
        "end": "wednesday 23:00"
    }, {
        "start": "thursday 15:00",
        "end": "thursday 23:00"
    }, {
        "start": "friday 15:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 15:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 15:00",
        "end": "sunday 22:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-17",
        "glass": null,
        "ambient": 6,
        "serviceInformedness": 5,
        "serviceAmiability": 8,
        "restroomCleanness": 5,
        "towels": "paper",
        "isCashOnly": null,
        "isSmokingAllowed": null,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 85
                }
            }]
        }, {
            "whiskyId": 8,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }]
    }]
}, {
    "id": 14,
    "name": "Fléda",
    "type": "club",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.209846,
    "longitude": 16.603350,
    "openingHours": [],
    "reviews": [{
        "raterId": 0,
        "date": "2016-10-27",
        "glass": "tumbler",
        "ambient": 4,
        "serviceInformedness": 2,
        "serviceAmiability": 3,
        "restroomCleanness": 1,
        "towels": null,
        "isCashOnly": true,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 99
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 109
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 95
                }
            }]
        }]
    }]
}, {
    "id": 15,
    "name": "Jakoby",
    "type": "restaurant",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.196964,
    "longitude": 16.608623,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 11:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 11:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 11:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 01:00"
    }, {
        "start": "saturday 12:00",
        "end": "sunday 01:00"
    }, {
        "start": "sunday 12:00",
        "end": "sunday 23:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-11-05",
        "glass": null,
        "ambient": 6,
        "serviceInformedness": 1,
        "serviceAmiability": 4,
        "restroomCleanness": 8,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": null,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 195
                }
            }]
        }]
    }]
}, {
    "id": 16,
    "name": "JBM Brew Lab Pub",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.200665,
    "longitude": 16.596017,
    "openingHours": [{
        "start": "monday 16:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 16:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 16:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 16:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 16:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 16:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 16:00",
        "end": "monday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-11-20",
        "glass": "tumbler",
        "ambient": 5,
        "serviceInformedness": 4,
        "serviceAmiability": 5,
        "restroomCleanness": 5,
        "towels": null,
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 109
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 159
                }
            }]
        }, {
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 99
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 159
                }
            }]
        }]
    }]
}, {
    "id": 17,
    "name": "Clubwash",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.199481,
    "longitude": 16.617906,
    "openingHours": [{
        "start": "monday 18:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 18:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 18:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 18:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 18:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 18:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 18:00",
        "end": "monday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-02",
        "glass": "tumbler",
        "ambient": 4,
        "serviceInformedness": 4,
        "serviceAmiability": 5,
        "restroomCleanness": null,
        "towels": "paper",
        "isCashOnly": null,
        "isSmokingAllowed": null,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 79
                }
            }]
        }]
    }]
}, {
    "id": 18,
    "name": "St. Patrick Irish Pub",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.196228,
    "longitude": 16.608145,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 11:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 11:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 11:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 12:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 12:00",
        "end": "sunday 23:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-03",
        "glass": null,
        "ambient": 5,
        "serviceInformedness": 1,
        "serviceAmiability": 5,
        "restroomCleanness": 0,
        "towels": null,
        "isCashOnly": null,
        "isSmokingAllowed": null,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 105
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 185
                }
            }]
        }, {
            "whiskyId": 22,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 109
                }
            }]
        }, {
            "whiskyId": 91,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 115
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 105
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 145
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 375
                }
            }]
        }]
    }]
}, {
    "id": 19,
    "name": "Soul Bistro",
    "type": "bistro",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.197029,
    "longitude": 16.610234,
    "openingHours": [{
        "start": "monday 08:00",
        "end": "monday 22:00"
    }, {
        "start": "tuesday 08:00",
        "end": "tuesday 22:00"
    }, {
        "start": "wednesday 08:00",
        "end": "wednesday 22:00"
    }, {
        "start": "thursday 08:00",
        "end": "thursday 22:00"
    }, {
        "start": "friday 08:00",
        "end": "friday 23:00"
    }, {
        "start": "saturday 10:00",
        "end": "saturday 23:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-12",
        "glass": "glencairn",
        "ambient": 5,
        "serviceInformedness": 4,
        "serviceAmiability": 5,
        "restroomCleanness": null,
        "towels": null,
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 85,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 154
                }
            }]
        }, {
            "whiskyId": 61,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 219
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 259
                }
            }]
        }]
    }]
}, {
    "id": 20,
    "name": "Lucky Bastard Beerhouse",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.198941,
    "longitude": 16.591249,
    "openingHours": [{
        "start": "monday 17:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 17:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 17:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 17:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 17:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 17:00",
        "end": "sunday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-12",
        "glass": "glencairn",
        "ambient": 5,
        "serviceInformedness": 3,
        "serviceAmiability": 4,
        "restroomCleanness": 7,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 85,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 20,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 110
                }
            }]
        }, {
            "whiskyId": 91,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 100
                }
            }]
        }, {
            "whiskyId": 88,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }]
    }]
}, {
    "id": 21,
    "name": "Tusto",
    "type": "restaurant",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.187611,
    "longitude": 16.606267,
    "openingHours": [{
        "start": "monday 10:00",
        "end": "monday 23:00"
    }, {
        "start": "tuesday 10:00",
        "end": "tuesday 23:00"
    }, {
        "start": "wednesday 10:00",
        "end": "wednesday 23:00"
    }, {
        "start": "thursday 10:00",
        "end": "thursday 23:00"
    }, {
        "start": "friday 10:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 11:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 11:00",
        "end": "sunday 23:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-16",
        "glass": "tumbler",
        "ambient": 5,
        "serviceInformedness": 4,
        "serviceAmiability": 6,
        "restroomCleanness": 8,
        "towels": "none",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 20,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 105
                }
            }]
        }]
    }]
}, {
    "id": 22,
    "name": "U Třech Čertů",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.192336,
    "longitude": 16.607137,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 11:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 11:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 11:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 02:00"
    }, {
        "start": "saturday 11:00",
        "end": "sunday 02:00"
    }, {
        "start": "sunday 12:00",
        "end": "monday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2016-12-29",
        "glass": null,
        "ambient": 3,
        "serviceInformedness": 4,
        "serviceAmiability": 5,
        "restroomCleanness": 4,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 109
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 129
                }
            }]
        }, {
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 129
                }
            }]
        }]
    }]
}, {
    "id": 23,
    "name": "The Whisky Shop",
    "type": "store",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.192143,
    "longitude": 16.601467,
    "openingHours": [{
        "start": "monday 10:00",
        "end": "monday 20:00"
    }, {
        "start": "tuesday 10:00",
        "end": "tuesday 20:00"
    }, {
        "start": "wednesday 10:00",
        "end": "wednesday 20:00"
    }, {
        "start": "thursday 10:00",
        "end": "thursday 20:00"
    }, {
        "start": "friday 10:00",
        "end": "friday 21:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-01-06",
        "glass": "tulip tasting",
        "ambient": 7,
        "serviceInformedness": 10,
        "serviceAmiability": 9,
        "restroomCleanness": null,
        "towels": null,
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": []
    }]
}, {
    "id": 24,
    "name": "4pokoje",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.195766,
    "longitude": 16.611808,
    "openingHours": [{
        "start": "monday 07:00",
        "end": "tuesday 05:00"
    }, {
        "start": "tuesday 07:00",
        "end": "wednesday 05:00"
    }, {
        "start": "wednesday 07:00",
        "end": "thursday 05:00"
    }, {
        "start": "thursday 07:00",
        "end": "friday 05:00"
    }, {
        "start": "friday 07:00",
        "end": "saturday 05:00"
    }, {
        "start": "saturday 07:00",
        "end": "sunday 05:00"
    }, {
        "start": "sunday 07:00",
        "end": "monday 05:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-02-09",
        "glass": "tulip tasting",
        "ambient": 8,
        "serviceInformedness": 7,
        "serviceAmiability": 9,
        "restroomCleanness": 7,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 23,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 0,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 270
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 41,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 36,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 500
                }
            }]
        }, {
            "whiskyId": 10,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 19,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 90
                }
            }]
        }, {
            "whiskyId": 44,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 180
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }]
    }]
}, {
    "id": 25,
    "name": "Zelená Kočka Pivárium",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.195954,
    "longitude": 16.610147,
    "openingHours": [{
        "start": "monday 16:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 16:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 16:00",
        "end": "thursday 01:00"
    }, {
        "start": "thursday 16:00",
        "end": "friday 01:00"
    }, {
        "start": "friday 16:00",
        "end": "saturday 01:00"
    }, {
        "start": "saturday 16:00",
        "end": "sunday 01:00"
    }, {
        "start": "sunday 17:00",
        "end": "sunday 23:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-01-19",
        "glass": null,
        "ambient": 8,
        "serviceInformedness": 5,
        "serviceAmiability": 7,
        "restroomCleanness": 6,
        "towels": "none",
        "isCashOnly": true,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 61,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 225
                }
            }]
        }]
    }]
}, {
    "id": 26,
    "name": "Jedna báseň",
    "type": "restaurant",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "Jihlavská 7a, 625 00 Brno-Bohunice",
    "links": [],
    "latitude": 49.173078,
    "longitude": 16.582966,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "monday 23:00"
    }, {
        "start": "tuesday 11:00",
        "end": "tuesday 23:00"
    }, {
        "start": "wednesday 11:00",
        "end": "wednesday 23:00"
    }, {
        "start": "thursday 11:00",
        "end": "thursday 23:00"
    }, {
        "start": "friday 11:00",
        "end": "friday 23:00"
    }, {
        "start": "saturday 11:00",
        "end": "saturday 23:00"
    }, {
        "start": "sunday 11:00",
        "end": "sunday 23:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-01-28",
        "glass": "whisky tumbler",
        "ambient": 7,
        "serviceInformedness": 4,
        "serviceAmiability": 7,
        "restroomCleanness": 7,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 92,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 99
                }
            }]
        }]
    }]
}, {
    "id": 27,
    "name": "Campus River",
    "type": "restaurant",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "Netroufalky 2, 625 00 Brno-Starý Lískovec",
    "links": [],
    "latitude": 49.175709,
    "longitude": 16.566324,
    "openingHours": [{
        "start": "monday 07:30",
        "end": "monday 22:00"
    }, {
        "start": "tuesday 07:30",
        "end": "tuesday 22:00"
    }, {
        "start": "wednesday 07:30",
        "end": "wednesday 22:00"
    }, {
        "start": "thursday 07:30",
        "end": "thursday 22:00"
    }, {
        "start": "friday 07:30",
        "end": "friday 22:00"
    }, {
        "start": "saturday 08:30",
        "end": "saturday 22:00"
    }, {
        "start": "sunday 08:30",
        "end": "sunday 21:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-02-01",
        "glass": "whisky tumbler",
        "ambient": 6,
        "serviceInformedness": 5,
        "serviceAmiability": 5,
        "restroomCleanness": 7,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 4,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 155
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 125
                }
            }]
        }]
    }]
}, {
    "id": 28,
    "name": "Charlie’s Square",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "",
    "links": [],
    "latitude": 49.192693,
    "longitude": 16.611329,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 11:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 11:00",
        "end": "thursday 01:00"
    }, {
        "start": "thursday 11:00",
        "end": "friday 01:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 01:00"
    }, {
        "start": "saturday 11:30",
        "end": "sunday 01:00"
    }, {
        "start": "sunday 11:30",
        "end": "sunday 22:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-01-26",
        "glass": null,
        "ambient": 6,
        "serviceInformedness": 5,
        "serviceAmiability": 7,
        "restroomCleanness": 7,
        "towels": null,
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 138
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 158
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 192
                }
            }]
        }, {
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 148
                }
            }]
        }, {
            "whiskyId": 13,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 160
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 188
                }
            }]
        }]
    }]
}, {
    "id": 29,
    "name": "Pivnice U Čápa",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "Obilní trh 68/10",
    "links": [],
    "latitude": 49.199696,
    "longitude": 16.598110,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 11:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 11:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 11:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 11:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 11:00",
        "end": "monday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-03-08",
        "glass": null,
        "ambient": 6,
        "serviceInformedness": 6,
        "serviceAmiability": 6,
        "restroomCleanness": 7,
        "towels": "none",
        "isCashOnly": true,
        "isSmokingAllowed": true,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 101,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 109
                }
            }]
        }, {
            "whiskyId": 12,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 109
                }
            }]
        }]
    }]
}, {
    "id": 30,
    "name": "Stopkova Plzeňská Pivnice",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "Česká 163/5",
    "links": [],
    "latitude": 49.195588,
    "longitude": 16.606626,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 11:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 11:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 11:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 11:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 11:00",
        "end": "monday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-03-13",
        "glass": null,
        "ambient": 7,
        "serviceInformedness": 6,
        "serviceAmiability": 7,
        "restroomCleanness": 7,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 119
                }
            }]
        }, {
            "whiskyId": 102,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 119
                }
            }]
        }]
    }]
}, {
    "id": 31,
    "name": "Dominik PUB",
    "type": "pub",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "Dominikánská 3",
    "links": ["http://www.dominikpub.cz/"],
    "latitude": 49.193382,
    "longitude": 16.606946,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 11:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 11:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 11:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 01:00"
    }, {
        "start": "saturday 12:00",
        "end": "sunday 01:00"
    }, {
        "start": "sunday 12:00",
        "end": "sunday 23:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-04-04",
        "glass": null,
        "ambient": 7,
        "serviceInformedness": 6,
        "serviceAmiability": 7,
        "restroomCleanness": 3,
        "towels": "paper",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 99
                }
            }]
        }]
    }]
}, {
    "id": 32,
    "name": "Runway Bar",
    "type": "bar",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "Jezuitská 687/6",
    "links": [],
    "latitude": 49.197672,
    "longitude": 16.613418,
    "openingHours": [{
        "start": "tuesday 18:00",
        "end": "wednesday 02:00"
    }, {
        "start": "wednesday 18:00",
        "end": "thursday 02:00"
    }, {
        "start": "thursday 18:00",
        "end": "friday 02:00"
    }, {
        "start": "friday 18:00",
        "end": "saturday 02:00"
    }, {
        "start": "saturday 18:00",
        "end": "sunday 02:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-03-17",
        "glass": "tulip tasting",
        "ambient": 8,
        "serviceInformedness": 8,
        "serviceAmiability": 7,
        "restroomCleanness": 9,
        "towels": "cloth",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 96,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 390
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 100
                }
            }]
        }, {
            "whiskyId": 103,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 98,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }, {
            "whiskyId": 18,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 130
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 104,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 7,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 140
                }
            }]
        }, {
            "whiskyId": 10,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 145
                }
            }]
        }, {
            "whiskyId": 36,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 150
                }
            }]
        }, {
            "whiskyId": 2,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 155
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 165
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 170
                }
            }]
        }, {
            "whiskyId": 8,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 175
                }
            }]
        }, {
            "whiskyId": 5,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 175
                }
            }]
        }, {
            "whiskyId": 72,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 215
                }
            }]
        }, {
            "whiskyId": 15,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 195
                }
            }]
        }]
    }]
}, {
    "id": 33,
    "name": "Restaurace Rubín",
    "type": "restaurant",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "Makovského nám. 2574/1",
    "links": [],
    "latitude": 49.218546,
    "longitude": 16.576324,
    "openingHours": [{
        "start": "monday 11:00",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 11:00",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 11:00",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 11:00",
        "end": "friday 00:00"
    }, {
        "start": "friday 11:00",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 11:00",
        "end": "sunday 00:00"
    }, {
        "start": "sunday 11:00",
        "end": "monday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-06-03",
        "glass": null,
        "ambient": 7,
        "serviceInformedness": 5,
        "serviceAmiability": 6,
        "restroomCleanness": 7,
        "towels": "none",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 105,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 190
                }
            }]
        }, {
            "whiskyId": 6,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 390
                }
            }]
        }, {
            "whiskyId": 42,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 240
                }
            }]
        }, {
            "whiskyId": 39,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 220
                }
            }]
        }, {
            "whiskyId": 16,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 125
                }
            }]
        }, {
            "whiskyId": 32,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 165
                }
            }]
        }, {
            "whiskyId": 43,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 220
                }
            }]
        }, {
            "whiskyId": 106,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 430
                }
            }]
        }]
    }]
}, {
    "id": 34,
    "name": "Restaurace STAVBA",
    "type": "restaurant",
    "description": {
        "cs-CZ": "",
        "en-US": ""
    },
    "address": "Grohova 101/2",
    "links": [],
    "latitude": 49.201201,
    "longitude": 16.599165,
    "openingHours": [{
        "start": "monday 10:30",
        "end": "tuesday 00:00"
    }, {
        "start": "tuesday 10:30",
        "end": "wednesday 00:00"
    }, {
        "start": "wednesday 10:30",
        "end": "thursday 00:00"
    }, {
        "start": "thursday 10:30",
        "end": "friday 00:00"
    }, {
        "start": "friday 10:30",
        "end": "saturday 00:00"
    }, {
        "start": "saturday 11:00",
        "end": "sunday 00:00"
    }],
    "reviews": [{
        "raterId": 0,
        "date": "2017-06-14",
        "glass": null,
        "ambient": 5,
        "serviceInformedness": 5,
        "serviceAmiability": 6,
        "restroomCleanness": 6,
        "towels": "none",
        "isCashOnly": false,
        "isSmokingAllowed": false,
        "isOperational": true,
        "bottles": [{
            "whiskyId": 1,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 80
                }
            }]
        }, {
            "whiskyId": 33,
            "offers": [{
                "size": 0.04,
                "price": {
                    "currency": "CZK",
                    "value": 120
                }
            }]
        }]
    }]
}];
exports.default = rawPlaces;