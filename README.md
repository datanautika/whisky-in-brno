Whisky in Brno
==============


## Installation

- Install **[Node.js](http://nodejs.org/) v7**.
- Install **[Git](https://git-scm.com/) v2.9.x**
- Clone the repo and run from **PowerShell**:
    - `./git-setup` (**This is super important!** It links `.gitconfig` from the repo.)
    - `npm install`
    - `npm install gulpjs/gulp#4.0 -g`
    - `npm install node-dev -g`
    - `npm run production-build` or `npm run watch`


## Run

- Dev server: `npm start`
- Visit [http://localhost:8080](http://localhost:8080)


## License

Copyright 2016 Datanautika s.r.o.

[The MIT License](./LICENSE)
